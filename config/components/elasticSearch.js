
'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
    ELASTIC_SEARCH_URL: joi.string().required(),
    ELASTIC_SEARCH_AUTH: joi.string().required()

}).unknown()
    .required()

const envVars = joi.attempt(process.env, envVarsSchema)

const config = {
    elasticSearch: {
        url: envVars.ELASTIC_SEARCH_URL,
        Auth: envVars.ELASTIC_SEARCH_AUTH
    }
}

module.exports = config;