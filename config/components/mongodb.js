'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
    MONGODB_URL: joi.string()
        .required()
}).unknown()
    .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
    throw new Error(`Config validation error: ${error.message}`)
}

const config = {
    mongodb: {
        url: envVars.MONGODB_URL
        //url: "mongodb://chris:jhzgur6jw2nxsefv@pairzy-shard-00-00-2huaq.mongodb.net:27017,pairzy-shard-00-01-2huaq.mongodb.net:27017,pairzy-shard-00-02-2huaq.mongodb.net:27017/admin?ssl=true&replicaSet=Pairzy-shard-0&authSource=admin&w=majority"
    }
}

module.exports = config