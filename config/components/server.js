'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
  PORT: joi.number().required(),
  API_URL: joi.string().required(),
  APPNAME: joi.string().required(),
  TEAMNAME: joi.string().required(),
  LATEST_VERSION: joi.string().required(),
}).unknown()
  .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const config = {
  server: {
    port: envVars.PORT,
    API_URL: envVars.API_URL,
    APPNAME: envVars.APPNAME,
    TEAMNAME: envVars.TEAMNAME,
    LATEST_VERSION: envVars.LATEST_VERSION
  }
}

module.exports = config