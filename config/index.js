/* eslint-disable global-require, import/no-dynamic-require */

'use strict'

const processType = process.env.PROCESS_TYPE
// console.log("processType", processType)
let config = require(`./${processType}`)

module.exports = config
