const FCM = require('fcm-push');
const config = require('../../config');
const fcm = new FCM(process.env.FCM_SERVER_KEY);
const sendPushToTopic = (userId, request, callback) => {

    //console.log("======>",request)

    let payload = {};
    let resData = {
        action: 1,
        pushType: 2,
        title: 'Match',
        categoryIdentifier: "chat.category",
        msg: 'Hey You Got Match',
        "type": request.data.type,
        "target_id": request.data.target_id,
        "title": request.notification.title || process.env.APPNAME,
        "message": request.notification.body,
        "sound": "default",

        apns: {
            headers: {
                'apns-collapse-id': "1"
            }
        },
    }
    payload = {
        to: `${userId}`, // required fill with device token or topics
        // to: `/topics/${userId}`, // required fill with device token or topics
        collapse_key: '1',
        priority: 'high',
        apns: {
            headers: {
                'collapse_key': "1"
            }
        },
        "delay_while_idle": true,
        "dry_run": false,
        "time_to_live": 3600,
        "content_available": true,
        badge: 1,
        data: resData,
        categoryIdentifier: "chat.category",
    };
    if (request.data.deviceType == "1") {
        console.log("androidddddddddddddddddddddddddddd")

        payload.notification = {
            title: request.notification.title || process.env.APPNAME,
            body: request.notification.body,
            sound: "default",
            categoryIdentifier: "chat.category",

        };
        payload.apns = {
            headers: {
                'collapse_key': "1"
            }
        }
    }

    // if (request.data.deviceType == "2") {
    //     console.log("androidddddddddddddddddddddddddddd")
    //     payload.data = {
    //         title: request.notification.title || process.env.APPNAME,
    //         body: request.notification.body,
    //         sound: "default",
    //         categoryIdentifier: "chat.category",

    //     };
    //     payload.apns = {
    //         headers: {
    //             'collapse_key': "1"
    //         }
    //     }
    // }
    

    //console.log("fcm data ", payload)
    fcm.send(payload)
        .then(function (response) {
            // console.log("notification sent payload ", payload)
            console.log("notification sent ", response)
            callback(null, 1);
        })
        .catch(function (err) {
            console.log("Something has gone wrong ! ", err);
            callback(err, null);
        });



    /** 
    * Function to send push notification to a specified topic 
    * topic: should follow [a-zA-Z0-9-_.~%] format
    * payload: must be object 
           format: { 
               notification : { body : "string", title : "string", icon : "string" },
               data: { field1: 'value1', field2: 'value2' } // values must be string
           }
    
           * Send a message to devices subscribed to the provided topic.
         */
    // new Promise((resolve, reject) => {

    //     admin.messaging().sendToTopic(topic, payload)
    //         .then(function (response) {
    //             console.log("notification sent")
    //             // resolve({ err: 0, message: "notification sent" });
    //         })
    //         .catch(function (error) {
    //             /* Error sending message  */
    //             console.log("notification not  sent", error)
    //             // reject({ err: 1, message: error.message, error: error });
    //         });
    // })
}



/**
 * Function to send push notification to specified push tokens
 * registrationTokens: Array of registration tokens(push tokens)  
 * payload: must be object 
        format: { 
            notification : { body : "string", title : "string", icon : "string" },
            data: { field1: 'value1', field2: 'value2' } // values must be string
        }
 */
async function sendPushToToken(registrationTokens, payload) {

    /** 
     * Send a message to devices subscribed to the provided topic.
     */

    var message = {
        to: registrationTokens, // required fill with device token or topics
        collapse_key: 'your_collapse_key',
        data: payload.data,
        notification: payload.notification
    };

    //callback style
    fcm.send(message, function (err, response) {
        if (err) {
            console.log("Something has gone wrong!");
        } else {
            console.log("Successfully sent with response: ", response);
        }
    });

}
module.exports = {
    sendPushToTopic,
    sendPushToToken
}