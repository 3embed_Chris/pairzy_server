'use strict';
const joi = require('joi')
const moment = require('moment');//date-time
const logger = require('winston');
const config = require('../../config')
// const emailLog = require('../../models/emailLog')
const mailgun = require('mailgun-js')({ apiKey: config.mailgun.MAILGUN_AUTH_KEY, domain: config.mailgun.MAILGUN_DOMAIN_NAME });

const envVarsSchema = joi.object({
    from: joi.string().default(config.mailgun.MAILGUN_FROM),
    to: joi.string().required(),
    subject: joi.string().required(),
    text: joi.string().required(),

}).unknown();//params

/**
 * sendMail this method  used to send mail usign mailgun 
 * store messgae log  in database
 * @param {Object} params - the params like:from,to,subject,text
 * @param {Object} callback - sucesss either error 
 * @returns {Object} object - return messgae body or error.
 */
function sendMail(params, callback) {
    try {
        const mailParam = joi.attempt(params, envVarsSchema)
        mailgun.messages().send(mailParam, function (error, body) {
            if (typeof body != 'undefined') {
                var insData = {
                    msgId: body.id,
                    createDate: moment().unix(),
                    status: body.message,
                    trigger: params.trigger,
                    subject: params.subject,
                    to: params.to
                };
                // emailLog.Insert(insData, (err, response) => {
                // });
                return callback(null, body)
            } else {
                return callback(error)
            }
        });
    } catch (e) {
        return callback(e)
    }

}//send mail and store database

module.exports = { sendMail };


