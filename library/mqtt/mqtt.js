const mqtt = require('mqtt');
const logger = require('winston')
const config = require('../../config');

// get ip address
var os = require('os');
var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address);
        }
    }
}

var MqttClientId = "MQTT_CLIENT_DATUM_VERSION_2" + addresses[0];
/**
 * options:
 *  - clientId
 *  - username
 *  - password
 *  - keepalive: number
 *  - clean:
 *  - will: 
 */
var mqtt_options = {
    clientId: MqttClientId,
    keepalive: 0,
    clean: false,
    username: config.mqtt.MQTT_USERNAME,
    password: config.mqtt.MQTT_PASSWORD
};

const client = mqtt.connect(config.mqtt.MQTT_URL, mqtt_options);

logger.info("MQTT_URL connect")
client.on('connect', function () {
    // logger.info("MQTT_URL connect")
})
/**
 * options: object
 *  - qos (integer): 0 > fire and forget
 *          1 > guaranteed delivery
 *          2 > guaranteed delivery with awk
 */
function mqtt_publish(topic, message, options, callback) {

    try {
        client.publish(topic, message, { qos: (options.qos) ? options.qos : 0 })
        callback(null, { err: 0, message: 'Publish.' })
    } catch (exec) {
        callback({ err: 1, message: exec.message })
    }

}


/**
 * topic : string
 * options: object
 *  - qos : 0 > fire and forget
 *          1 > guaranteed delivery
 *          2 > guaranteed delivery with awk
 */
function mqtt_subscribe(topic, options, callback) {
    try {
        client.subscribe(topic, { qos: (options.qos) ? options.qos : 0 });
    } catch (exec) {
        callback({ err: 1, message: exec.message })
    }
    callback(null, { err: 0, message: 'Subscribed.' })
}

client.on('message', (topic, message) => {
    /**
     * match the pattern (start with) 
     */
    switch (topic) {
        case String(topic.match(/^match.*/)):
            var data = JSON.parse(message);

            if (data.status == 5) {

            }
            break;
        default:


    }


})

exports.publish = mqtt_publish
exports.subscribe = mqtt_subscribe