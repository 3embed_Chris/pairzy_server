
module.exports = {
    getExpiryTimeForMongoTTL: function (timeInMilisecond = 0, plushMinites = 0, plushSecond = 0, plushMiliSecond = 0) {
        /**
         * You need to pass following paramaters.
         * timeInMilisecond, plushMinites, plushSecond, plushMiliSecond
         */
        let myDate = new Date(timeInMilisecond);
        return myDate.setMinutes(myDate.getMinutes() + plushMinites, plushSecond, plushMiliSecond);
    }
}