'use strict'

const path = require('path');
const fork = require('child_process').fork;
let logger = require('winston');
const find = require('find-process');
var numCPUs = require('os').cpus().length;

let email_queue = {
    name: "email Worker",
    path: path.join(__dirname, '../../worker/email-worker/'),
    worker: null,
    flag: true,
    alwaysRun: true
};

let update_location = {
    name: "update location Worker",
    path: path.join(__dirname, '../../worker/ipToLatLong-worker/'),
    worker: null,
    flag: true,
    alwaysRun: true
};

let update_latLong_to_city = {
    name: "update latLong to city",
    path: path.join(__dirname, '../../worker/latLongToCity-worker/'),
    worker: null,
    flag: true,
    alwaysRun: true
};

let update_bulk = {
    name: "update bulk",
    path: path.join(__dirname, '../../worker/bulk-worker/'),
    worker: null,
    flag: true,
    alwaysRun: true
};

let search_result = {
    name: "search result ",
    path: path.join(__dirname, '../../worker/searchResult-worker/'),
    worker: null,
    flag: true,
    alwaysRun: true
};

let sms_queue = {
    name: "sms queue",
    path: path.join(__dirname, '../../worker/sms-worker/'),
    worker: null,
    flag: true,
    alwaysRun: true
};

let coinWallet_queue = {
    name: "coin queue",
    path: path.join(__dirname, '../../worker/bulk-CoinInsert/'),
    worker: null,
    flag: true,
    alwaysRun: true
};

function startWorkerProcess(workerData) {
    if (workerData.worker == null) {
        find('name', workerData.path, false)
            .then(function (list) {
                console.log('there are %s %s process(es)', JSON.stringify(list.length), workerData.path);
                if (list.length < numCPUs) {
                    workerData.worker = fork(workerData.path);
                    workerData.worker.on('exit', function (code, signal) {
                        logger.error(`${workerData.name} worker exited with code ${code} and signal ${signal}`);
                        workerData.worker = null;
                        if (workerData.flag) {
                            if (workerData.alwaysRun)
                                startWorkerProcess(workerData);
                        }
                    });
                }
            });
    } else {
        logger.warn(`${workerData.name} worker is already running`);
    }
}

function stopWorkerProcess(workerData) {
    if (workerData.worker == null) {
        logger.warn(`${workerData.name} worker is not running`);
    } else {
        workerData.flag = false;
        workerData.worker.kill();
        workerData.worker = null;
    }
}

function checkWorker(queue) {
    find('name', queue.path, false)
        .then(function (list) {
            console.log('there are %s %s process(es)', JSON.stringify(list.length), queue.path);
            if (list.length < numCPUs) {
                startWorkerProcess(queue.worker);
            }
        });
}

function startWorker() {
    startWorkerProcess(email_queue);
    startWorkerProcess(update_location);
    startWorkerProcess(update_latLong_to_city);
    startWorkerProcess(update_bulk);
    startWorkerProcess(search_result);
    startWorkerProcess(coinWallet_queue);

    //startWorkerProcess(sms_queue);
}

function exitHandler() {
    stopWorkerProcess(email_queue);
    stopWorkerProcess(update_location);
    stopWorkerProcess(update_latLong_to_city);
    stopWorkerProcess(update_bulk);
    stopWorkerProcess(search_result);
    startWorkerProcess(coinWallet_queue);

    //stopWorkerProcess(sms_queue);
    process.exit();
}

module.exports = {
    startWorker,
    startWorkerProcess,
    stopWorkerProcess,
    exitHandler,
    checkWorker,
    email_queue,
    update_location,
    update_latLong_to_city,
    update_bulk,
    search_result,
    sms_queue,
    coinWallet_queue
};