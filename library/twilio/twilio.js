'use strict'

const joi = require('joi')
let moment = require('moment');//date-time
const config = require('../../config')
// const smsLog = require('../../models/smsLog')
const configuration = require('../../config');
const client = require('twilio')(
    config.twilio.TWILIO_ACCOUNT_SID,
    config.twilio.TWILIO_AUTH_TOKEN
);
const envVarsSchema = joi.object({
    to: joi.string().required(),
    body: joi.string().required(),
    from: joi.string().default(config.twilio.CELL_PHONE_NUMBER),
}).unknown().required()

/**
 * sendSms this method  used to send message usign twilio 
 * store messgae log  in database
 * @param {Object} params - the params like:from,to,body and statusCallback
 * @param {Object} callback - sucesss either error 
 * @returns {Object} object - return messgae body or error.
 */
function sendSms(params, callback) {
    try { 
        const twilioConf = joi.attempt(params, envVarsSchema)//Joi validation 
        client.messages.create(twilioConf, function (err, message) { 
            if (typeof message != 'undefined') {
                let userdata = {
                    msgId: message.sid,
                    createDate: moment().unix(),
                    status: message.status,
                    trigger: params.trigger,
                    msg: message.body,
                    to: message.to
                };
                // smsLog.Insert(userdata, (err, response) => {
                // });//insert  log in database
                callback(null,userdata)
            } 
        });
    } catch (e) {
        console.log("error --: ", e);
        // callback(e);
    }

}

module.exports = { sendSms };