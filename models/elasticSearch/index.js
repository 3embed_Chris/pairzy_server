'use strict'

const url = require('./config')
const logger = require('winston');
const config = require('../../config/index');

var elasticsearch = require('elasticsearch');
var elasticClient;

var state = {
  connection: null,
}

exports.connect = function (done) {
  if (state.connection) return done()

  // elasticClient = new elasticsearch.Client({
  //   host: config.elasticSearch.url,
  //   // log: 'info'
  // });

  elasticClient = new elasticsearch.Client({
    hosts: {
      protocol: 'https',
      host: config.elasticSearch.url,
      port: 9243,
      auth: config.elasticSearch.Auth
    }
  }
  );
  logger.info("elasticSearch connection successfully established to  ", config.elasticSearch.url)
  state.connection = elasticClient
  done()
}

exports.get = function () {
  return state.connection
}