
'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
    datum2_url: joi.string().required()
}).unknown()
    .required()

const envVars = joi.attempt(process.env, envVarsSchema)

module.exports = envVars.datum2_url;
