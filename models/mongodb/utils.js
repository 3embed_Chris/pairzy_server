'use strict'

var db;
var url = require('./config')
var logger = require('winston');
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;

MongoClient.connect(url, function (err, connDB) {
    if (err) {
        console.log(err);
    }
    else {
        logger.info(`Connection to database url ${url} established successfuly`);
        db = connDB;
    }
})


module.exports = { };
