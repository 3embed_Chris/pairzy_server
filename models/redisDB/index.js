'use strict'
const redis = require("redis");
const logger = require('winston');
const config = require('../../config');

var client = redis.createClient(6379, config.redis.REDIS_IP);

/**
 * Method to connect to the redis
 * @param {*} url
 * @returns connection object
 */
client.auth("3embed")
client.on('connect', function () {
    logger.info('Redis connected');
});

module.exports={client}