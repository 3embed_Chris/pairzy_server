'use strict'
const logger = require('winston');
const elasticClient = require('../elasticSearch');
const tablename = 'userList';
const indexName = process.env.ELASTICK_INDEX;
const version = 382;

/* function findMatch(data, callback) {
    let condition = {
        "from": data.skip, "size": data.limit,
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "gender": data.gender
                        }
                    },
                    {
                        "range": {
                            "height": {
                                "gte": data.heightMin,
                                "lte": data.heightMax
                            }
                        }
                    },
                    {
                        "range": {
                            "dob": {
                                "gte": data.dobMin,
                                "lte": data.dobMax
                            }
                        }
                    }
                ],
                //"must_not": data.must_not,
                "must_not": data.must_not,
                "filter": {
                    "geo_distance": {
                        "distance": data.distanceMax,
                        "location": {
                            "lat": data.latitude,
                            "lon": data.longitude
                        }
                    }
                }
            },

        },
        "sort": [
            { "boostTimeStamp": "asc" },
            {
                "_geo_distance": {
                    "location": {
                        "lat": data.latitude,
                        "lon": data.longitude
                    },
                    "order": "asc",
                    "unit": "km",
                    "distance_type": "plane"
                }
            },

        ],
        "_source": [
            "firstName", "contactNumber", "gender", "registeredTimestamp",
            "profilePic", "otherImages", "email", "profileVideo", "dob", "about",
            "instaGramProfileId", "onlineStatus", "height", "location", "firebaseTopic",
            "deepLink", "profileVideoThumbnail", "myPreferences", "dontShowMyAge", "dontShowMyDist",
            "profileVideoWidth", "profileVideoHeight", "boostTimeStamp"
        ]
    }

    logger.silly("condition sdsds : ", JSON.stringify(condition))
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: condition
    }, function (err, result) {
        callback(err, result);
    });
} */


function findMatch(data, callback) {
    let condition = {
        "from": data.skip, "size": data.limit,
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "gender": data.gender
                        }
                    },
                    {
                        "range": {
                            "height": {
                                "gte": data.heightMin,
                                "lte": data.heightMax
                            }
                        }
                    },
                    {
                        "range": {
                            "dob": {
                                "gte": data.dobMin,
                                "lte": data.dobMax
                            }
                        }
                    }
                ],

                "must_not": data.must_not,
                "filter": [
                    {
                        "geo_distance": {
                            "distance": data.distanceMax,
                            "location": {
                                "lat": data.latitude,
                                "lon": data.longitude
                            }
                        }
                    },
                    {
                        "bool": {
                            //"should": []
                            "should": data.prefData

                        }
                    }

                ]
            }
        },
        "sort": [
            { "boostTimeStamp": "asc" },
            {
                "_geo_distance": {
                    "location": {
                        "lat": data.latitude,
                        "lon": data.longitude
                    },
                    "order": "asc",
                    "unit": "km",
                    "distance_type": "plane"
                }
            },

        ],
        "_source": [
            "firstName", "contactNumber", "gender", "registeredTimestamp",
            "profilePic", "otherImages", "email", "profileVideo", "dob", "about",
            "instaGramProfileId", "onlineStatus", "height", "location", "firebaseTopic", "photosPrivate",
            "deepLink", "profileVideoThumbnail", "myPreferences", "dontShowMyAge", "dontShowMyDist",
            "profileVideoWidth", "profileVideoHeight", "boostTimeStamp"
        ]
    }

    logger.silly("condition : ", JSON.stringify(condition))
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: condition
    }, function (err, result) {
        callback(err, result);
    });
}
function Select(data, callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            "query": {
                "match": data
            }
        }
    }, function (err, result) {
        callback(err, result);
    });
}
function SelectVoteFromuserpairdetails(data, callback) {
    elasticClient.get().search({
        index: process.env.ELASTICK_INDEX_PAIRZ,
        type: "_doc",
        body: {
            "query": {
                "bool": {
                    "should": data
                }
            }
        }
    }, function (err, result) {
        callback(err, result);
    });
}

function SelectCoinrewardUser(condition, callback) {
    elasticClient.get().search({
        index: process.env.ELASTICK_INDEX_PAIRZ,
        type: "_doc",
        body: {
            "query": {
                "bool": {
                    "must": condition
                }
            }
        }
    }, function (err, result) {
        callback(err, result);
    });
}
function UpdateInUserPairDetail(_id, data, callback) {

    elasticClient.get().update({
        index: process.env.ELASTICK_INDEX_PAIRZ,
        type: "_doc",
        id: _id,
        retry_on_conflict: 5,
        body: {
            doc: data,
            doc_as_upsert: true
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function UpdateWithPushIncCoinCount(_id, callback) {
    var condition = { "script": "if( ctx._source.containsKey(\"coinEarnByVote\") ){ ctx._source.coinEarnByVote += 100; } else { ctx._source.coinEarnByVote = 100; } if( ctx._source.containsKey(\"userpairCount\") ){ ctx._source.userpairCount += 1; } else { ctx._source.userpairCount = 1; }" }
    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: condition
    }, (err, results) => {
        callback(err, results)
    })
}


function SelectAll(callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: 0, size: 200,
            "query": {
                "match_all": {}
            }
        }
    }, function (err, result) {
        callback(err, result);
    });
}

function CustomeSelect(data, callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: data
    }, function (err, result) {
        callback(err, result);
    });
}
function Insert(data, callback) {
    let _id = "" + data._id;
    delete data._id;
    elasticClient.get().index({
        index: indexName,
        type: tablename,
        id: _id,
        body: data
    }, (err, result) => {
        callback(err, result);
    });
}

function UpdateWithPush(_id, field, value, callback) {


    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            "script": "ctx._source." + field + ".add('" + value + "')"
        }

    }, (err, results) => {
        callback(err, results)
    })
}

function UpdateWithPull(_id, field, value, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            "script": "ctx._source." + field + ".remove(ctx._source." + field + ".indexOf('" + value + "'))"
        }
    }, (err, results) => {
        callback(err, results);
    })
}

function Update(_id, data, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            doc: data,
            doc_as_upsert: true
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function updateByQuery(condition, fieldName, fieldValue, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            query: { "match": condition },
            "script": { "source": `ctx._source.${fieldName}=${fieldValue}` }
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function updateByQuery(condition, fieldName, fieldValue, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            query: { "match": condition },
            "script": { "source": `ctx._source.${fieldName}=${fieldValue}` }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function removeField(_id, fieldName, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            "script": `ctx._source.remove("${fieldName}")`,
            "query": {
                "bool": {
                    "must": [
                        {
                            "exists": {
                                "field": `${fieldName}`
                            }
                        }
                    ]
                }
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function setOffline(timestamp, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        conflicts: "proceed",
        body: {
            query: {
                "range": {
                    "lastOnline": {
                        "lt": timestamp
                    }
                },
                // "match":{"onlineStatus":1}
            },
            "script": { "source": "ctx._source.onlineStatus=0" }
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function Delete(tablename, condition, callback) {
    elasticClient.get().deleteByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {

            query: {
                match: condition
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function DeleteUser(id, callback) {
    elasticClient.get().deleteByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {

            "query": {
                "match": {
                    "_id": id
                }
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function DeletePairData(dt, callback) {
    elasticClient.get().deleteByQuery({
        index: process.env.ELASTICK_INDEX_PAIRZ,
        type: "_doc",
        version: version,
        body: {
            "query": {
                "bool": {
                    "should": dt
                }
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function Bulk(dataInArray, callback) {
    elasticClient.get().bulk({
        body: dataInArray
    }, (err, results) => {
        callback(err, results)
    })
}
function updateByQueryWithArray(condition, callback) {
    elasticClient.updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: condition
    }, (err, results) => {
        callback(err, results)
    })
}

module.exports = {
    updateByQueryWithArray, setOffline, Bulk, removeField, UpdateInUserPairDetail,DeletePairData,DeleteUser,
    CustomeSelect, Select, Insert, Update, updateByQuery, Delete, findMatch, SelectVoteFromuserpairdetails,
    UpdateWithPush, UpdateWithPull, SelectAll, SelectCoinrewardUser, UpdateWithPushIncCoinCount,
};