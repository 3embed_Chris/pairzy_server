'use strict'

const db = require('../mongodb')
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;

const tablename = 'userPost';
function Select(data, callback) {
    db.get().collection(tablename)
        .find(data)

        .toArray((err, result) => {
            return callback(err, result);
        });

}

function SelectWithSort(data, sortBy = {}, porject = {}, skip = 0, limit = 20, callback) {
    db.get().collection(tablename)
        .find(data)
        .sort(sortBy)
        .project(porject)
        .skip(skip)
        .limit(limit)
        .toArray((err, result) => {
            return callback(err, result);
        });

}
function matchList(data, callback) {
    db.get().collection(tablename)
        .aggregate(data, (err, result) => {
            return callback(err, result);
        });
}

function SelectOne(data, callback) {
    db.get().collection(tablename)
        .findOne(data, (err, result) => {
            return callback(err, result);
        });
}


function SelectById(condition, requiredFeild, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .findOne(condition, requiredFeild, ((err, result) => {
            return callback(err, result);
        }));
}

function SelectByIdPost(condition, requiredFeild,sort, callback) {
    db.get().collection(tablename)
        .find(condition, requiredFeild,sort).toArray((err, result) => {
            return callback(err, result);
        });
}


function Insert(data, callback) {
    db.get().collection(tablename)
        .insert(data, (err, result) => {
            return callback(err, result);
        });
}
function Update(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $set: data }, { multi: true }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateWithIncrease(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $inc: data }, (err, result) => {
            return callback(err, result);
        });
}


function UpdateById(_id, data, callback) {
    db.get().collection(tablename)
        .update({ _id: ObjectID(_id) }, { $set: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateByIdWithAddToSet(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $addToSet: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateByIdWithPush(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $push: data }, (err, result) => {
            return callback(err, result);
        });
}
function UpdateByData(condition, data, callback) { 
    db.get().collection(tablename)
        .update({_id:ObjectID(condition)},  data , (err, result) => {
            return callback(err, result);
        });
}
function UpdateByIdWithPull(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $pull: data }, (err, result) => {
            return callback(err, result);
        });
}

function Delete(condition, callback) {
    db.get().collection(tablename)
        .remove(condition, (err, result) => {
            return callback(err, result);
        });
}
function Aggregate(condition, callback) {
    db.get().collection(tablename)
        .aggregate(condition, (err, result) => {
            return callback(err, result);
        });
}

function Unset(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $unset: data }, (err, result) => {
            return callback(err, result);
        });
}


module.exports = {
    Aggregate, SelectWithSort, Unset, UpdateWithIncrease,UpdateByData,SelectByIdPost,
    Select, matchList, SelectOne, Insert, Update, SelectById, UpdateById,
    Delete, UpdateByIdWithAddToSet, UpdateByIdWithPush, UpdateByIdWithPull
};