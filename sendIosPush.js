const apn = require("apn");
var objectId = require("mongodb").ObjectID;
var middleWare = require("./Controller/dbMiddleware.js");
var messageTypes = ["text", "image", "video", "location", "concat", "audio", "sticker", "doodle", "gify", "document"];

let options = {
    cert: "./iosCertificate/voIpNotification/certificate.pem",
    key: "./iosCertificate/voIpNotification/key.pem",
    production: true
};

var sendOn = {};

sendOn.iosPushIfRequiredOnAudioCall = function iosPushIfRequiredOnAudioCall(dataInJSON) {
    try {
        console.log("In iosPushIfRequiredOnAudioCall." + dataInJSON.targetId)
        middleWare.Select("userList", { _id: objectId(dataInJSON.targetId) }, "Mongo", {}, function (err, result) {
            // if (result[0] && result[0].iosAudioCallPush) {

            options = {
                cert: "./iosCertificate/voIpNotification/certificate.pem",
                key: "./iosCertificate/voIpNotification/key.pem",
                production: false
            };

            let service = new apn.Provider(options);
            let note = new apn.Notification({
                alert: "get new massage from voip",
                payload: { data: dataInJSON },
                "content-available": 1
                // sound: "ringtone.wav",
                // topic: "com.pipelineAI.pairzy"
            });
            console.log("result[0].dataInJSON ", dataInJSON);
            console.log("result[0].voipiospush ", result[0].voipiospush);

            service.send(note, result[0].voipiospush).then(result => {
                console.log("sent:", result.sent.length);
                console.log("failed:", result.failed.length);
                console.log(result.failed);
            });

            service.shutdown();
            // }
        })
    } catch (error) {
        console.log("Exception : ", error)
    }

}
sendOn.iosPushIfRequiredOnVideoCall = function iosPushIfRequiredOnVideoCall(dataInJSON) {
    try {
        console.log("In MONGODB_URL." + process.env.MONGODB_URL)
        console.log("In iosPushIfRequiredOnVideoCall." + dataInJSON.targetId)
        middleWare.Select("userList", { _id: objectId(dataInJSON.targetId), "currentLoggedInDevices.deviceType": "1" }, "Mongo", {}, function (err, result) {
            if (result[0] && result[0].apnsPush) {
                options = {
                    cert: "./iosCertificate/apnsNotification/certificate.pem",
                    key: "./iosCertificate/apnsNotification/key.pem",
                    production: false
                };

                let service = new apn.Provider(options);
                let note = new apn.Notification({
                    alert: dataInJSON["message"].alert,
                    payload: { data: dataInJSON },
                    sound: "ringtone.wav",
                    topic: "com.3embed.datumdev"
                });
                console.log("result[0].dataInJSON ", dataInJSON)
                console.log("result[0].apnsPush ", result[0].apnsPush)
                service.send(note, result[0].apnsPush).then(result => {
                    console.log("sent:", result.sent.length);
                    console.log("failed:", result.failed.length);
                    console.log(result.failed);
                });

                service.shutdown();
            }
        })
    } catch (error) {
        console.log("Exception : ", error)
    }

}
sendOn.iosPushIfRequiredOnMessage = function iosPushIfRequiredOnMessage(dataInJSON) {
    try {

        console.log("In iosPushIfRequiredOnMessage.")
        dataInJSON["thumbnail"] = dataInJSON["payload"];
        middleWare.Select("userList", { _id: objectId(dataInJSON.targetId), "currentLoggedInDevices.deviceType": "1" }, "Mongo", {}, function (err, result) {
            let isInactiveNotification = 0;
            let bage = 0;
            try {
                bage = (result[0].bage) ? result[0].bage + 1 : 1;
                let _id = (dataInJSON.userId) ? dataInJSON.userId : dataInJSON.groupId;
                isInactiveNotification = (result && result[0]["inActiveUsersNotification"] != "undefine" &&
                    result[0]["inActiveUsersNotification"].map(id => id.toString()).includes(_id)) ? 1 : 0;

            } catch (error) {

            }

            if (result[0] && !result[0].onlineStatus) {
                middleWare.Update("userList", { "bage": bage }, { _id: objectId(result[0]._id.toString()) }, "Mongo", () => { });
            }
            if (result[0] && result[0].iosVideoCallPush && !isInactiveNotification) {
                options = {
                    cert: "./iosCertificate/apnsNotification/certificate.pem",
                    key: "./iosCertificate/apnsNotification/key.pem",
                    production: false
                };
                options.production = result[0].isdevelopment || true;
                let service = new apn.Provider(options);
                var b64string = dataInJSON["message"].msg;
                var buf = b64string;
                try {
                    if (dataInJSON.type != 0) {
                        buf = messageTypes[dataInJSON.type];
                    } else {
                        buf = Buffer.from(b64string, 'base64')
                    }
                } catch (error) {
                    console.log("error ", error);
                }
                let note = new apn.Notification({
                    // alert: dataInJSON["message"].alert,
                    alert: { "title": dataInJSON["message"].alert, "body": "" + buf },
                    payload: { data: dataInJSON },
                    badge: bage,
                    sound: "sms-received.wav",
                    topic: "com.3embed.datumdev",
                    "thread-id": "123"
                });
                service.send(note, result[0].apnsPush).then(result => {
                    console.log("iosPushIfRequiredOnMessage sent:", result.sent.length);
                    console.log("failed:", result.failed.length);
                    console.log(result.failed);
                });

                service.shutdown();
            }
        })
    } catch (error) {
        console.log("Exception : ", error)
    }

}
sendOn.iosPushCutCall = function iosPushCutCall(dataInJSON) {
    try {
        console.log("In iosPushCutCall.")
        middleWare.Select("userList", { _id: objectId(dataInJSON.targetId), "currentLoggedInDevices.deviceType": 1 }, "Mongo", {}, function (err, result) {
            if (result[0] && result[0].iosVideoCallPush) {
                options = {
                    cert: "./iosCertificate/apnsNotification/certificate.pem",
                    key: "./iosCertificate/apnsNotification/key.pem",
                    production: false
                };
                options.production = result[0].isdevelopment || true;
                let service = new apn.Provider(options);
                let note = new apn.Notification({
                    alert: dataInJSON["message"].alert,
                    payload: { data: dataInJSON },
                    sound: "defualt",
                    topic: "com.3embed.datumdev"
                });
                service.send(note, result[0].iosVideoCallPush).then(result => {
                    console.log("sent:", result.sent.length);
                    console.log("failed:", result.failed.length);
                    console.log(result.failed);
                });

                service.shutdown();
            }
        })
    } catch (error) {
        console.log("Exception : ", error)
    }

}

module.exports = sendOn