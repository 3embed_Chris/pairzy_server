'use strict'
var Joi = require('joi');
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const planSubscription = require("../../../models/subscription")

let handler = (req, res) => {

    let _id = req.auth.credentials._id;

    getMatchdata()
        .then(function (value) {
            return res({ message: req.i18n.__('GetActivePlan')['200'], data: value }).code(200);
        })
        .catch(function (err) {
            logger.error(err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(412);
        });

    function getMatchdata() {
        return new Promise(function (resolve, reject) {
            planSubscription.SelectWithSort({ userId: ObjectID(_id) }, { _id: -1 }, {}, 0, 1, (err, result) => {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                return res({ message: req.i18n.__('GetActivePlan')['200'], data: result[0] || [] }).code(200);
            })
        });
    }
};
module.exports = { handler }