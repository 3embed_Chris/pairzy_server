'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const askAQuestion = require('../../../models/askAQuestion');
const mailgun = require('../../../library/mailgun');

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;

    setInMongo()
        .then(function (value) {
            return res({ message: req.i18n.__('PostAskAQuestion')['200'] }).code(200);
        })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });

    function setInMongo() {
        return new Promise(function (resolve, reject) {
            let dataToSend = {
                "from": `${req.user.firstName} <${req.user.email}>`,
                "to": process.env.MAILGUN_FROM,
                "h:Reply-To": req.user.email,
                "subject": "Make a Suggestion",
                "text":`From:${req.user.email}  
                ${req.payload.description} `
            };
            mailgun.sendMail(dataToSend, (err, result) => {
                // console.log("errrrrrrrr", err);
                //console.log("resssssssssssssss", result);


                logger.error((err) ? err : "")
            })
            let dataToInsert = {
                userId: ObjectID(_id),
                question: req.payload.description
            }
            askAQuestion.Insert(dataToInsert, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                }
                else {
                    return resolve(result);
                }
            });
        });
    }
};

let validator = Joi.object({
    description: Joi.string().required().description("ex :  is RahulXX a spam user ?").example("is Rahul a spam user").error(new Error('description is missing')),
}).required();

let response = {
    status: {
        200: { message: Joi.any().default(local['PostAskAQuestion']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { APIHandler, validator, response }