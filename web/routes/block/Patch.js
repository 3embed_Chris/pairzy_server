'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const dateSchedule = require('../../../models/dateSchedule');
const userListType = require('../../../models/userListType');
const userListCollection = require('../../../models/userList');
const userMatchCollection = require("../../../models/userMatch");
const userBlocksCollection = require('../../../models/userBlocks');
const mqttClient = require("../../../library/mqtt");

let validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId").error(new Error('targetUserId is missing'))
}).required();

/**
 * @method PATCH block
 * @description This API use to block a user.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} lang targetUserId
 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let targetUserId = req.payload.targetUserId;

    if (_id === targetUserId) {
        return res({ message: req.i18n.__('PatchBlock')['422'] }).code(422);
    }

    let condition = { _id: ObjectID(_id), myBlock: ObjectID(targetUserId) };
    userListCollection.Select(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result[0]) {
            return res({ message: req.i18n.__('PatchBlock')['200'] }).code(200);
        } else {
            targetUserExists()
                .then(function (value) {
                    return processAPI();
                }).then(function (value) {
                    return res({ message: req.i18n.__('PatchBlock')['200'] }).code(200);
                }).catch(function (err) {
                    return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                });
        }
    });

    function targetUserExists() {
        return new Promise(function (resolve, reject) {
            userListCollection.SelectById({ _id: targetUserId }, { firstName: 1, profilePic: 1, currentLoggedInDevices: 1 }, (err, result) => {
                if (result._id) {
                    resolve(result);
                } else {
                    reject(new Error('Ooops, something broke!'));
                }
            });
        });
    }

    function processAPI() {
        return new Promise(function (resolve, reject) {
            mqttClient.publish(targetUserId, JSON.stringify({ "messageType": "block", userId: _id }), { qos: 0 }, (e, r) => { if (e) logger.error("OAKSOAKS  : ", e) })
            /**Update In elastic Search */
            userListType.UpdateWithPush(targetUserId, "blockedBy", _id, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });
            userListType.UpdateWithPush(_id, "myBlock", targetUserId, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });
            /**
             **********************************************
             * remove if match
             **********************************************
             */
            // userListType.UpdateWithPull(targetUserId, "matchedWith", _id, (err, result) => {
            //     if (err) return reject(new Error('Ooops, something broke!'));
            // });
            // userListType.UpdateWithPull(_id, "matchedWith", targetUserId, (err, result) => {
            //     if (err) return reject(new Error('Ooops, something broke!'));
            // });

            /**Update in MongoDB */
            userListCollection.UpdateByIdWithAddToSet({ _id: targetUserId }, { "blockedBy": ObjectID(_id) }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });
            userListCollection.UpdateByIdWithAddToSet({ _id: _id }, { "myBlock": ObjectID(targetUserId) }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });
            /**
            **********************************************
            * remove if match
            **********************************************
            */
            // userListCollection.UpdateByIdWithPull({ _id: targetUserId }, { "matchedWith": ObjectID(_id) }, (err, result) => {
            //     if (err) return reject(new Error('Ooops, something broke!'));
            // });
            // userListCollection.UpdateByIdWithPull({ _id: _id }, { "matchedWith": ObjectID(targetUserId) }, (err, result) => {
            //     if (err) return reject(new Error('Ooops, something broke!'));
            // });
            let condition = {
                "$or": [
                    { "initiatedBy": ObjectID(_id), "opponentId": ObjectID(targetUserId) },
                    { "initiatedBy": ObjectID(targetUserId), "opponentId": ObjectID(_id) }
                ]
            };

            let data = { "unMatchedBy": ObjectID(_id), "unMatchedTimestamp": new Date().getTime() };
            userMatchCollection.Update(condition, data, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });


            let conditionDeactivate = { _id: _id, reason: "Bloack" };
            dateSchedule.deActivateDates(conditionDeactivate, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });

            let dataToInsert = {
                userId: ObjectID(_id),
                targetUserId: ObjectID(targetUserId),
                timestamp: new Date().getTime()
            }
            userBlocksCollection.Insert(dataToInsert, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
                return resolve("---");
            });
        });
    }
};


let response = {
    status: {
        200: { message: Joi.any().default(local['PatchBlock']['200']) },
        422: { message: Joi.any().default(local['PatchBlock']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { APIHandler, validator, response }