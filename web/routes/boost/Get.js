'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userListCollection = require('../../../models/userList');

/**
 * @method PATCH boost
 * @description This API use to rewind a user.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} lang targetUserId
 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */

let APIHandler = (req, res) => {
    try {
        var _id = req.auth.credentials._id;

        getBoostData()
            .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
            .catch((err) => {
                logger.error(err);
                return res({ message: err.message }).code(err.code);
            });

    } catch (error) {
        logger.error("in Get Boost : ", error);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }

    function getBoostData() {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));

                return (result && result.boost)
                    ? resolve({ code: 200, message: req.i18n.__('GetBoost')['200'], data: result.boost })
                    : reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
            });
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['GetBoost']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}
module.exports = { APIHandler, response }