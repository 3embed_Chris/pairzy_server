'use strict'
const Joi = require("joi");
const moment = require("moment");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const cronJobTask = require('../../../cronJobTask');
const userListType = require('../../../models/userListType');
const userListCollection = require('../../../models/userList');
const coinConfigCollection = require('../../../models/coinConfig');
const coinWalletCollection = require('../../../models/coinWallet');
const userBoost = require('../../../models/userBoost');
const redisClient = require('../../../models/redisDB').client;
const walletCustomerCollection = require('../../../models/walletCustomer');


/**
 * @method PATCH boost
 * @description This API use to rewind a user.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} lang targetUserId
 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */

let APIHandler = (req, res) => {
    try {
        var _id = req.auth.credentials._id;
        var timestamp = new Date().getTime();
        var trigger = "boostProfileForADay";
        var data = {};
        var expire = moment();

        getRequiredCoinForTrigger()
            .then((value) => { return getUsersCoinFromCoinWalletAndValidate(value); })
            .then(() => { return boostThisProfile(); })
            .then(() => { return insertInWalletCusetomerAndUpdateInCoinWallet(); })
            .then(() => { return res({ message: req.i18n.__('PatchBoost')['200'], coinWallet: data, expire: expire }).code(200); }
            ).catch((err) => {
                logger.error(err);
                return res({ message: err.message }).code(err.code);
            });

    } catch (error) {
        logger.error("in PatchBoost : ", error);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }

    function getRequiredCoinForTrigger() {
        return new Promise((resolve, reject) => {
            coinConfigCollection.SelectOne({}, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));

                return resolve(result[trigger] || {});
            });
        });
    }
    function getUsersCoinFromCoinWalletAndValidate(requiredCoin) {
        return new Promise((resolve, reject) => {
            coinWalletCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));

                for (let key in requiredCoin) {
                    if (requiredCoin[key] == 0 || (result && result["coins"][key] && requiredCoin[key] <= result["coins"][key])) {
                    } else {
                        return reject({ code: 402, message: req.i18n.__('genericErrMsg')['402'] });
                    }
                }
                data["coinWallet"] = result["coins"] || {};
                data["requiredCoin"] = requiredCoin;
                return resolve(true);
            });
        });
    }
    function boostThisProfile() {
        return new Promise(function (resolve, reject) {


            expire = expire.add(cronJobTask.boostTime, "ms").valueOf()
            let dataToInsert = {
                "_id": new ObjectID(),
                "isActive": 1,
                "userId": ObjectID(_id),
                "start": new Date().getTime(),
                "expire": expire,
                "count": { "likes": 0, "disLikes": 0, "supperLikes": 0, "match": 0, "unmatch": 0, "views": 0 },
                "likes": [],
                "disLikes": [],
                "supperLikes": [],
                "match": [],
                "unmatch": [],
                "views": []
            }
            userBoost.Insert(dataToInsert, (err, result) => {
                if (err) logger.error("insert in userBoost ", err);
            })

            let dataToUpdate = { "boostTimeStamp": dataToInsert.start };
            userListType.Update(_id, dataToUpdate, (err, result) => {
                if (err) logger.error("update in userList ES ", err);
            });

            dataToUpdate = {
                "boost":
                {
                    "_id": dataToInsert._id, "start": dataToInsert.start, "expire": expire,
                    "likes": 0, "disLikes": 0, "supperLikes": 0, "match": 0, "unmatch": 0, "views": 0
                }
            };
            userListCollection.UpdateById(_id, dataToUpdate, (err, result) => {
                if (err) logger.error("update in userList ES ", err);
            });

            //for remove boostTimeStamp from ES db
            redisClient.hmset(cronJobTask.redisKeyForBoost + _id, { _id: _id }, (e, r) => {
                if (e) logger.error("at set hmset in redis with " + cronJobTask.redisKeyForBoost + _id, e);
                console.log("expire ", (expire / 1000))
                redisClient.expire(cronJobTask.redisKeyForBoost + _id, cronJobTask.boostTime / 1000, (e, r) => {
                    if (e) logger.error("at set hmset  expire in redis with " + cronJobTask.redisKeyForBoost + _id, e);
                });
            });
            return resolve(true);
        });
    }
    function insertInWalletCusetomerAndUpdateInCoinWallet() {
        return new Promise((resolve, reject) => {
            let coinWallet = data.coinWallet;
            let requiredCoin = data.requiredCoin;

            let insertData = [];
            let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
            for (let key in requiredCoin) {

                if (coinWallet[key]) {
                    insertData.push({
                        "txnId": txnId,
                        "userId": ObjectID(_id),
                        "docType":"on boost",
                        "docTypeCode":5,
                        "txnTypeCode": 2,
                        "txnType": "DEBIT",
                        "trigger": `Spent ${requiredCoin[key]} coins to boost profile for "${((cronJobTask.boostTime / 1000) / 60)}" minutes`,
                        "currency": "N/A",
                        "currencySymbol": "N/A",
                        "coinType": key,
                        "coinOpeingBalance": coinWallet[key],
                        "cost": 0,
                        "coinAmount": requiredCoin[key],
                        "coinClosingBalance": (coinWallet[key] - requiredCoin[key]),
                        "paymentType": "N/A",
                        "timestamp": timestamp,
                        "transctionTime": timestamp,
                        "transctionDate": timestamp,
                        "paymentTxnId": "N/A",
                        "initatedBy": "customer"
                    });
                    coinWallet[key] = coinWallet[key] - requiredCoin[key];
                }
            }
            walletCustomerCollection.InsertMany(insertData, () => { });
            coinWalletCollection.Update({ _id: ObjectID(_id) }, { "coins": coinWallet }, () => { });
            data = coinWallet;
            return resolve("---");
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PatchBoost']['200']), coinWallet: Joi.any(), expire: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        402: { message: Joi.any().default(local['genericErrMsg']['402']) },
    }
}
module.exports = { APIHandler, response }