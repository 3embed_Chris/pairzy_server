'use strict'
let headerValidator = require('../../middleware/validator');
let patchAPI = require('./Patch');
let GetAPI = require('./Get');
let GetWithLikeAPI = require('./GetWithLike');
let GetWithSupperLikeAPI = require('./GetWithSupperLike');

module.exports = [
    {
        method: 'Get',
        path: '/boostWithSupperLike',
        handler: GetWithSupperLikeAPI.APIHandler,
        config: {
            description: 'This API will be used to boost user',
            tags: ['api', 'boost'],
            auth: 'userJWT',
            response: GetWithSupperLikeAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
    {
        method: 'Get',
        path: '/boostWithLike',
        handler: GetWithLikeAPI.APIHandler,
        config: {
            description: 'This API will be used to boost user',
            tags: ['api', 'boost'],
            auth: 'userJWT',
            response: GetWithLikeAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
    {
        method: 'Get',
        path: '/boostWithView',
        handler: GetWithLikeAPI.APIHandler,
        config: {
            description: 'This API will be used to boost user',
            tags: ['api', 'boost'],
            auth: 'userJWT',
            response: GetWithLikeAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
    {
        method: 'PATCH',
        path: '/boost',
        handler: patchAPI.APIHandler,
        config: {
            description: 'This API will be used to boost user',
            tags: ['api', 'boost'],
            auth: 'userJWT',
            response: patchAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
    {
        method: 'Get',
        path: '/boost',
        handler: GetAPI.APIHandler,
        config: {
            description: 'This API will be used to boost user',
            tags: ['api', 'boost'],
            auth: 'userJWT',
            response: GetAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];