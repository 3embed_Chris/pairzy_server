'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
const local = require('../../../locales');
const messagesCollection = require('../../../models/messages');
const userListCollection = require('../../../models/userList');
const chatLikesCollection = require('../../../models/chatList');
const userMatchCollection = require('../../../models/userMatch');



let validator = Joi.object({
    targetUserId: Joi.string().required().min(24).max(24).description("targetUserId").error(new Error('targetUserId is missing'))
}).required();
/**
 * @method POST like
 * @description This API use to like a user.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId,Eg. 5a14013b7b334c19349aec7b
 */
let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let targetUserId = req.payload.targetUserId;

    targetUserExists()
        .then(function (value) { return processToMatch(); })
        .then(function (value) {
            return res({ message: req.i18n.__('PostChatWithoutMatch')['200'], data: value }).code(200);
        }).catch(function (err) {
            logger.silly('Caught an error!', err);
            return res({ message: req.i18n.__('PostLike')['412'] }).code(412);
        });

    function targetUserExists() {
        return new Promise(function (resolve, reject) {
            userListCollection.SelectById({ _id: targetUserId }, { _id: 1, firstName: 1, profilePic: 1, currentLoggedInDevices: 1 }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result) {
                    return resolve(result);
                } else {
                    userListType.Delete("userList", { _id: targetUserId }, () => { });
                    return reject(new Error('Ooops, something broke! userNot found' + targetUserId));
                }
            });
        });
    }
    function processToMatch() {
        return new Promise(function (resolve, reject) {

            let chatId = new ObjectID();
            let msg_id = new ObjectID();
            userListCollection.SelectById({ _id: _id }, { _id: 1, firstName: 1, profilePic: 1 }, (err, result) => {

                if (err) return reject(new Error('Ooops, something broke!'));

                let dataToInsert = {
                    "_id": chatId,
                    "message": [msg_id],
                    "members": {
                        [_id]: {
                            "status": "NormalMember"
                        },
                        [targetUserId]: {
                            "status": "NormalMember"
                        }
                    },
                    "initiatedBy": ObjectID(_id),
                    "createdAt": new Date().getTime(),
                    "chatType": "NormalChat",
                    "secretId": "",
                    "isMatchedUser": 0,
                    creationTs: new Timestamp(),
                    creationDate: new Date()
                };
                chatLikesCollection.Insert(dataToInsert, (e, r) => { });
                /** create dummy message */
                let newMessage = {
                    "_id": msg_id,
                    "messageId": "" + new Date().getTime(),
                    "secretId": "",
                    "dTime": -1,
                    "senderId": ObjectID(_id),
                    "receiverId": ObjectID(targetUserId),
                    "payload": "3embed test",
                    "messageType": "0",
                    "timestamp": new Date().getTime(),
                    "expDtime": 0,
                    "chatId": chatId,
                    "userImage": result.profilePic,
                    "toDocId": "test message",
                    "name": result.firstName,
                    "dataSize": null,
                    "thumbnail": null,
                    "mimeType": null,
                    "extension": null,
                    "fileName": null,
                    "isWithoutMatch": true,
                    "members": {
                        [_id]: {

                        },
                        [targetUserId]: {
                            "status": 0
                        }
                    },
                    creationTs: new Timestamp(),
                    creationDate: new Date(),
                }
                messagesCollection.Insert(newMessage, (e, r) => { })
                return resolve({ chatId: chatId });
            });
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PostChatWithoutMatch']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { APIHandler, validator, response }