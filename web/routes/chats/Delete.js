'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const chatListCollection = require('../../../models/chatList');
const messagesCollection = require("../../../models/messages");

let validator = Joi.object({
    chatId: Joi.string().min(24).max(24).required().description("targetUserId").error(new Error('chatId is missing'))
}).required();

/**
 * @method PATCH block
 * @description This API use to block a user.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} lang targetUserId
 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let chatId = req.payload.chatId;


    function deleteAllChatFromChatList() {
        return new Promise(function (resolve, reject) {
            //let condition = { "_id": ObjectID(chatId), ["members." + _id]: { $exists: true } };
           // let dataToUpdate = { ["members." + _id + ".inactive"]: true };
            let condition = {"chatId": ObjectID(chatId)};

            messagesCollection.SelectForDelete(condition, (err, result) => {
                //console.log("=========>",result)
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                } else {
                    return resolve(result);
                }
            })
        });
    }
    function deleteAllMessages(result) {
       // console.log("main111111111111111",result[0])

       var ids= Object.keys(result[0].members)

       console.log("mainnnnnnnnnnnnnnnnnnnnnn",ids[0],ids[1])


        return new Promise(function (resolve, reject) {
            let condition = { "chatId": ObjectID(chatId),"payload":{'$ne':"3embed test"} };
            let dataToUpdate = { 
                ["members." + ids[0] + ".del"]: true,
                ["members." + ids[1] + ".del"]: true

        };

            messagesCollection.Update(condition, dataToUpdate, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                } else {
                    return resolve(true);
                }
            })
        });
    }

    deleteAllChatFromChatList()
        .then((data) => {
            return deleteAllMessages(data);
        }).then((data) => {
            return res({ message: req.i18n.__('DeleteChat')['200'] }).code(200);
        }).catch((data) => {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });
};

let response = {
    status: {
        200: { message: Joi.any().default(local['DeleteChat']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}

module.exports = { APIHandler, validator, response }