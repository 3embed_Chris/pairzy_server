'use strict'
var conf = process.env;
var Joi = require('joi');
var async = require("async");
var jsonwebtoken = require('jsonwebtoken');
var ObjectID = require('mongodb').ObjectID

const local = require('../../../locales');
const mqtt = require("../../../library/mqtt");
const userListCollection = require('../../../models/userList');
const chatListCollection = require('../../../models/chatList');

var secretKey = conf.SECRET_KEY;

let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let JWTDecoded = { userId: _id };
    async.waterfall([

        function (mainFuncCB) {

            var limit = 100, skip = 0;
            if (req.params.pageNo.toString() != "") {
                skip = req.params.pageNo * 100;
                limit = 100 + skip;
            }
            var userID = "members." + JWTDecoded.userId;
            var chatListAggr = [
                { "$match": { [userID]: { "$exists": true }, [userID + ".inactive"]: { "$exists": false }, isUnMatched: { "$ne": 1 }, isDeleted: { "$ne": 1 } } },
                {
                    "$project": {
                        "id": 1, "initiatedBy": 1, "chatMembers": "$members", "subject": 1, "isMatchedUser": 1,
                        "image": 1, "messageDBId": { $slice: ["$message", -1] }, "chatType": 1, "boostByUser": 1
                    }
                },
                { "$unwind": "$messageDBId" },
                {
                    "$lookup": {
                        "from": "messages", "localField": "messageDBId", "foreignField": "_id", as: "messageDetails"
                    }
                },
                { "$unwind": "$messageDetails" },
                {
                    "$project": {
                        "initiatedBy": 1, "chatMembers": 1, "subject": 1, "image": 1, "chatType": 1, "chatId": "$messageDetails.chatId", "secretId": "$messageDetails.secretId", "dTime": "$messageDetails.dTime", "messageId": "$messageDetails.messageId",
                        "payload": "$messageDetails.payload", "memberId": "$messageDetails.memberId", "memberIdentifier": "$messageDetails.memberIdentifier", "messageType": "$messageDetails.messageType", "timestamp": "$messageDetails.timestamp",
                        "previousReceiverIdentifier": "$messageDetails.previousReceiverIdentifier", "previousFrom": "$messageDetails.previousFrom", "previousPayload": "$messageDetails.previousPayload",
                        "previousType": "$messageDetails.previousType", "previousId": "$messageDetails.previousId", "replyType": "$messageDetails.replyType", "previousFileType": "$messageDetails.previousFileType",
                        "receiverId": "$messageDetails.receiverId", "members": "$messageDetails.members", "senderId": "$messageDetails.senderId",
                        "initiatorId": "$messageDetails.initiatorId", "initiatorIdentifier": "$messageDetails.initiatorIdentifier",
                        "targetUserId": { "$cond": { if: { $eq: ["$messageDetails.senderId", ObjectID(JWTDecoded.userId)] }, then: "$messageDetails.receiverId", else: "$messageDetails.senderId" } },
                        "isMatchedUser": 1, "boostByUser": 1
                    },
                },
                {
                    "$lookup": {
                        "from": "userList", "localField": "targetUserId", "foreignField": "_id", as: "targetDetails"
                    }
                },
                { "$unwind": "$targetDetails" },
                {
                    "$project": {
                        "_id": 0, "initiatedBy": 1, "chatMembers": 1, "subject": 1, "image": 1, "chatType": 1, "chatId": 1, "secretId": 1, "dTime": 1, "messageId": 1, "payload": 1, "members": 1, "messageType": 1, "timestamp": 1, "senderId": 1, "receiverId": 1,
                        "previousReceiverIdentifier": 1, "previousFrom": 1, "previousPayload": 1, "previousType": 1, "previousId": 1, "replyType": 1, "previousFileType": 1,
                        "initiatorId": 1, "initiatorIdentifier": 1, "memberId": 1, "memberIdentifier": 1, "isMatchedUser": 1,
                        "profilePic": "$targetDetails.profilePic", "number": "$targetDetails.contactNumber",
                        "userName": "$targetDetails.firstName", "recipientId": "$targetDetails._id",
                        "onlineStatus": "$targetDetails.onlineStatus", "boostByUser": 1
                    }
                },
                { "$sort": { "timestamp": -1 } },
                { "$skip": skip }, { "$limit": limit },
                { "$sort": { "messageId": -1 } }
            ];

            chatListCollection.Aggregate(chatListAggr, (err, result) => {

                if (err) {
                    return res({ message: "Unknown error occurred" }).code(503);
                }
                else if (result.length) {

                    var condition, userId, targetId, index = 0;
                    var unchatCount = [], finalRes = [];

                    async.each(result, function (val, callbackloop) {
                        let JWTDecoded = { userId: _id };

                        userId = "members." + JWTDecoded.userId;
                        targetId = "messageDetails.members." + JWTDecoded.userId + ".readAt";

                        if (val.chatType == 'GroupChat') {
                            /** query for group chat */
                            condition = [
                                { "$match": { "_id": ObjectID(val.chatId) } },
                                // { "$match": { [userId]: { "$exists": true }, secretId: val.secretId } },
                                { "$unwind": "$message" },
                                { "$lookup": { "from": "messages", "localField": "message", "foreignField": "_id", as: "messageDetails" } },
                                { "$unwind": "$messageDetails" },
                                { "$match": { "messageDetails.payload": { $ne: "" } } },
                                // { "$match": { "messageDetails.senderId": ObjectID(val.senderId) } },
                                // { "$match": { "messageDetails.receiverId": ObjectID(JWTDecoded.userId) } },
                                { "$match": { ["messageDetails.members." + JWTDecoded.userId]: { $exists: true } } },
                                { "$match": { [targetId]: { "$exists": false } } },
                                { "$group": { _id: JWTDecoded.userId, "totalUnread": { $sum: 1 } } }
                            ];
                        } else {

                            condition = [
                                { "$match": { "_id": ObjectID(val.chatId) } },
                                { "$match": { [userId]: { "$exists": true }, secretId: val.secretId } },
                                { "$unwind": "$message" },
                                { "$lookup": { "from": "messages", "localField": "message", "foreignField": "_id", as: "messageDetails" } },
                                { "$unwind": "$messageDetails" },
                                { "$match": { "messageDetails.payload": { $ne: "" } } },
                                { "$match": { "messageDetails.senderId": ObjectID(val.senderId) } },
                                { "$match": { "messageDetails.receiverId": ObjectID(JWTDecoded.userId) } },
                                { "$match": { [targetId]: { "$exists": false } } },
                                { "$group": { _id: JWTDecoded.userId, "totalUnread": { $sum: 1 } } }
                            ];
                        }
                        chatListCollection.Aggregate(condition, (err, resultA) => {
                            if (err) {
                                val["totalUnread"] = 0;
                            } else if (resultA[0]) {
                                val["totalUnread"] = resultA[0].totalUnread;
                            } else {
                                val["totalUnread"] = 0;
                            }
                            if (result[index].messageType != "1" && result[index].messageType != "2" && result[index].messageType != "7") {
                                delete result[index].thumbnail;
                            }
                            index++;
                            callbackloop(err, resultA);
                        });

                    }, function (err, resultLoop) {
                        if (err) {
                            return res({ code: 200, message: "DB Error" }).code(500);
                        } else {
                            /**
                             * Success status goes with API response. 
                             * Actual data goes through MQTT
                             */
                            async.eachSeries(result, function (resObj, resCB) {
                                /**
                                 * Loop throught the result and prepare the final data
                                 */
                                var finalChatObj = {
                                    "initiated": (resObj.initiatedBy == JWTDecoded.userId),
                                    "isTargetBoostProfile": (resObj && resObj.boostByUser) ? (resObj.boostByUser != JWTDecoded.userId) : false,
                                    "chatInitiatedBy": resObj.initiatedBy,
                                    "chatId": resObj.chatId,
                                    "groupChat": (resObj.chatType == "GroupChat"),
                                    "firstName": resObj.userName,
                                    "profilePic": resObj.profilePic,
                                    "secretId": resObj.secretId,
                                    "dTime": resObj.dTime,
                                    "messageId": resObj.messageId,
                                    "payload": resObj.payload,
                                    "messageType": resObj.messageType,
                                    "timestamp": resObj.timestamp,
                                    "receiverId": resObj.receiverId,
                                    "senderId": resObj.senderId,
                                    "receiverIdentifier": resObj.number,
                                    "recipientId": resObj.recipientId,
                                    "memberId": resObj.memberId,
                                    "memberIdentifier": resObj.memberIdentifier,
                                    "previousReceiverIdentifier": resObj.previousReceiverIdentifier,
                                    "previousFrom": resObj.previousFrom,
                                    "previousPayload": resObj.previousPayload,
                                    "previousType": resObj.previousType,
                                    "previousId": resObj.previousId,
                                    "replyType": resObj.replyType,
                                    "previousFileType": resObj.previousFileType,
                                    "status": (resObj.members) ? ((resObj.members[resObj.receiverId] && resObj.members[resObj.receiverId].status) ? resObj.members[resObj.receiverId].status : 1) : 1,
                                    "totalUnread": resObj.totalUnread,
                                    "isMatchedUser": resObj.isMatchedUser || 0,
                                    "onlineStatus": resObj.onlineStatus,
                                    "isSupperLikedMe": 0
                                }
                                if (resObj.senderId == JWTDecoded.userId) {
                                    finalChatObj["isBlocked"] = (req.user &&
                                        req.user.blockedBy &&
                                        (req.user.blockedBy.map(id => id.toString())
                                            .includes(resObj.receiverId.toString()))) ? 1 : 0;
                                    finalChatObj["isBlockedByMe"] = (req.user &&
                                        req.user.myBlock &&
                                        req.user.myBlock.map(id => id.toString())
                                            .includes(resObj.receiverId.toString())) ? 1 : 0;

                                    finalChatObj["isSupperLikedMe"] = (req.user && req.user.supperLikeByHistory && req.user.supperLikeByHistory.map(id => id.toString()).includes(resObj.receiverId.toString())) ? 1 : 0;
                                } else {

                                    finalChatObj["isBlocked"] = (req.user && req.user.blockedBy && req.user.blockedBy.map(id => id.toString()).includes(resObj.senderId.toString())) ? 1 : 0;
                                    finalChatObj["isBlockedByMe"] = (req.user &&
                                        req.user.myBlock &&
                                        req.user.myBlock.map(id => id.toString())
                                            .includes(resObj.senderId.toString())) ? 1 : 0;
                                    finalChatObj["isSupperLikedMe"] = (req.user && req.user.supperLikeByHistory && req.user.supperLikeByHistory.map(id => id.toString()).includes(resObj.senderId.toString())) ? 1 : 0;
                                }

                                finalRes.push(finalChatObj)
                                resCB(null);
                            },
                                function (finalLoopErr) {
                                    if (finalLoopErr) {
                                        return res({ message: finalLoopErr.message }).code(503);
                                    }
                                    mqtt.publish("GetChats/" + JWTDecoded.userId, JSON.stringify({ "chats": finalRes }), { qos: 1 }, () => { })
                                    return res({ code: 200, message: "success", data: finalRes }).code(200);
                                })
                        }
                    })
                }
                else {
                    mqtt.publish("GetChats/" + JWTDecoded.userId, JSON.stringify({ "chats": [] }), { qos: 1 }, () => { })
                    return res({ code: 200, message: "No data Found.", data: [] }).code(200);
                }
            })
        }
    ])
}

let validator = Joi.object({
    pageNo: Joi.string().required().description("0").error(new Error('pageNo is missing'))
}).required();

let response = {
    // status: {
    //     200: { message: Joi.any().default(local['GetChats']['200']), data: Joi.any() },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    // }
}

module.exports = { handler, validator, response }