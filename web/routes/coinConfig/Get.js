'use strict'
var Joi = require('joi');
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const local = require('../../../locales');
const coinConfig = require('../../../models/coinConfig');

let handler = (req, res) => {

    let limit = parseInt(req.query.limit) || 20;
    let skip = parseInt(req.query.offset) || 0;

    coinConfig.Select({},(err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

        return res({ message: req.i18n.__('GetcoinConfig')['200'], data:result}).code(200);
    });
}

let validator = Joi.object({
    offset: Joi.number().default(0).description("0").error(new Error('offset is missing')),
    limit: Joi.number().default(20).description("0").error(new Error('limit is missing'))
}).required();

let response = {
    status: {
        200: { message: Joi.any().default(local['GetcoinConfig']['200']),data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}
module.exports = { handler, response, validator }