'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const moment = require("moment");

const local = require('../../../locales');
const userMatchCollection = require('../../../models/userMatch');
const coinWalletCollection = require('../../../models/coinWallet');
const walletCustomerCollection = require('../../../models/walletCustomer');

let validator = Joi.object({
    coinAmount: Joi.number().required().example(100).min(0).error(new Error('coinAmount is missing or incorrect it must be > 0')),
}).required();

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let coinAmount = req.payload.coinAmount;
    let opeingBalance = 0;

    getCoinFromCoinWallet()
        .then((value) => { return updateInCoinWallet(); })
        .then((value) => { return insertInWalletCusetomerAndUpdateInCoinWallet(); })
        .then((value) => { return res({ message: req.i18n.__('PostcoinFromVideo')['200'], closingBalance: (opeingBalance + coinAmount) }).code(200); })
        .catch((err) => {
            logger.error('Caught an error!', err);
            return res({ message: err.message }).code(err.code);
        });

    function getCoinFromCoinWallet() {
        return new Promise((resolve, reject) => {
            let condition = { _id: ObjectID(_id) };

            coinWalletCollection.SelectOne(condition, (err, result) => {
                if (err) {
                    logger.error(err)
                    return reject(err);
                } else {
                    opeingBalance = (result.coins) ? result.coins.Coin : 0;
                    return resolve(true);
                }
            })
        });
    }
    function updateInCoinWallet() {
        return new Promise((resolve, reject) => {
            let condition = { _id: ObjectID(_id) };
            let dataToUpdate = {
                "coins.Coin": coinAmount
            };

            coinWalletCollection.UpdateWitInc(condition, dataToUpdate, (err, result) => {
                if (err) {
                    logger.error(err)
                    return reject(err);
                } else {
                    return resolve(true);
                }
            })
        });
    }
    function insertInWalletCusetomerAndUpdateInCoinWallet() {
        return new Promise((resolve, reject) => {
            let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
            let insertData = [];
            insertData.push({
                "txnId": txnId,
                "userId": ObjectID(_id),
                "txnType": "CREDIT",
                "txnTypeCode": 1,
                "trigger": "earned coins by watching video ads",
                "docType": "on Earn coins By waching video(ads)",
                "docTypeCode": 11,
                "currency": "N/A",
                "currencySymbol": "N/A",
                "coinType": "Coin",
                "coinOpeingBalance": opeingBalance,
                "cost": 0,
                "coinAmount": coinAmount,
                "coinClosingBalance": (opeingBalance + coinAmount),
                "paymentType": "N/A",
                "timestamp": new Date().getTime(),
                "transctionTime": new Date().getTime(),
                "transctionDate": new Date().getTime(),
                "paymentTxnId": "N/A",
                "initatedBy": "customer",
                "note": ""
            });

            walletCustomerCollection.InsertMany(insertData, () => { });
            return resolve(true);
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PostcoinFromVideo']['200']), closingBalance: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}

module.exports = { APIHandler, validator }