'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const coinPlansCollection = require('../../../models/coinPlans');

/**
 * @method GET coinPlans
 * @description This API use to  POST unmatch.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 * @returns  200 : User unmatched successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  422 : targetUserId  doesnot exist in the database,try with the different ID.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    getData()
        .then(function (value) {
            return res({ message: req.i18n.__('GetCoinPlans')['200'], data: value }).code(200);
        }).catch(function (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });

    function getData() {
        return new Promise(function (resolve, reject) {
            coinPlansCollection.SelectWithSort({ statusCode: 1 }, { _id: -1 }, { status: 0, statusCode: 0, timestamp: 0 }, 0, 1000, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                result.forEach(element => {
                    element["planId"] = element._id;
                    delete element["_id"];
                });

                return resolve(result);
            })
        });
    }
};

const response = {
    status: {
        200: { message: local['GetCoinPlans']['200'], data: Joi.any() },
        412: { message: local['GetCoinPlans']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

module.exports = { handler, response }