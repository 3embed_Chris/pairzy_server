'use strict'
const Joi = require("joi");
const moment = require("moment");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const walletPGCollection = require('../../../models/walletPG');
const walletAppCollection = require('../../../models/walletApp');
const coinPlansCollection = require('../../../models/coinPlans');
const coinWalletCollection = require('../../../models/coinWallet');
const walletCustomerCollection = require('../../../models/walletCustomer');

/**
 * @method POST subscription
 * @description This API use to  POST unmatch.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 
 * @returns  200 : User unmatched successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  422 : targetUserId  doesnot exist in the database,try with the different ID.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let planDetails = {};
    let timestamp = new Date().getTime();
    let PG_COMMITION_PER = 3;
    let PG_COMMITION_AMT = 0;
    let coinBalance = null, trigger = "";
    let transctionTime = timestamp,
        transctionDate = timestamp;


    coinPlansCollection.SelectOne({ _id: ObjectID(req.payload.planId), statusCode: 1 }, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

        if (result) {
            planDetails = result;
            getCoinWallet()
                .then(function (value) { return insertInWalletCustomer(value) })
                .then(function (value) { return insertInWalletApp() })
                .then(function (value) { return insertInWalletPG() })
                .then(function (value) { return res({ message: req.i18n.__('PostCoinPlans')['200'], data: { coinBalance: coinBalance } }).code(200); })
                .catch(function (err) {
                    logger.error('Caught an error!', err);
                    return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                });

        } else {
            return res({ message: req.i18n.__('PostCoinPlans')['412'] }).code(412);
        }
    })

    function getCoinWallet() {
        return new Promise((resolve, reject) => {
            /* in coinWallet _id is userId, so we find only _id */
            coinWalletCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                if (!result) {
                    coinWalletCollection.Insert({ _id: ObjectID(_id), coins: { "Coin": 0 } }, () => { });
                    return resolve({ "Coin": 0 });
                } else {
                    return resolve(result["coins"]);
                }

            });
        });
    }
    function insertInWalletCustomer(coinWallet) {
        return new Promise((resolve, reject) => {
            let insertData = [];
            let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);

            for (let key in planDetails.noOfCoinUnlock) {

                coinWallet[key] = coinWallet[key] || 0;
                trigger = `${planDetails.noOfCoinUnlock[key]} coins added to wallet via in-app purchase`;
                insertData.push({
                    "txnId": txnId,
                    "userId": ObjectID(_id),
                    "docType":"on purchase by user",
                    "docTypeCode":1,
                    "txnType": "PURCHSE",
                    "txnTypeCode": 3,
                    "trigger": `${planDetails.noOfCoinUnlock[key]} coins added to wallet via in-app purchase`,
                    "userPurchaseTime": req.payload.userPurchaseTime,
                    "currency": planDetails.currency,
                    "currencySymbol": planDetails.currencySymbol,
                    "coinType": key,
                    "coinOpeingBalance": coinWallet[key],
                    "cost": planDetails.cost,
                    "coinAmount": planDetails.noOfCoinUnlock[key],
                    "coinClosingBalance": (coinWallet[key] + planDetails.noOfCoinUnlock[key]),
                    "paymentType": req.payload.paymentType,
                    "timestamp": timestamp,
                    "transctionTime": transctionTime,
                    "transctionDate": transctionDate,
                    "paymentTxnId": req.payload.paymentGatewayTxnId,
                    "initatedBy": "customer"
                });
                coinWallet[key] = coinWallet[key] + planDetails.noOfCoinUnlock[key];
            }
            coinBalance = coinWallet;
            walletCustomerCollection.InsertMany(insertData, (e, r) => { logger.error((e) ? "walletCustomerCollection : " + e : "") });
            coinWalletCollection.Update({ _id: ObjectID(_id) }, { "coins": coinWallet }, (e, r) => { logger.error((e) ? "coinWalletCollection : " + e : "") });
            return resolve("---");
        });
    }
    function insertInWalletApp() {
        return new Promise((resolve, reject) => {

            walletAppCollection.SelectWithSort({}, { _id: -1 }, {}, 0, 1, (err, result) => {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                let opeingBalance = (result && result[0]) ? result[0]["closingBalance"] : 0;
                let closingBalance = opeingBalance + planDetails.cost;
                PG_COMMITION_AMT = (planDetails.cost * PG_COMMITION_PER) / 100;

                let dataToInsert = {
                    "txnId": "TXN-ID-" + Math.floor(Math.random() * 10000000000),
                    "userId": 1,
                    "txnType": "CREDIT",
                    "txnTypeCode": 1,
                    "trigger": trigger,
                    "currency": planDetails.currency,
                    "currencySymbol": planDetails.currencySymbole,
                    "opeingBalance": opeingBalance,
                    "cost": planDetails.cost,
                    "closingBalance": closingBalance,
                    "paymentType": req.payload.paymentType,
                    "timestamp": timestamp,
                    "transctionTime": transctionTime,
                    "transctionDate": transctionDate,
                    "paymentTxnId": req.payload.paymentGatewayTxnId,
                    "initatedBy": "customer"
                };
                walletAppCollection.Insert(dataToInsert, (err, result) => {
                    if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                });

                let dataToInsertDebit = {
                    "txnId": "TXN-ID-" + Math.floor(Math.random() * 10000000000),
                    "userId": 1,
                    "txnType": "DEBIT",
                    "txnTypeCode": 2,
                    "trigger": trigger,
                    "currency": planDetails.currency,
                    "currencySymbol": planDetails.currencySymbole,
                    "opeingBalance": closingBalance,
                    "cost": PG_COMMITION_AMT,
                    "closingBalance": closingBalance - PG_COMMITION_AMT,
                    "paymentType": req.payload.paymentType,
                    "timestamp": timestamp,
                    "transctionTime": transctionTime,
                    "transctionDate": transctionDate,
                    "paymentTxnId": req.payload.paymentGatewayTxnId,
                    "initatedBy": "customer"
                };
                walletAppCollection.Insert(dataToInsertDebit, (err, result) => {
                    if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                    return resolve("----");
                });
            });
        });
    }
    function insertInWalletPG() {
        return new Promise((resolve, reject) => {

            walletPGCollection.SelectWithSort({}, { _id: -1 }, {}, 0, 1, (err, result) => {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                let opeingBalance = (result && result[0]) ? result[0]["closingBalance"] : 0;
                let closingBalance = opeingBalance + PG_COMMITION_AMT;

                let dataToInsert = {
                    "txnId": "TXN-ID-" + Math.floor(Math.random() * 10000000000),
                    "userId": 2,
                    "txnType": "CREDIT",
                    "txnTypeCode": 1,
                    "trigger": trigger,
                    "currency": planDetails.currency,
                    "currencySymbol": planDetails.currencySymbole,
                    "opeingBalance": opeingBalance,
                    "cost": PG_COMMITION_AMT,
                    "closingBalance": closingBalance,
                    "paymentType": req.payload.paymentType,
                    "timestamp": timestamp,
                    "transctionTime": transctionTime,
                    "transctionDate": transctionDate,
                    "paymentTxnId": req.payload.paymentGatewayTxnId,
                    "initatedBy": "customer"
                };
                walletPGCollection.Insert(dataToInsert, (err, result) => {
                    if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                    return resolve("----");
                });
            });
        });
    }
};

let response = {
    status: {
        200: { message: local['PostCoinPlans']['200'], data: Joi.any() },
        412: { message: local['PostCoinPlans']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

let validator = Joi.object({
    planId: Joi.string().required().min(24).max(24).description("planId, Eg. 5b08158770c84b13a29c95cf ").error(new Error('planId is missing')),
    paymentGatewayTxnId: Joi.string().required().description("paymentGatewayTxnId ").error(new Error('paymentGatewayTxnId is missing')),
    // trigger: Joi.string().required().allow("").description("trigger ").error(new Error('trigger is missing')),
    paymentType: Joi.string().required().description("paymentType ,Eg. card ").error(new Error('paymentType is missing')),
    // paymentTxnId: Joi.string().required().description("paymentTxnId ").error(new Error('paymentTxnId is missing')),
    userPurchaseTime: Joi.string().required().description("userPurchaseTime ").error(new Error('userPurchaseTime is missing')),
}).unknown();

module.exports = { handler, response, validator }