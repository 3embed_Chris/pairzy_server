'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const dateScheduleCollection = require('../../../models/dateSchedule');

let validator = Joi.object({
    date_id: Joi.string().required().description("ex : 5a281337005a4e3b65bf12a8").example("5a281337005a4e3b65bf12a8").max(24).min(24).description("targetUserId").error(new Error('targetUserId is missing or incorrect it must be 24 char || digit only')),
}).required();

let APIHandler = (req, res) => {

    let date_id = ObjectID(req.payload.date_id);
    dateScheduleCollection.Update({ _id: date_id }, { "isComplete": true }, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['200'] }).code(500);
        } else {
            return res({ message: req.i18n.__('PostCompleteDate')['200'] }).code(200);
        }
    });

};

module.exports = {
    APIHandler,
    validator
}