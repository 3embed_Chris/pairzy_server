'use strict'
let headerValidator = require('../../middleware/validator');
let postAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/completeDate',
        handler: postAPI.APIHandler,
        config: {
            description: 'This API will be used to make a complete Date ',
            tags: ['api', 'Date'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];