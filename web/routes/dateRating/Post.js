'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const dateScheduleCollection = require('../../../models/dateSchedule');




let validator = Joi.object({
    dateId: Joi.string().required().description("ex : 5a281337005a4e3b65bf12a8").example("5a281337005a4e3b65bf12a8").max(24).min(24).error(new Error('dateId is missing or incorrect it must be 24 char || digit only')),
    rate: Joi.number().min(1).max(5).required().description("ex : 1 || 2 || 3 || 4 || 5 ").example(1).error(new Error('rate is missing  or incorrect it must be 1 || 2 || 3 only'))
}).required();

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let dateId = req.payload.dateId;
    let targetId = "";

    if (req.payload.rate) {
        req.payload.rate = parseInt(req.payload.rate);
    } else {
        return res({ message: req.i18n.__('PostDateRating')['412'] }).code(412);
    }

    getTargetId()
        .then(function (value) {
            return setRate();
        })
        .then(function (value) {
            return res({ message: req.i18n.__('PostDateRating')['200'] }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: req.i18n.__('PostDateRating')['412'] }).code(412);
        });

    function getTargetId() {
        return new Promise(function (resolve, reject) {
            let condition = { _id: ObjectID(dateId) };
            dateScheduleCollection.SelectOne(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result) {
                    targetId = (result.initiatedBy == _id) ? result.opponentId : result.initiatedBy;
                    result["targetId"] = targetId;
                    return resolve(result);
                } else {
                    return reject(new Error('Ooops, something broke!'));
                }
            });
        });
    }

    function setRate() {
        return new Promise(function (resolve, reject) {
            let condition = { _id: ObjectID(dateId) };
            let data = {
                dateFeedback: {
                    "reviewerId": ObjectID(_id),
                    "opponentId": ObjectID(targetId),
                    "rating": req.payload.rate
                }
            }
            dateScheduleCollection.UpdateByIdWithPush(condition, data, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                return resolve(result);

            });
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PostDateRating']['200']), data: Joi.any() },
        412: { message: Joi.any().default(local['genericErrMsg']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { response, APIHandler, validator }