'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const moment = require("moment");

const local = require('../../../locales');
const fcmPush = require("../../../library/fcm");
const myLibrary = require("../../../library/myLibrary");
const userListCollection = require('../../../models/userList');
const coinConfigCollection = require('../../../models/coinConfig');
const coinWalletCollection = require('../../../models/coinWallet');
const dateScheduleCollection = require('../../../models/dateSchedule');
const walletCustomerCollection = require('../../../models/walletCustomer');
const dateSchedule_TTL_Collection = require('../../../models/dateSchedule_TTL');

let validator = Joi.object({
    response: Joi.number().required().min(1).max(3).description("1 || 2 || 3 , ex : accepted : 1 / denied : 2 / reschedule : 3").example(1).error(new Error('response is missing or incorrect it must be 1 || 2 || 3  in digit only')),
    date_id: Joi.string().required().max(24).min(24).description("ex : 5a281337005a4e3b65bf12a8").example("5a281337005a4e3b65bf12a8").error(new Error('date_id is missing or incorrect it must be 24 char || digit only')),
    proposedOn: Joi.number().required().description("ex : 1512829147000").example(1512829147000).error(new Error('proposedOn is missing  or incorrect it must be 13 digit only')),
    dateType: Joi.number().required().description("ex : 1 : videoDate, 2 :  inPersonDate,3 : audioDate").min(1).max(3).example(1).default(1).error(new Error('dateType is missing  or incorrect it must be 1 || 2 || 3 only')),
    longitude: Joi.number().default(0).error(new Error('longitude is missing')),
    latitude: Joi.number().default(0).error(new Error('latitude is missing')),
    placeName: Joi.string().allow("").default("").error(new Error('placeName is missing')),
}).required();

let APIHandler = (req, res) => {

    console.log("req.payload. ", req.payload)
    // return res({ message: req.i18n.__('PostDateResponse')['200'] }).code(200);
    const dateTypeObj = { 1: "videoDate", 2: "inPersonDate", 3: "audioDate" };
    let _id = req.auth.credentials._id;
    let dateType = req.payload.dateType;
    let response = ((req.payload.response == 1) ? "accepted" : ((req.payload.response == 2) ? "denied" : "reschedule"));
    var trigger = "resheduleDate";
    var timeZone;
    var data = {}, isNeedToPay = false, coinWallet;
    let befor = {
        initiatedId: "",
        initiatedName: "",
        opponentId: "",
        opponentName: "",
        proposedOn: 0
    };

    checkDateSchedule()
        .then((value) => { return getRequiredCoinForTrigger(); })
        .then((value) => { return getUsersCoinFromCoinWalletAndValidate(value); })
        .then((value) => { return updateDateSchedule(); })
        .then((value) => { return insertInWalletCusetomerAndUpdateInCoinWallet(); })
        .then((value) => { return res({ message: req.i18n.__('PostDateResponse')['200'], coinWallet: data.coinWallet }).code(200); })
        .catch((err) => {
            logger.error('Caught an error!', err);
            return res({ message: err.message }).code(err.code);
        });


    function checkDateSchedule() {
        return new Promise(function (resolve, reject) {
            let condition = {
                _id: ObjectID(req.payload.date_id),
                "$or": [
                    { "negotiations": { "$elemMatch": { "opponentResponse": "" } } },
                    {
                        "negotiations": {
                            "$elemMatch": {
                                "opponentResponse": "accepted"
                            }
                        }
                    }
                ]
            };

            dateScheduleCollection.SelectOne(condition, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                }
                else if (result) {

                    for (let index = 0; index < result["negotiations"].length; index++) {
                        if (result["negotiations"][index]["opponentResponse"] == "" || result["negotiations"][index]["opponentResponse"] == "accepted") {

                            isNeedToPay = (result.acceptedDateTimeStamp && req.payload.response == 3)
                            console.log("isNeedToPay   ", isNeedToPay)

                            if (_id == result["negotiations"][index]["proposerId"]) {
                                befor.opponentId = result["negotiations"][index]["proposerId"].toString();
                                befor.initiatedId = result["negotiations"][index]["opponnedId"].toString();

                                befor.proposedOn = result["negotiations"][index]["proposedOn"]
                                befor.opponentName = (result["negotiations"][index]["proposerId"] == result["initiatedBy"]) ? result["initiatorName"] : result["opponentName"];
                                befor.initiatedName = (result["negotiations"][index]["opponnedId"] == result["opponentId"]) ? result["opponentName"] : result["initiatorName"];

                            } else {
                                befor.initiatedId = result["negotiations"][index]["proposerId"].toString();
                                befor.opponentId = result["negotiations"][index]["opponnedId"].toString();

                                befor.proposedOn = result["negotiations"][index]["proposedOn"]
                                befor.initiatedName = (result["negotiations"][index]["proposerId"] == result["initiatedBy"]) ? result["initiatorName"] : result["opponentName"];
                                befor.opponentName = (result["negotiations"][index]["opponnedId"] == result["opponentId"]) ? result["opponentName"] : result["initiatorName"];

                            }
                            break;
                        }
                    }

                    return resolve(result);

                } else {

                    return reject(new Error('Ooops, something broke!'));
                }
            });
        });
    }
    function getRequiredCoinForTrigger() {
        return new Promise((resolve, reject) => {
            coinConfigCollection.SelectOne({}, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));
                return resolve(result["resheduleDate"] || {});
            });
        });
    }
    function getUsersCoinFromCoinWalletAndValidate(requiredCoin) {
        return new Promise((resolve, reject) => {
            coinWalletCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });


                if (isNeedToPay) {
                    logger.silly("requiredCoin ", requiredCoin)
                    for (let key in requiredCoin) {
                        if (requiredCoin[key] == 0 || (result && result["coins"][key] && requiredCoin[key] <= result["coins"][key])) {
                        } else {
                            return reject({ code: 402, message: req.i18n.__('genericErrMsg')['402'] });
                        }
                    }
                }

                data["coinWallet"] = result["coins"] || {};
                data["requiredCoin"] = requiredCoin;
                return resolve(true);
            });
        });
    }
    function updateDateSchedule() {
        return new Promise(function (resolve, reject) {
            switch (req.payload.response) {
                case 1: {

                    userListCollection.SelectOne({ _id: ObjectID(befor.initiatedId) }, (err, result) => {
                        let deviceType = "1";
                        if (result && result._id) {
                            timeZone = result.timeZone || "Asia/Kolkata";
                            deviceType = result.deviceType || "1";
                        }

                        let payload = {
                            notification: {
                                //“Congratulations! Laura just accepted your date request for June 18th at 10:45am.” 
                                body: `Congratulations! ${befor.opponentName}, ${befor.initiatedName} just accepted your date request for ${moment.tz(req.payload["proposedOn"], timeZone).format('MMM Do h:mm a')}`
                            },
                            data: { type: "9", deviceType: deviceType } // values must be string
                        }
                        fcmPush.sendPushToTopic("/topics/" + befor.initiatedId, payload, (e, r) => {
                            if (e) {
                                return reject(new Error('Ooops, something broke!'));
                            }
                        });
                    });

                    let condition = {
                        "_id": ObjectID(req.payload.date_id),
                        "negotiations": { "$elemMatch": { "opponentResponse": "" } }

                    };
                    let dataToUpdate = {
                        "negotiations.$.opponentResponse": response,
                        "acceptedDateTimeStamp": new Date().getTime()
                    };
                    dateSchedule_TTL_Collection.Update(condition, dataToUpdate, (err, result) => {
                        if (err) {
                            return reject(new Error('Ooops, something broke!'));
                        }
                    });

                    dateScheduleCollection.Update(condition, dataToUpdate, (err, result) => {
                        if (err) {
                            return reject(new Error('Ooops, something broke!'));
                        }
                        return resolve(result);
                    });
                    break;
                }
                case 2: {
                    let condition = {
                        _id: ObjectID(req.payload.date_id),
                        "negotiations": { "$elemMatch": { "opponentResponse": "" } }
                    };
                    let dataToUpdate = {
                        "negotiations.$.opponentResponse": response,
                        "negotiations.$.by": ObjectID(_id),
                        "acceptedDateTimeStamp": new Date().getTime()
                    };
                    dateScheduleCollection.SelectOne(condition, (err, result) => {
                        if (err) {
                            return reject(new Error('Ooops, something broke!'));
                        }


                        if (result && result._id) {
                            userListCollection.SelectOne({ _id: ObjectID(befor.initiatedId) }, (err, result) => {
                                let deviceType = "1";
                                if (result && result._id) {

                                     timeZone = result.timeZone || "Asia/Kolkata";

                                    deviceType = result.deviceType || "1";
                                }
                                let payload = {
                                    notification: {
                                        body: `Hey ${befor.opponentName}, ${befor.initiatedName}, has rejected your date request for ${moment.tz(befor.proposedOn, timeZone).format('MMM Do h:mm a')}`
                                    },
                                    data: { type: "10", deviceType: deviceType } // values must be string
                                }
                                fcmPush.sendPushToTopic("/topics/" + befor.initiatedId, payload, (e, r) => {
                                    if (e) {
                                        return reject(new Error('Ooops, something broke!'));
                                    }
                                });
                            });
                            dateSchedule_TTL_Collection.Update(condition, dataToUpdate, (err, result) => {
                                if (err) return reject(new Error('Ooops, something broke!'));
                            });
                            dateScheduleCollection.Update(condition, dataToUpdate, (err, result) => {
                                if (err) return reject(new Error('Ooops, something broke!'));
                                return resolve(result);
                            });
                        } else {
                            userListCollection.SelectOne({ _id: ObjectID(befor.initiatedId) }, (err, result) => {
                                let deviceType = "1";
                                if (result && result._id) {
                                    deviceType = result.deviceType || "1";
                                    timeZone = result.timeZone ||"Asia/Kolkata";
                                }
                                let payload = {
                                    notification: {
                                        body: `Hey ${befor.opponentName}, ${befor.initiatedName} has cancelled the set for ${moment.tz(befor.proposedOn, timeZone).format('MMM Do h:mm a')}`
                                    },

                                    data: { type: "10", deviceType: deviceType } // values must be string
                                }
                                fcmPush.sendPushToTopic("/topics/" + befor.initiatedId, payload, (e, r) => {
                                    if (e) console.log("E : ", e)
                                });
                            });
                            condition = {
                                _id: ObjectID(req.payload.date_id),
                                "negotiations": { "$elemMatch": { "opponentResponse": "accepted" } }
                            };
                            dateSchedule_TTL_Collection.Update(condition, dataToUpdate, (err, result) => {
                                if (err) return reject(new Error('Ooops, something broke!'));
                            });
                            dateScheduleCollection.Update(condition, dataToUpdate, (err, result) => {
                                if (err) return reject(new Error('Ooops, something broke!'));
                                return resolve(result);
                            });
                        }
                    })
                    break;
                }
                case 3: {
                    userListCollection.SelectOne({ _id: ObjectID(befor.initiatedId) }, (err, result) => {
                        let deviceType = "1";
                        if (result && result._id) {
                            deviceType = result.deviceType || "1";
                         timeZone = result.timeZone || "Asia/Kolkata";

                        }
                        let payload = {
                            notification: {
                                body: `Hey ${befor.opponentName}, ${befor.initiatedName}  wants to reschedule the date from ${moment.tz(befor.proposedOn, timeZone).format('MMM Do h:mm a')} to ${moment.tz(req.payload["proposedOn"], timeZone).format('MMM Do h:mm a')}`
                            },

                            data: { type: "8", deviceType: deviceType } // values must be string
                        }
                        fcmPush.sendPushToTopic("/topics/" + befor.initiatedId, payload, (e, r) => { });
                    });
                    let condition = {
                        _id: ObjectID(req.payload.date_id),
                        "negotiations": { "$elemMatch": { "opponentResponse": "" } }
                    };
                    let dataToUpdate = {
                        "expireAt": new Date(myLibrary.getExpiryTimeForMongoTTL(req.payload.proposedOn, 15)),
                        "negotiations.$.opponentResponse": response,
                        "negotiations.$.by": ObjectID(_id),
                    };

                    dateSchedule_TTL_Collection.Update(condition, dataToUpdate, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));
                    });
                    dateScheduleCollection.Update(condition, dataToUpdate, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));
                    });
                    condition = {
                        _id: ObjectID(req.payload.date_id),
                        "negotiations": { "$elemMatch": { "opponentResponse": "accepted" } }
                    };
                    dateSchedule_TTL_Collection.Update(condition, dataToUpdate, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));
                    });
                    dateScheduleCollection.Update(condition, dataToUpdate, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));
                    });

                    let conditionToPush = { _id: ObjectID(req.payload.date_id) };
                    dataToUpdate = {
                        "placeName": req.payload.placeName,
                        "location": {
                            "longitude": req.payload.longitude,
                            "latitude": req.payload.latitude,
                        }
                    };

                    dateSchedule_TTL_Collection.Update(conditionToPush, dataToUpdate, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));
                    });
                    dateScheduleCollection.Update(conditionToPush, dataToUpdate, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));
                    });
                    let dataToPush = {
                        negotiations: {
                            "proposerId": ObjectID(befor.opponentId),
                            "proposedOn": req.payload.proposedOn,
                            "opponnedId": ObjectID(befor.initiatedId),
                            "proposedTime": new Date().getTime(),
                            "opponentResponse": "",
                            "requestedFor": dateTypeObj[dateType],
                            "placeName": req.payload.placeName,
                            "longitude": req.payload.longitude,
                            "latitude": req.payload.latitude
                        }
                    };
                    dateSchedule_TTL_Collection.UpdateByIdWithPush(conditionToPush, dataToPush, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));
                    });
                    dateScheduleCollection.UpdateByIdWithPush(conditionToPush, dataToPush, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));

                        return resolve(result);
                    });
                    break;
                }
            }
        });
    }
    function insertInWalletCusetomerAndUpdateInCoinWallet() {
        return new Promise((resolve, reject) => {

            if (isNeedToPay) {
                let coinWallet = data.coinWallet;
                let requiredCoin = data.requiredCoin;
                let insertData = [];
                let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
                for (let key in requiredCoin) {

                    if (coinWallet[key]) {
                        insertData.push({
                            "txnId": txnId,
                            "userId": ObjectID(_id),
                            "docType": "on reshedule date",
                            "docTypeCode": 9,
                            "txnType": "DEBIT",
                            "txnTypeCode": 2,
                            "trigger": `Spent ${requiredCoin[key]} coins on rescheduling a confirmed date`,
                            "currency": "N/A",
                            "currencySymbol": "N/A",
                            "coinType": key,
                            "coinOpeingBalance": coinWallet[key],
                            "cost": 0,
                            "coinAmount": requiredCoin[key],
                            "coinClosingBalance": (coinWallet[key] - requiredCoin[key]),
                            "paymentType": "N/A",
                            "timestamp": new Date().getTime(),
                            "transctionTime": new Date().getTime(),
                            "transctionDate": new Date().getTime(),
                            "paymentTxnId": "N/A",
                            "initatedBy": "customer"
                        });
                        coinWallet[key] = coinWallet[key] - requiredCoin[key];
                    }
                }
                walletCustomerCollection.InsertMany(insertData, () => { });
                coinWalletCollection.Update({ _id: ObjectID(_id) }, { "coins": coinWallet }, () => { });

            }
            coinWallet = data;
            return resolve(true);
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PostDateResponse']['200']), data: Joi.any(), coinWallet: Joi.any() },
        412: { message: Joi.any().default(local['PostDateResponse']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        402: { message: Joi.any().default(local['genericErrMsg']['402']) },
    }
}


module.exports = { APIHandler, validator, response }