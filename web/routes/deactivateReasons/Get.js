'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const local = require('../../../locales');
const deactivateReasons = require("../../../models/deactivateReasons");

let handler = (req, res) => {
    let _id = req.auth.credentials._id;
    let dataTosend = [];

    deactivateReasons.Select({}, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        if (!result.length) return res({ message: req.i18n.__('GetDeactivateReasons')['412'] }).code(412);

        for (let index = 0; index < result.length; index++) {
            dataTosend.push(result[index].reason["1"]);
        }
        return res({ message: req.i18n.__('GetDeactivateReasons')['200'], data: dataTosend }).code(200);
    });
};
let response = {
    status: {
        200: {
            message: Joi.any().default(local['GetDeactivateReasons']['200']), data: Joi.any().example({
                "data": [
                    "Duplicate user"
                ]
            })
        },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        412: { message: Joi.any().default(local['GetDeactivateReasons']['412']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { handler, response }