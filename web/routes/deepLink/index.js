'use strict'
let headerValidator = require('../../middleware/validator');
let PostAPI = require('./Post');
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'POST',
        path: '/deepLink',
        handler: PostAPI.APIHandler,
        config: {
            description: 'This API will be used',
            tags: ['api', 'deepLink'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'GET',
        path: '/deepLink/{targetUserId}',
        handler: GetAPI.handler,
        config: {
            description: 'This API will be used',
            tags: ['api', 'deepLink'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];