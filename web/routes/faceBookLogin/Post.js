'use strict'
const Config = process.env;
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const Timestamp = require('mongodb').Timestamp;
const moment = require("moment")

const coinConfig = require('../../../models/coinConfig');
const coinWallet = require('../../../models/coinWallet');

const local = require('../../../locales');
const cronJobTask = require('../../../cronJobTask');
const planCollection = require("../../../models/plans");
const Auth = require("../../middleware/authentication.js");
const userList_ES = require("../../../models/userListType");
const userListCollection = require('../../../models/userList');
const rabbitMq = require('../../../library/rabbitMq');
const userPrefrances = require('../../../models/userPrefrances');
const coinWalletCollection = require("../../../models/coinWallet")
const userDevicesCollection = require('../../../models/userDevices');
const unverfiedUsersCollection = require('../../../models/unverfiedUsers');
const searchPreferencesCollection = require('../../../models/searchPreferences');

const validator = Joi.object({
    firstName: Joi.string().required().description("First Name,Eg. Lisa/John").error(new Error('firstName is missing.')),
    dateOfBirth: Joi.number().allow(["", 0]).description("Date of Birth(miliseconds),Eg. 699860604000").example(1504867670000).error(new Error('dateOfBirth is missing')),
    fbId: Joi.string().required().description("fbId").error(new Error('fbId is missing')),
    // countryCode: Joi.string().required().description("Country Code, Eg. +91/+1").error(new Error('countryCode is missing')),
    // mobileNumber: Joi.string().required().description("Mobile Number, Eg. 1234567890").error(new Error('mobileNumber is missing')),
    // gender: Joi.number().min(1).max(2).description("gender 1 : Male, 2 : Female, Eg.1").default(1).error(new Error('gender is missing or invalid')),
    height: Joi.number().default(160).description("in centimeter eg 160").default(160).error(new Error('height is missing or invalid')),
    pushToken: Joi.string().required().description("Push Token, Eg. dY0xOny_uEA:APA91bE_HZQiTq").error(new Error('pushToken is missing')),
    deviceId: Joi.string().required().description("Device Id, Eg. 157875de315458000000000000000").error(new Error('deviceId is missing')),
    deviceMake: Joi.string().required().description("Device Make/Company, Eg. Samsung/Apple").error(new Error('deviceMake is missing')),
    deviceModel: Joi.string().required().description("Device Model Number, Eg. SM-N920T").error(new Error('deviceModel is missing')),
    deviceType: Joi.string().required().description("Device Type(1-IOS and 2-Android), Eg. 2").error(new Error('deviceType is missing')),
    deviceOs: Joi.string().allow("").description("deviceOs, Eg. Oreo").error(new Error('deviceOs is missing')),
    appVersion: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('appVersion is missing')),
    bioData: Joi.string().description("bioData").error(new Error('bioData is missing')),
    work: Joi.string().description("work").error(new Error('work is missing')),
    eduation: Joi.string().description("eduation").error(new Error('eduation is missing')),
    email: Joi.string().allow("").description("email").error(new Error('email is missing')),
    profilePic: Joi.string().description("photo Link, Eg. https://res.cloudinary.com/closebrace/image/upload/w_400/v1491315007/usericon_id76rb.png")
        .default("https://res.cloudinary.com/closebrace/image/upload/w_400/v1491315007/usericon_id76rb.png")
        .error(new Error('profilePic is missing')),
    profileVideo: Joi.string().description("profileVideo").allow("").error(new Error('profileVideo is missing')),
    profileVideoThumbnail: Joi.string().allow("").description("profileVideoThumbnail").error(new Error('profileVideoThumbnail is missing')),
    otherImages: Joi.array().allow(["", []]).description("otherImages").error(new Error('otherImages is missing')),
    longitude: Joi.number().description("Longitude,Eg. 77.5894554").example(13.123456).error(new Error('longitude is missing')),
    latitude: Joi.number().description("Latitude,Eg. 13.0286543").example(77.123456).error(new Error('latitude is missing')),
    ip: Joi.string().description("IP Address,Eg. 106.51.66.44").default("106.51.66.44").example("106.51.66.44").error(new Error('ip is missing')),
    voipiospush: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('voipiospush is missing')),
    apnsPush: Joi.string().allow("").description("apnsPush, Eg. 1.0.0").error(new Error('apnsPush is missing')),


}).unknown();

/**
 * This API use to create new user as inactive.
 * if user also register and verifyed then return error other wise success 
 *  * @function POST/profile
 * @param {object} request - Required payload and headers.
 * @returns {object} - object represents the HTTP Status Code,Response Message and some profileFields.
 * 
 * @property {string} request.headers.authorization - authorization
 * @property {string} request.headers.lang - language
 * 
 * @property {string} request.payload.firstName - First Name,Eg. Lisa/John
 * @property {string} request.payload.dateOfBirth - Date of Birth(miliseconds),Eg. 1504867670000
 * @property {string} request.payload.gender - Gender,Eg. Male/Female
 * @property {string} request.payload.height - Height(Centimetre),Eg. 100/200
 * @property {string} request.payload.password - Password
 * @property {string} request.payload.countryCode - Country Code, Eg. +91/+1
 * @property {string} request.payload.mobileNumber - Mobile Number, Eg. 1234567890
 * @property {string} request.payload.pushToken - Push Token, Eg. dY0xOny_uEA:APA91bE_HZQiTq
 * @property {string} request.payload.deviceId - Device Id, Eg. 157875de315458000000000000000
 * @property {string} request.payload.deviceMake - Device Make/Company, Eg. Samsung/Apple
 * @property {string} request.payload.deviceModel - Device Model Number, Eg. SM-N920T
 */
const handler = (req, res) => {

    let fbId = req.payload.fbId;
    let condition = { fbId: fbId, "deleteStatus": { '$exists': false, '$ne': 1 }};
    let gender = (req.payload.gender == 1) ? "Male" : "Female";
    let coinWalletObj = {}, coinConfigObj;

    userListCollection.Select(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result[0] && result[0].profileStatus) {
            return res({ message: req.i18n.__('genericErrMsg')['403'] }).code(403);
        } else if (result.length) {
            let myPrefrances = [];
            let prefrances = {}
            let _id = result[0]._id;
            let searchPreferencesObj = {};
            let searchPreferences = [];
            let work = "", eduation = "", job = "";
            let deviceData = {
                userId: ObjectID(_id),
                deviceId: req.payload.deviceId,
                pushToken: req.payload.pushToken,
                deviceMake: req.payload.deviceMake,
                deviceModel: req.payload.deviceModel,
                deviceType: req.payload.deviceType,
                deviceOs: req.payload.deviceOs || "",
                appVersion: req.payload.appVersion || ""
            }
            userListCollection.Update({ _id: ObjectID(result[0]._id.toString()) }, {
                voipiospush: req.payload.voipiospush, apnsPush: req.payload.apnsPush
            }, () => { });
            async.series([
                function (callback) {
                    let userData = {
                        currentLoggedInDevices: {
                            deviceId: req.payload.deviceId,
                            pushToken: req.payload.pushToken
                        }
                    }
                    userListCollection.UpdateById(result[0]._id, userData, (e, r) => { });
                    callback(null, 1);
                },
                /*  function (callback) {
                     userPrefrances.Select({}, (err, data) => {
                         myPrefrances = data;
                         for (let index = 0; index < result[0].myPreferences.length; index++) {
                             prefrances[result[0].myPreferences[index]["pref_id"]] = result[0].myPreferences[index];
                         }
 
 
                         for (let index = 0; index < myPrefrances.length; index++) {
                             myPrefrances[index]["isDone"] = prefrances[myPrefrances[index]["_id"]]["isDone"];
                             myPrefrances[index]["selectedValues"] = prefrances[myPrefrances[index]["_id"]]["selectedValues"];
 
                             if (myPrefrances[index]["_id"] == "5a30fda027322defa4a14638"
                                 && myPrefrances[index]["isDone"]) {
                                 work = myPrefrances[index]["selectedValues"][0];
                             }
                             if (myPrefrances[index]["_id"] == "5a30fdd127322defa4a14649"
                                 && myPrefrances[index]["isDone"]) {
                                 job = myPrefrances[index]["selectedValues"][0];
                             }
                             if (myPrefrances[index]["_id"] == "5a30fdfa27322defa4a14653"
                                 && myPrefrances[index]["isDone"]) {
                                 eduation = myPrefrances[index]["selectedValues"][0];
                             }
                         }
 
                         let myPreferencesByGroupObj = {};
                         for (let index = 0; index < myPrefrances.length; index++) {
                             if (myPreferencesByGroupObj[myPrefrances[index]["title"]]) {
                                 myPreferencesByGroupObj[myPrefrances[index]["title"]].push(myPrefrances[index]);
                             } else {
                                 myPreferencesByGroupObj[myPrefrances[index]["title"]] = [myPrefrances[index]];
                             }
                         }
                         let myPreferencesByGroupArray = [];
                         for (let key in myPreferencesByGroupObj) {
                             myPreferencesByGroupArray.push({
                                 "title": key,
                                 "data": myPreferencesByGroupObj[key]
                             });
                         }
 
                         myPrefrances = myPreferencesByGroupArray;
 
                         callback(err, data);
                     })
                 }, */
                function (callback) {
                    let myPrefrancesDataToSend = []
                    let MyVices = []
                    let MyVitals = []
                    let MyVirtues = []
                    userPrefrances.Select({ mandatory: true }, (err, data) => {
                        myPrefrances = data;
                        result[0].myPreferences.forEach(element => {
                            myPrefrances.find(e => {
                                if (e._id == element.pref_id.toString()) {
                                    switch (e.title) {
                                        case "My Vices":

                                            MyVices.push({
                                                "title": e.title,
                                                "label": e.label,
                                                "type": e.type,
                                                "priority": e.priority,
                                                "mandatory": e.mandatory,
                                                "ActiveStatus": e.ActiveStatus,
                                                "pref_id": element.pref_id,
                                                "isDone": element.isDone,
                                                "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                                "options": e.options
                                            })
                                            break;
                                        case "My Vitals":
                                            MyVitals.push({
                                                "title": e.title,
                                                "label": e.label,
                                                "type": e.type,
                                                "priority": e.priority,
                                                "mandatory": e.mandatory,
                                                "ActiveStatus": e.ActiveStatus,
                                                "pref_id": element.pref_id,
                                                "isDone": element.isDone,
                                                "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                                "options": e.options
                                            })
                                            break;
                                        case "My Virtues":
                                            MyVirtues.push({
                                                "title": e.title,
                                                "label": e.label,
                                                "type": e.type,
                                                "priority": e.priority,
                                                "mandatory": e.mandatory,
                                                "ActiveStatus": e.ActiveStatus,
                                                "pref_id": element.pref_id,
                                                "isDone": element.isDone,
                                                "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                                "options": e.options
                                            })
                                        default:
                                            break;
                                    }
                                    return true
                                }
                            })
                        });
                        myPrefrancesDataToSend.push({
                            "title": "My Vices",
                            "data": MyVices
                        })
                        myPrefrancesDataToSend.push({
                            "title": "My Vitals",
                            "data": MyVitals
                        })
                        myPrefrancesDataToSend.push({
                            "title": "My Virtues",
                            "data": MyVirtues
                        })
                        myPrefrances = myPrefrancesDataToSend
                        callback(err, data);
                    })
                },
                function (callback) {
                    searchPreferencesCollection.Select({ isSearchPreference: true }, (err, result) => {
                        if (result[0]) {
                            let pref_id = "";
                            for (let index = 0; index < result.length; index++) {
                                pref_id = result[index]._id;
                                searchPreferencesObj[pref_id] = result[index];
                                searchPreferencesObj[pref_id]["selectedValues"] = searchPreferencesObj[pref_id]["options"];
                                delete searchPreferencesObj[pref_id]["ActiveStatus"];
                                if (result[index]["TypeOfPreference_User"]) {
                                    delete searchPreferencesObj[pref_id]["TypeOfPreference_User"];
                                }
                            }
                        }
                        callback(null, 1);
                    })
                },
                function (callback) {
                    /* Get Preferences to save In userList collection */
                    searchPreferencesCollection.Select({ isSearchPreference: true }, (err, result) => {
                        if (err) callback(err, result);
                        // console.log("=========>",result)
                        //dataToSend.searchPreferences = [];
                        for (let index = 0; index < result.length; index++) {

                            /* add in my searchPreferences */
                            let searchPreferencesData = {
                                pref_id: result[index]._id,
                                selectedValues: (result[index].type == 1 || result[index].type == 2) ? [result[index].options[0]] : result[index].options.map(Number)
                            }
                            if (result[index].type == 3 || result[index].type == 4) {
                                searchPreferencesData["selectedUnit"] = result[index].optionsUnits[0];
                            }
                            if (result[index]._id.toString() == "5d6384b9b80db667866938f3") {
                                if (gender == "Male") {
                                    searchPreferencesData["selectedValues"] = ["Female"]
                                } else {
                                    searchPreferencesData["selectedValues"] = ["Male"]
                                }
                            }
                            // for (let pref_id in searchPreferencesData) {
                            //     searchPreferences.push(searchPreferencesObj[pref_id])
                            // }
                            searchPreferences.push(searchPreferencesData)
                            //dataToSend.searchPreferences.push(searchPreferencesData);
                        }
                        callback(null, 1);
                    })
                },
               /*  function (callback) {
                    for (let index = 0; index < result[0].searchPreferences.length; index++) {
                        let pref_id = result[0].searchPreferences[index].pref_id;
                        if (searchPreferencesObj[pref_id]) {
                            searchPreferencesObj[pref_id]["selectedValues"] = result[0].searchPreferences[index]["selectedValues"];
                            if (result[0].searchPreferences[index]["selectedUnit"]) {
                                searchPreferencesObj[pref_id]["selectedUnit"] = result[0].searchPreferences[index]["selectedUnit"];
                            }
                        }
                    }
                    for (let pref_id in searchPreferencesObj) {
                        searchPreferences.push(searchPreferencesObj[pref_id])
                    }
                    callback(null, 1);
                }, */
                function (callback) {
                    /* insert into userList collection*/
                    userDevicesCollection.Insert(deviceData, (err, result) => {
                        callback(err, result);
                    })
                },
                function (callback) {
                    coinWallet.Select({ _id: ObjectID(_id) }, (err, result) => {
                        if (err) return callback(err, result);

                        coinWalletObj = result;
                        return callback(err, result);
                    });
                },
                function (callback) {
                    coinConfig.Select({}, (err, result) => {
                        if (err) return callback(err, result);

                        coinConfigObj = result[0];
                        return callback(err, result);
                    });
                }
            ], function (err, d) {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                let token = Auth.SignJWT({ _id: result[0]._id, key: 'acc' }, 'user', 60000000);
                let cur = new Date();
                let diff = cur - result[0].dob; // This is the difference in milliseconds
                let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                console.log("coinConfig : ", coinConfigObj)
                console.log("coinWalletObj : ", coinWalletObj[0].coins)
                return res({
                    message: req.i18n.__('PostFbLogin')['200'], data: {
                        token: token, _id: _id,
                        firstName: result[0].firstName,
                        gender: result[0].gender,
                        height: result[0].height,
                        email: result[0].email,
                        profilePic: result[0].profilePic,
                        profileVideo: result[0].profileVideo,
                        profileVideoThumbnail: result[0].profileVideoThumbnail,
                        otherImages: result[0].otherImages,
                        dob: result[0].dob,
                        about: result[0].about,
                        instaGramProfileId: result[0].instaGramProfileId,
                        instaGramToken: result[0].instaGramToken || "",
                        age: { value: age, isHidden: result[0].dontShowMyAge || 0 },
                        distance: { value: 0, isHidden: result[0].dontShowMyDist || 0 },
                        subscription: (result[0].subscription) ? result[0].subscription : [],
                        eduation: eduation,
                        work: work,
                        job: job,
                        searchPreferences: searchPreferences,
                        myPrefrances: myPrefrances,
                        location: result[0].location

                    },
                   /*  coinWallet: coinWalletObj[0].coins,
                    coinConfig: coinConfigObj */
                }).code(200);
            });
        } else {
            let userId = new ObjectID();
            let myPrefrances = [];
            let deviceData = {
                userId: userId,
                deviceId: req.payload.deviceId,
                pushToken: req.payload.pushToken,
                deviceMake: req.payload.deviceMake,
                deviceModel: req.payload.deviceModel,
                deviceType: req.payload.deviceType,
                deviceOs: req.payload.deviceOs || "",
                appVersion: req.payload.appVersion || ""
            }
            /* create objects to save in Collections */
            let newUser = {
                _id: userId,
                firstName: req.payload.firstName,
                lastName: req.payload.lastName || "",
                fbId: req.payload.fbId,
                gender: gender,
                height: req.payload.height,
                heightInFeet: `5'3"`,
                userType: "Normal",
                // countryCode: countryCode,
                // contactNumber: countryCode + mobileNumber,
                searchPreferences: [],
                myPreferences: [],
                registeredTimestamp: new Date().getTime(),
                email: req.payload.email.toLowerCase() || req.payload.fbId + "@facebook.com",
                profilePic: req.payload.profilePic || "",
                profileVideo: req.payload.profileVideo || "",
                profileVideoThumbnail: req.payload.profileVideoThumbnail || "",
                otherImages: req.payload.otherImages || [],
                dob: req.payload.dateOfBirth,
                about: req.payload.about || "",
                instaGramProfileId: req.payload.instaGramProfileId || "",
                currentLoggedInDevices: deviceData,
                onlineStatus: 0,// 0/1   0 :Online , 1: offline
                likedBy: [],
                myLikes: [],
                myunlikes: [],
                mySupperLike: [],
                supperLikeBy: [],
                disLikedUSers: [],
                matchedWith: [],
                lastUnlikedUser: [],
                recentVisitors: [],
                balance: 0,
                WalletTxns: [],
                blockedBy: [],
                myBlock: [],
                ProfileTotalViewCount: 0,
                textMessageCount: 0,
                imgCount: 0,
                videoCount: 0,
                videoCallDuration: 0,
                audioCallDuration: 0,
                creationTs: new Timestamp(),
                creationDate: new Date(),
                eduation: req.payload.eduation,
                work: req.payload.work,
                bioData: req.payload.bioData,
                location:
                {
                    longitude: req.payload.longitude || 13,
                    latitude: req.payload.latitude || 77
                },
                dontShowMyAge: 1,
                dontShowMyDist: 1,
                "count": {
                    "rewind": 0,
                    "like": 0
                },
                lastTimestamp: { rewind: 0, like: 0 },
                voipiospush: req.payload.voipiospush, apnsPush: req.payload.apnsPush
            };

            if (typeof req.payload.latitude === 'undefined' || typeof req.payload.longitude === 'undefined') {
                rabbitMq.sendToQueue(rabbitMq.update_location_queue, { isVerified: false, data: { ip: req.payload.ip, _id: userId } }, (e, r) => {
                    if (e) {
                        logger.error("worker error  : ", e);
                    }
                });
            }

            async.series([
                function (callback) {
                    /* Get Preferences to save In userList collection */
                    searchPreferencesCollection.Select({ isSearchPreference: true }, (err, result) => {
                        if (err) callback(err, result);

                        newUser.searchPreferences = [];
                        for (let index = 0; index < result.length; index++) {

                            /* add in my searchPreferences */
                            let searchPreferencesData = {
                                pref_id: result[index]._id,
                                selectedValues: (result[index].TypeOfPreference == 1) ? [result[index].options[0]] : result[index].options.map(Number)
                            }
                            if (result[index].TypeOfPreference == "3" || result[index].TypeOfPreference == "4") {
                                searchPreferencesData["selectedUnit"] = result[index].optionsUnits[0];
                            }
                            if (result[index]._id.toString() == "5d6384b9b80db667866938f3") {
                                if (gender == "Male") {
                                    searchPreferencesData["selectedValues"] = ["Female"]
                                } else {
                                    searchPreferencesData["selectedValues"] = ["Male"]
                                }
                            }
                            newUser.searchPreferences.push(searchPreferencesData);
                        }
                        callback(err, result);
                    })
                },
                function (callback) {
                    userPrefrances.Select({}, (err, result) => {
                        myPrefrances = result;
                        callback(err, result);
                    })
                },
                function (callback) {
                    for (let index = 0; index < myPrefrances.length; index++) {
                        myPrefrances[index]["isDone"] = false;
                        myPrefrances[index]["selectedValues"] = [];
                        newUser.myPreferences.push({ pref_id: myPrefrances[index]._id, isDone: false })
                    }

                    let myPreferencesByGroupObj = {};
                    for (let index = 0; index < myPrefrances.length; index++) {
                        if (myPreferencesByGroupObj[myPrefrances[index]["title"]]) {
                            myPreferencesByGroupObj[myPrefrances[index]["title"]].push(myPrefrances[index]);
                        } else {
                            myPreferencesByGroupObj[myPrefrances[index]["title"]] = [myPrefrances[index]];
                        }
                    }
                    let myPreferencesByGroupArray = [];
                    for (let key in myPreferencesByGroupObj) {
                        myPreferencesByGroupArray.push({
                            "title": key,
                            "data": myPreferencesByGroupObj[key]
                        });
                    }

                    myPrefrances = myPreferencesByGroupArray;

                    callback(null, 1);
                },
                function (callback) {
                    planCollection.SelectOne({ _id: ObjectID("5b0d52c33e05b520f1605611") }, (err, planDetails) => {
                        if (planDetails) {
                            let purchaseDate = moment().startOf('day').valueOf();
                            let expiryTime = 0;
                            if (planDetails.durationInMonths) {
                                expiryTime = moment().add(planDetails.durationInMonths, "months").startOf('day').valueOf();
                            } else {
                                expiryTime = moment().add(planDetails.durationInDays, "days").startOf('day').valueOf();
                            }
                            let purchaseTime = new Date().getTime();

                            let dataToPush = {
                                "planId": ObjectID(planDetails._id),
                                "subscriptionId": "Free Plan",
                                "purchaseDate": purchaseDate,
                                "purchaseTime": purchaseTime,
                                "userPurchaseTime": purchaseTime,
                                "durationInMonths": planDetails.durationInMonths,
                                "actualId": planDetails.actualId,
                                "actualIdForiOS": planDetails.actualIdForiOS,
                                "actualIdForAndroid": planDetails.actualIdForAndroid,
                                "expiryTime": expiryTime,
                                "likeCount": planDetails.likeCount,
                                "rewindCount": planDetails.rewindCount,
                                "superLikeCount": planDetails.superLikeCount,
                                "whoLikeMe": planDetails.whoLikeMe,
                                "whoSuperLikeMe": planDetails.whoSuperLikeMe,
                                "recentVisitors": planDetails.recentVisitors,
                                "readreceipt": planDetails.readreceipt,
                                "passport": planDetails.passport,
                                "noAdds": planDetails.noAdds,
                                "hideDistance": planDetails.hideDistance,
                                "hideAge": planDetails.hideAge,
                            };
                            newUser["subscription"] = [dataToPush];
                            callback(err, result);
                        } else {
                            callback(err, result);
                        }

                    })
                },//assign free plan from DB
                function (callback) {
                    /* insert into userList collection*/
                    userListCollection.Insert(newUser, (err, result) => {
                        callback(err, result);
                    })
                },
                function (callback) {
                    let dataToInsert = {
                        _id: newUser._id,
                        "coins": {
                            "Coin": 1000000
                        }
                    };
                    coinWalletCollection.Insert(dataToInsert, (err, result) => {
                        callback(err, result);
                    });
                },

                function (callback) {
                    /* insert into userDevices collection*/
                    userDevicesCollection.Insert(deviceData, (err, result) => {
                        callback(err, result);
                    })
                },
                function (callback) {
                    /* insert into userList collection*/
                    // delete newUser.myPreferences;
                    let lat = newUser.location.latitude;
                    let lon = newUser.location.longitude;
                    delete newUser["location"];
                    newUser["location"] = { "lat": lat, "lon": lon };
                    userList_ES.Insert(newUser, (err, result) => {
                        callback(err, result);
                    });
                    cronJobTask.sendWellComeMailOnSignUp({ email: newUser.email, firstName: newUser.firstName })
                },
                function (callback) {
                    coinWallet.Select({ _id: userId }, (err, result) => {
                        if (err) return callback(err, result);

                        coinWalletObj = [{ coins: { "Coin": 0 } }]
                        return callback(err, result);
                    });
                },
                function (callback) {
                    coinConfig.Select({}, (err, result) => {
                        if (err) return callback(err, result);

                        coinConfigObj = result[0];
                        return callback(err, result);
                    });
                }
            ], function (err, result) {
                if (err) {
                    logger.error(err);
                    return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                }
                let token = Auth.SignJWT({ _id: userId.toString(), key: 'acc' }, 'user', 6000000);
                return res({
                    message: req.i18n.__('PostFbLogin')['201'], data: {
                        token: token,
                        _id: userId,
                        firstName: req.payload.firstName,
                        gender: gender,
                        height: req.payload.height,
                        email: req.payload.email,
                        profilePic: req.payload.profilePic,
                        profileVideo: req.payload.profileVideo,
                        profileVideoThumbnail: req.payload.profileVideoThumbnail,
                        otherImages: req.payload.otherImages,
                        dob: req.payload.dob,
                        myPreferences: myPrefrances,
                    },
                   /*  coinWallet: coinWalletObj[0].coins,
                    coinConfig: coinConfigObj */
                }).code(201);
            });
        }
    })
};

let response = {
    // status: {
    //     200: { message: Joi.any().default(local['PostFbLogin']['200']), data: Joi.any() ,coinWallet:Joi.any(),coinConfig:Joi.any() },
    //     201: { message: Joi.any().default(local['PostFbLogin']['201']), data: Joi.any() ,coinWallet:Joi.any(),coinConfig:Joi.any()},
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}


module.exports = { validator, handler, response };