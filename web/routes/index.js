// const login = require('./login');
const date = require("./date");
const plan = require("./plan");
const like = require("./like");
const chats = require("./chats");
const boost = require("./boost");
const block = require("./block");
const match = require("./match");
const unLike = require("./unLike");
const rewind = require("./rewind");
const unBlock = require("./unBlock");
const profile = require('./profile');
const unMatch = require("./unMatch");
const version = require("./version");
const trigger = require("./trigger");
const location = require("./location");
const messages = require("./messages");
const coinPlan = require("./coinPlan");
const pastDates = require("./pastDates");
const checkFbId = require("./checkFbId");
const rateToApp = require("./rateToApp");
const coinConfig = require("./coinConfig");
const dateRating = require("./dateRating");
const requestOtp = require('./requestOtp');
const supperLike = require("./supperLike");
const cloudinary = require("./cloudinary");
const reportUser = require("./reportUser");
const coinHistory = require("./coinHistory");
const instagramId = require("./instagramId");
const preferences = require('./preferences');
const onlineUsers = require("./onlineUsers");
const planHistory = require("./planHistory");
const activePlans = require("./activePlans");
const completeDate = require("./completeDate");
const mobileNumber = require("./mobileNumber");
const subscription = require("./subscription");
const searchResult = require("./searchResult");
const verification = require('./verification');
const currentDates = require("./currentDates");
const dateResponse = require("./dateResponse");
const askAQuestion = require("./askAQuestion");
const pendingCalls = require("./pendingCalls");
const reportAnIssue = require("./reportAnIssue");
const coinFromVideo = require("./coinFromVideo");
const faceBookLogin = require('./faceBookLogin');
const unCompleteUser = require('./unCompleteUser');
const recentVisitors = require("./recentVisitors");
const makeASuggestion = require("./makeASuggestion");
const searchPrefrances = require('./searchPrefrances');
const verificaitonCode = require('./verificaitonCode');
const currentCoinBalance = require("./currentCoinBalance");
const messageWithoutMatch = require("./messageWithoutMatch");
 const userPost = require("./userPost");
const postLikeAndComment =require("./postLikeAndUnlike");
const postComment =require("./posComment");


module.exports = [].concat(verification, faceBookLogin, verificaitonCode, preferences, requestOtp,
    profile, unCompleteUser, searchPrefrances, searchResult, like, unLike, match, cloudinary,
    supperLike, location, instagramId, recentVisitors, unMatch, currentDates, date,
    dateResponse, dateRating, pastDates, onlineUsers, askAQuestion, makeASuggestion,
    rateToApp, reportAnIssue, reportUser, mobileNumber, subscription, coinPlan,
    currentCoinBalance, trigger, coinHistory, planHistory, plan,userPost,postComment,
    block, unBlock, chats, rewind, boost, coinConfig, messageWithoutMatch,postLikeAndComment,
    messages, activePlans, coinFromVideo, checkFbId, version, completeDate, pendingCalls
);