'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const GeoPoint = require('geopoint')
const userLikesCollection = require('../../../models/userLikes');



/**
 * @method GET like
 * @description This API is used to get likedlist.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @returns  200 : User found in database.
 * @example {"message": "User found in database", "data": [
    {
      "opponentId": "5a14013b7b334c19349aec7b",
      "firstName": "Rahul",
      "profilePic": "..........................",
      "otherImages": [
        "................",
        "...................."
      ],
      "profileVideo": "..........................",
      "otherVideos": [
        "................",
        "...................."
      ],
      "countryCode": "+91",
    "mobileNumber": "+919101819103",
                        "emailId": "rahul@appscrip.com",
                        "dateOfBirth": 1504867670000,
      "about": "hey, i am using sync 1 to 1",
      "firebaseTopic": "firebaseTopic",
      "height": 190
    }
  ] }
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : User doesnot exist in the database.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;

    getMatchdata()
        .then(function (value) {
            return res({ message: req.i18n.__('GetLikes')['200'], data: value }).code(200);
        })
        .catch(function (err) {
            return res({ message: req.i18n.__('GetLikes')['412'] }).code(412);
        })


    function getMatchdata() {
        return new Promise(function (resolve, reject) {
            let condition = [
                { "$match": { "userId": ObjectID(_id), "isMatch": { "$exists": false } } },
                { "$lookup": { "from": "userList", "localField": "targetUserId", "foreignField": "_id", "as": "Data" } },
                { "$unwind": "$Data" },
                { "$match": { "Data.isDeactive": { "$ne": true }, "Data.deleteStatus": { "$ne": 1 } } },
                { "$project": { "Data": 1, "_id": 0, "creation": "$timestamp" } },
                {
                    "$project": {
                        "opponentId": "$Data._id",
                        "creation": 1,
                        "firstName": "$Data.firstName",
                        "profilePic": "$Data.profilePic",
                        "otherImages": "$Data.otherImages",
                        "profileVideo": "$Data.profileVideo",
                        "otherVideos": "$Data.otherVideos",
                        "countryCode": "$Data.countryCode",
                        "mobileNumber": "$Data.contactNumber",
                        "emailId": "$Data.email",
                        "dateOfBirth": "$Data.dob",
                        "about": "$Data.about",
                        "firebaseTopic": "$Data.firebaseTopic",
                        "height": "$Data.height",
                        "onlineStatus": "$Data.onlineStatus",
                        "myPreferences": "$Data.myPreferences",
                        "dontShowMyAge": "$Data.dontShowMyAge",
                        "dontShowMyDist": "$Data.dontShowMyDist",
                        "location": "$Data.location",
                        "profileVideoWidth": "$Data.profileVideoWidth",
                        "profileVideoHeight": "$Data.profileVideoHeight",
                    }
                },
                { "$sort": { "creation": -1 } },
                { "$skip": offset },
                { "$limit": limit },
            ]
            userLikesCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    console.log(err)
                    return reject(err);
                } else if ( result && result.length) {

                    let point1 = new GeoPoint(req.user.location.latitude, req.user.location.longitude);
                    result.forEach(element => {
                        let point2 = new GeoPoint(element.location.latitude, element.location.longitude);
                        let distance = point1.distanceTo(point2, true).toFixed(2)//output in kilometers
                        element["distance"] = { value: distance, isHidden: element.dontShowMyDist || 0 };
                        let cur = new Date();
                        let diff = cur - element.dateOfBirth; // This is the difference in milliseconds
                        let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                        element["age"] = { value: age, isHidden: element.dontShowMyAge || 0 };
                        element["isMatched"] = false;
                        element["work"] = "";
                        element["job"] = "";
                        element["education"] = "";

                        if (element["myPreferences"]) {
                            for (let pref_index = 0; pref_index < element["myPreferences"].length; pref_index++) {
                                if (element["myPreferences"][pref_index]["pref_id"] == "5a30fda027322defa4a14638"
                                    && element["myPreferences"][pref_index]["isDone"]) {
                                    element["work"] = element["myPreferences"][pref_index]["selectedValues"][0];
                                }
                                if (element["myPreferences"][pref_index]["pref_id"] == "5a30fdd127322defa4a14649"
                                    && element["myPreferences"][pref_index]["isDone"]) {
                                    element["job"] = element["myPreferences"][pref_index]["selectedValues"][0];
                                }
                                if (element["myPreferences"][pref_index]["pref_id"] == "5a30fdfa27322defa4a14653"
                                    && element["myPreferences"][pref_index]["isDone"]) {
                                    element["education"] = element["myPreferences"][pref_index]["selectedValues"][0];
                                }
                            }
                        }
                        delete element["myPreferences"];
                        delete element["location"];

                    });
                    return resolve(result);
                } else {
                    return reject(new Error('Ooops, something broke!'));
                }

            })
        });
    }

};

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

let response = {
    status: {
        200: {
            message: Joi.any().default(local['GetLikes']['200']), data: Joi.any().example(
                [
                    {
                        "opponentId": "5a14013b7b334c19349aec7b",
                        "firstName": "Rahul",
                        "profilePic": "..........................",
                        "otherImages": [
                            "................",
                            "....................",
                        ],
                        "profileVideo": "..........................",
                        "otherVideos": [
                            "................",
                            "....................",
                        ],
                        "countryCode": "+91",
                        "mobileNumber": "+919101819103",
                        "emailId": "rahul@appscrip.com",
                        "dateOfBirth": 1504867670000,
                        "about": "hey, i am using this",
                        "firebaseTopic": "firebaseTopic",
                        "height": 190
                    }
                ]
            )
        },
        412: { message: Joi.any().default(local['GetLikes']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { handler, response, queryValidator }