'use strict'
let headerValidator = require('../../middleware/validator');
let patchLocationAPI = require('./Patch');

module.exports = [
    {
        method: 'PATCH',
        path: '/location',
        handler: patchLocationAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'location'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchLocationAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: patchLocationAPI.response
        }
    },
];