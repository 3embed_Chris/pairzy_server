const Config = process.env;
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const cryptr = new Cryptr(Config.SECRET_KEY);
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const userListCollection = require('../../../models/userList');
const unverfiedUsersCollection = require('../../../models/unverfiedUsers');
const preferenceTableCollection = require('../../../models/preferenceTable');
const searchPreferencesCollection = require('../../../models/searchPreferences');


const validator = Joi.object({
    firstName: Joi.string().required().description("First Name,Eg. Lisa/John").error(new Error('firstName is missing.')),
    // dateOfBirth: Joi.number().required().description("Date of Birth(miliseconds),Eg. 1504867670000").example(1504867670000).error(new Error('dateOfBirth is missing')),
    // password: Joi.string().required().description("Password").error(new Error('password is missing')),
    countryCode: Joi.string().required().description("Country Code, Eg. +91/+1").error(new Error('countryCode is missing')),
    mobileNumber: Joi.string().required().description("Mobile Number, Eg. 1234567890").error(new Error('mobileNumber is missing')),
    pushToken: Joi.string().required().description("Push Token, Eg. dY0xOny_uEA:APA91bE_HZQiTq").error(new Error('pushToken is missing')),
    deviceId: Joi.string().required().description("Device Id, Eg. 157875de315458000000000000000").error(new Error('deviceId is missing')),
    deviceMake: Joi.string().required().description("Device Make/Company, Eg. Samsung/Apple").error(new Error('deviceMake is missing')),
    deviceModel: Joi.string().required().description("Device Model Number, Eg. SM-N920T").error(new Error('deviceModel is missing')),
    deviceType: Joi.string().required().description("Device Type(1-IOS and 2-Android), Eg. 2").error(new Error('deviceType is missing')),
}).required();


const handler = (req, res) => {

    logger.info("req.payload ", req.payload);

    let countryCode = req.payload.countryCode;
    let mobileNumber = req.payload.mobileNumber;
    let contactNumber = countryCode + mobileNumber;
    let condition = { contactNumber: contactNumber, deleteStatus: { "$ne": 1 } }
    let randomCode = Math.floor(1000 + Math.random() * 9000);

    userListCollection.Select(condition, (err, result) => {
        if (err) {
            logger.info("userList : ", err)
            return res({ message: allMessage.genericErrMsg["500"][inMylanguage] }).code(500);
        }
        else if (result.length) {

            let userData = {
                currentLoggedInDevices: {
                    deviceId: req.payload.deviceId,
                    pushToken: req.payload.pushToken
                },
                otp: randomCode
            }
            userListCollection.UpdateById(result[0]._id, userData, (e, r) => { });

            return res({ message: allMessage.PostRegisterUser["200"][inMylanguage] }).code(200);
        }
        else {

            let userId = new ObjectID();
            let userDevices_id = new ObjectID();
            let userLocationLogs_id = new ObjectID();
            let myPrefrances = [];
            /* create objects to save in Collections */
            let newUser = {
                _id: userId,
                firstName: req.payload.firstName,
                lastName: req.payload.lastName || "",
                countryCode: countryCode,
                contactNumber: countryCode + mobileNumber,
                searchPreferences: [],
                myPreferences: [],
                registeredTimestamp: new Date().getTime(),
                profilePic: "",
                otherImages: [],
                profileVideo: "",
                otherVideos: [],
                // dob: req.payload.dateOfBirth,
                about: req.payload.about || "",
                currentLoggedInDevices: {
                    deviceId: req.payload.deviceId,
                    pushToken: req.payload.pushToken
                },
                onlineStatus: 0,// 0/1   0 :Online , 1: offline
                profileStatus: 0,// 0/1 Approved Profile / Banned Profile
                loggedInLocations: [userLocationLogs_id],
                likedBy: [],
                myLikes: [],
                myunlikes: [],
                disLikedUSers: [],
                matchedWith: [],
                lastUnlikedUser: [],
                recentVisitors: [],
                balance: 0,
                WalletTxns: [],
                BlockedBy: [],
                ProfileTotalViewCount: 0,
                textMessageCount: 0,
                imgCount: 0,
                videoCount: 0,
                videoCallDuration: 0,
                audioCallDuration: 0,
                location:
                {
                    longitude: 0,
                    latitude: 0
                },
                firebaseTopic: req.payload.deviceId,
                otp: randomCode
            }

            async.series([
                function (callback) {
                    /* Get Preferences to save In userList collection */
                    searchPreferencesCollection.Select({isSearchPreference: true}, (err, result) => {
                        if (err) callback(err, result);

                        newUser.searchPreferences = [];

                        for (index = 0; index < result.length; index++) {

                            /* add in my searchPreferences */
                            let searchPreferencesData = {
                                pref_id: result[index]._id,
                                selectedValue: (result[index].TypeOfPreference == 1) ? [result[index].options[0]] : result[index].options
                            }
                            if (result[index].TypeOfPreference == "3" || result[index].TypeOfPreference == "4") {
                                searchPreferencesData["selectedUnit"] = result[index].optionsUnits[0];
                            }
                            newUser.searchPreferences.push(searchPreferencesData);



                        }
                        callback(err, result);

                    })
                },
                function (callback) {
                    /* Get Preferences to save In userList collection */
                    preferenceTableCollection.Select({}, (err, result) => {
                        if (err) callback(err, result);


                        newUser.myPreferences = [];
                        for (index = 0; index < result.length; index++) {

                            /* add in my Preferences */
                            newUser.myPreferences.push({
                                pref_id: result[index]._id,
                                selectedValue: (result[index].TypeOfPreference == 1) ? [result[index].options[0]] : result[index].options
                            })


                        }
                        callback(err, result);

                    })
                },
                function (callback) {
                    for (let index = 0; index < myPrefrances.length; index++) {
                        myPrefrances[index]["isDone"] = false;
                        myPrefrances[index]["selectedValues"] = [];
                        newUser.myPreferences.push({ pref_id: myPrefrances[index]._id, isDone: false })
                    }
                    callback(null, 1);
                },
                function (callback) {
                    /* insert into userList collection*/
                    unverfiedUsersCollection.Insert(newUser, (err, result) => {
                        callback(err, result);
                    })
                },
                function (callback) {
                    // messages.sendSms(contactNumber, "your verification code is : " + randomCode, "verification code", "log", function (err, result) {
                    // });
                    callback(null, 1);
                }
            ], function (err, result) {
                if (err) return res({ message: allMessage.genericErrMsg["500"][inMylanguage] }).code(500);

                return res({ message: allMessage.PostRegisterUser["201"][inMylanguage] }).code(201);
            });


        }
    })
};
let response = {
    status: {
        200: { message: allMessage.PostRegisterUser["200"]["1"] },
        201: { message: allMessage.PostRegisterUser["201"]["1"] },
        400: { message: allMessage.genericErrMsg["400"]["1"] },
        500: { message: allMessage.genericErrMsg["500"]["1"] }
    }
}

module.exports = { validator, handler, response };