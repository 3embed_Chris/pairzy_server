'use strict'
let postAPI = require('./Post');
let headerValidator = require("../../middleware/validator");

module.exports = [
    {
        method: 'POST',
        path: '/login',
        handler: postAPI.handler,
        config: {
            description: 'This API will be create New user as unverified user.',
            tags: ['api', 'Login'],
            auth: false,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    }
];