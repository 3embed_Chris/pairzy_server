var conf = process.env;
var Joi = require('joi');
var async = require("async");
var ObjectID = require('mongodb').ObjectID
var jsonwebtoken = require('jsonwebtoken');
var middleWare = require("../../../Controller/dbMiddleware");
const local = require('../../../locales');
var secretKey = conf.SECRET_KEY;
let handler = (req, res) => {

    let _id = req.auth.credentials._id;

    async.waterfall([
        function (validateCB) {

            if (!req.headers.authorization) {
                return res({ message: req.i18n.__('DeletMessages')['406'] }).code(406);
            } else if (!req.params.messageIds) {
                return res({ message: req.i18n.__('DeletMessages')['400'] }).code(400);
            } else {

                jsonwebtoken.verify(req.headers.authorization, secretKey, function (jwterr, decoded) {
                    if (jwterr) {
                        return res({ message: req.i18n.__('DeletMessages')['401'] }).code(401);
                    } else {
                        validateCB(null, decoded);
                    }
                });
            }
        },
        function (JWTDecoded, funcMainCB) {

            JWTDecoded = { userId: _id };

            if (req.params.all && req.params.all == 'true') {
                /**
                 * delete all messages
                 */
            } else {
                /**
                 * delete selected messages
                 */
                if (req.params.messageIds && (Array.isArray(req.params.messageIds) && ((req.params.messageIds).length > 0))) {
                    var or_clause = []
                    try {
                        or_clause.push({ senderId: ObjectID(JWTDecoded.userId) })
                        or_clause.push({ receiverId: ObjectID(JWTDecoded.userId) })
                    } catch (exec) {
                        return res({ message: exec.message }).code(400);
                    }

                    var usr_msg_del = "members." + JWTDecoded.userId + ".del"
                    middleWare.Update("messages",
                        { [usr_msg_del]: true },
                        { $and: [{ messageId: { $in: req.params.messageIds } }, { $or: or_clause }] },
                        "Mongo",
                        function (updtErr, updtResult) {
                            if (updtErr) {
                                console.log("Error : messages 101 ", err);
                            }
                        });
                }
            }
            return res({  message: req.i18n.__('DeletMessages')['200'] }).code(200);
        }
    ]);
};

let validator = Joi.object({
    all: Joi.string().description("Delete all / selected"),
    messageIds: Joi.array().description("Array of message ids").allow([])
}).required();



module.exports = { handler, validator };