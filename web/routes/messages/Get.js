var Joi = require('joi');
var async = require("async");
var conf = process.env;
var ObjectID = require('mongodb').ObjectID
const messages = require("../../../models/messages");
const mqtt = require("../../../library/mqtt");

var secretKey = conf.SECRET_KEY;

let handler = (req, res) => {
    let _id = "";
    let JWTDecoded = { userId: req.auth.credentials._id };

    async.waterfall([
        // function (validateCB) {

        //     if (!req.params.chatId) {
        //         return res({ message: "Mandatory uniqueKey is missing" }).code(400);
        //     } else {
        //         jsonwebtoken.verify(req.headers.authorization, secretKey, function (jwterr, decoded) {
        //             if (jwterr) {
        //                 console.log("jwterr =>", jwterr);
        //                 return res({ message: "failed to authenticate, authorization is Invalid " }).code(401);
        //             } else {
        //                 validateCB(null, decoded);
        //             }
        //         });
        //     }
        // },
        function (mainFuncCB) {

            var limit = 10, skip = 0, pagesize = parseInt(req.params.pageSize);

            var _id = req.params.chatId
            var usr_msg_del = "members." + JWTDecoded.userId + ".del"
            var getMesg = [
                { "$match": { "chatId": ObjectID(req.params.chatId) } },
                { "$match": { "timestamp": { $lt: parseInt(req.params.timestamp) } } },
                { "$match": { $or: [{ "expDtime": 0 }, { "expDtime": { $gt: new Date().getTime() } }] } },
                { "$match": { [usr_msg_del]: { "$exists": false } } },
                {
                    "$project": {
                        _id: 0, messageId: 1, secretId: 1, senderId: 1, receiverId: 1, payload: 1,
                        messageType: 1, timestamp: 1, chatId: 1, userImage: 1, toDocId: 1, name: 1,
                        dTime: 1, dataSize: 1, thumbnail: 1, mimeType: 1, fileName: 1, extension: 1, members: 1
                    }
                },
                { "$sort": { "timestamp": -1 } },
                { $limit: pagesize }
            ];

            messages.Aggregate(getMesg, function (err, result) {
                if (err) {
                    console.log("err 1 : ", err)
                    return res({ message: "Unknown error occurred" }).code(503);
                }
                else if (result.length) {

                    for (index = 0; index < result.length; index++) {

                        if (result[index].messageType != "1" && result[index].messageType != "2" && result[index].messageType != "7") {
                            delete result[index].thumbnail;
                        }
                        if (result[index].dataSize == null) {
                            delete result[index].dataSize;
                        }
                    }

                    var finalRes = [], oponentUid = '', secretId = '', dTime = ''
                    /**
                     * Loop through the result and prepare the final result 
                     */
                    async.eachSeries(result, function (resObj, resCB) {

                        /**
                         * keeping the common fields out of the response array
                         * OponentID and SecretID are common for all messages
                         */
                        if (oponentUid == '') {
                            oponentUid = (resObj.senderId == JWTDecoded.userId) ? resObj.receiverId : resObj.senderId
                            secretId = resObj.secretId
                            dTime = resObj.dTime
                        }

                        var msgObj = {
                            "messageId": resObj.messageId,
                            "senderId": resObj.senderId,
                            "receiverId": resObj.receiverId,
                            "payload": resObj.payload,
                            "messageType": resObj.messageType,
                            "timestamp": resObj.timestamp,
                            "userImage": resObj.userImage,
                            "toDocId": resObj.toDocId,
                            "name": resObj.name,
                            "status": (resObj.members[resObj.receiverId].status) ? resObj.members[resObj.receiverId].status : 1,
                            "delivered": (resObj.members[resObj.receiverId].deliveredAt) ? resObj.members[resObj.receiverId].deliveredAt : -1,
                            "read": (resObj.members[resObj.receiverId].readAt) ? resObj.members[resObj.receiverId].readAt : -1,
                        }

                        /**
                         * add dTime only if the its a secret chat
                         */
                        if (secretId != '') {
                            msgObj.dTime = resObj.dTime
                        }

                        /**
                         * check if the message type is of image / video / audio / doodle
                         * and add the additional fields (thumbnail, dataSize)
                         */
                        switch (resObj.messageType) {
                            case '1':
                            case '2':
                            case '7':
                                msgObj.thumbnail = resObj.thumbnail
                                msgObj.dataSize = resObj.dataSize
                                break;
                            case '5':
                                msgObj.dataSize = resObj.dataSize
                                break;
                            case '9':
                                msgObj.dataSize = resObj.dataSize
                                msgObj.extension = resObj.extension
                                msgObj.fileName = resObj.fileName
                                msgObj.mimeType = resObj.mimeType
                                break;
                        }

                        finalRes.push(msgObj);
                        resCB(null);
                    }, function (FinalLoopErr) {
                        if (FinalLoopErr) {
                            return res({ message: finalLoopErr.message }).code(503);
                        }

                        /**
                         * Success status goes with API response. 
                         * Actual data goes through MQTT
                         */



                        mqtt.publish("GetMessages/" + JWTDecoded.userId, JSON.stringify({
                            "chatId": req.params.chatId, "opponentUid": oponentUid, "secretId": secretId, "dTime": dTime,
                            "messages": finalRes, "a": "a"
                        }), { qos: 1 }, () => { });

                        return res({
                            message: req.i18n.__('GetMessages')['200'],
                            data: finalRes
                        }).code(200);

                    })
                }
                else {
                    /** No messages to send */
                    mqtt.publish("GetMessages/" + JWTDecoded.userId, JSON.stringify({
                        "chatId": req.params.chatId, "opponentUid": '', "secretId": '', "dTime": 0, "messages": [], "b": "b"
                    }), { qos: 1 }, () => { });
                    return res({ message: req.i18n.__('GetMessages')['200'], data: {} }).code(200);

                }
            })
        }
    ])

};

let validator = Joi.object({
    chatId: Joi.string().required().description("ex : 5a281337005a4e3b65bf12a8").example("5a281337005a4e3b65bf12a8").max(24).min(24).description("chatId").error(new Error('chatId is missing or incorrect it must be 24 char || digit only')),
    timestamp: Joi.string().required().description("0").error(new Error('timestamp is missing')),
    pageSize: Joi.string().required().description("0").error(new Error('pageSize is missing'))
}).unknown();



module.exports = { handler, validator };