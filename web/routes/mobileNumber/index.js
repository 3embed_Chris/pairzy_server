let headerValidator = require('../../middleware/validator');
let PatchAPI = require('./Patch');


module.exports = [
    {
        method: 'PATCH',
        path: '/mobileNumber',
        handler: PatchAPI.APIHandler,
        config: {
            description: 'This API will be used',
            tags: ['api', 'verificaitonCode'],
            auth: 'userJWT',
            response : PatchAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PatchAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }            
        }
    }
];