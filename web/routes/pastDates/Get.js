'use strict'
const Joi = require('joi');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID
const local = require('../../../locales');
const dateScheduleCollection = require('../../../models/dateSchedule')


let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let limit = parseInt(req.query.limit) || 20;
    let offset = parseInt(req.query.offset) || 0;
    let dataToSend = {};

    getPastDatesDates()
        .then(function (value) {
            return res({
                message: req.i18n.__('PastDates')['200'],
                data: dataToSend
            }).code(200);
        })
        .catch(function (err) {
            return res({
                message: req.i18n.__('PastDates')['412']
            }).code(412);
        });

    function getPastDatesDates() {
        return new Promise(function (resolve, reject) {

            let condition = [{
                "$match": {
                    $or: [{
                        initiatedBy: ObjectID(_id)
                    }, {
                        opponentId: ObjectID(_id)
                    }],
                    "deActivate": {
                        "$exists": false
                    }
                }
            },
            {
                "$match": {
                    "expireAt": { "$lt": new Date(new Date().getTime() + (1000 * 60)) }
                }
            },
            {
                "$project": {
                    "matchId": 1,
                    "initiatedBy": 1,
                    "initiatorName": 1,
                    "iniatorProfilePic": 1,
                    "opponentId": 1,
                    "opponentProfilePic": 1,
                    "opponentName": 1,
                    "createdTimestamp": 1,
                    "chatId": 1,
                    "status": 1,
                    "acceptedDateTimeStamp": 1,
                    "dateFeedback": 1,
                    "callLogId": 1,
                    "negotiations": {
                        "$slice": ["$negotiations", -1]
                    }
                }
            },
            {
                "$unwind": "$negotiations"
            },
            {
                "$project": {
                    "opponentId": {
                        "$cond": {
                            "if": {
                                "$eq": ["$initiatedBy", ObjectID(_id)]
                            },
                            "then": "$opponentId",
                            "else": "$initiatedBy"
                        }
                    },
                    "opponentName": {
                        "$cond": {
                            "if": {
                                "$eq": ["$initiatedBy", ObjectID(_id)]
                            },
                            "then": "$opponentName",
                            "else": "$initiatorName"
                        }
                    },
                    "opponentProfilePic": {
                        "$cond": {
                            "if": {
                                "$eq": ["$initiatedBy", ObjectID(_id)]
                            },
                            "then": "$opponentProfilePic",
                            "else": "$iniatorProfilePic"
                        }
                    },
                    "data_id": "$_id",
                    "proposedOn": "$negotiations.proposedOn",
                    "acceptedDateTimeStamp": 1,
                    "dateFeedback": 1,
                    "requestedFor": "$negotiations.requestedFor",
                    "opponentResponse": "$negotiations.opponentResponse",
                    "by": "$negotiations.by"
                }
            },
            {
                "$sort": {
                    _id: -1
                }
            },
            {
                "$skip": offset
            },
            {
                "$limit": limit
            }
            ];

            // console.log("====>>>>",JSON.stringify(condition))
            dateScheduleCollection.Aggregate(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                let status = "";
                for (let index = 0; index < result.length; index++) {
                    if (result[index]["dateFeedback"]) {
                        if (result[index]["dateFeedback"][0] && result[index]["dateFeedback"][0]["reviewerId"].toString() == _id.toString()) {
                            result[index]["rate"] = result[index]["dateFeedback"][0]["rating"];
                        } else if (result[index]["dateFeedback"][1] && result[index]["dateFeedback"][1]["reviewerId"].toString() == _id.toString()) {
                            result[index]["rate"] = result[index]["dateFeedback"][1]["rating"];
                        }
                        delete result[index]["dateFeedback"];
                    }
                    status = "";
                    if (result[index]["acceptedDateTimeStamp"]) {
                        /* date accepted */
                        switch (result[index]["requestedFor"]) {
                            case "inPersonDate":
                                {
                                    status = "Inperson Date";
                                    break;
                                }
                            case "Now":
                            case "Later":
                                {
                                    status = "Call Date";
                                    break;
                                }
                        }
                    }
                    /* date not accepted */
                    if (result[index]["opponentResponse"] == "denied") {
                        status = (result[index]["by"].toString() == _id.toString()) ? "Canceled by you" : "Canceled by " + result[index]["opponentName"];
                    } else if (result[index]["opponentResponse"] == "") {
                        status = "Date expired";
                    }
                    result[index]["status"] = status;

                    if (result[index]["requestedFor"] == "audioDate") {
                        result[index]["requestedFor"] = "Audio Date";
                    } else if (result[index]["requestedFor"] == "videoDate") {
                        result[index]["requestedFor"] = "Video Date";
                    } else if (result[index]["requestedFor"] == "inPersonDate") {
                        result[index]["requestedFor"] = "In Person Date";
                    }
                }
                dataToSend = result;
                // console.log(" dataToSend ", JSON.stringify(dataToSend))
                console.log(" dataToSend ", dataToSend.length)
                return resolve("--");
            });
        });
    }
}

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
})

module.exports = {
    handler,
    queryValidator
}