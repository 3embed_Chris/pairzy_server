'use strict'
var Joi = require("joi");
var logger = require('winston');
var local = require('../../../locales');
var redisClient = require("../../../models/redisDB").client

/**
 * @function GET PendingCalls
 * @description This API is used to get PendingCalls.

 * @property {string} authorization - authorization
 * @property {string} lang - language
  
 * @returns  200 : User profile sent successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Entered User Id(token) doesnot exist,please check the entered token. 
 * @returns  500 : An unknown error has occurred.
 **/
let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let key = "callerId:" + _id || "--";

    redisClient.hgetall(key, (err, data) => {
        if (data != null) {
            logger.silly("Data found ", data)
            return res({ message: req.i18n.__('PendingCalls')['200'], data: data }).code(200);
        } else {
            logger.silly("Data not found ")
            return res({ message: req.i18n.__('PendingCalls')['412'] }).code(412);
        }
    });
};

let APIResponse = {
    status: {
        200: { message: Joi.any().default(local['PendingCalls']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        412: { message: Joi.any().default(local['PendingCalls']['412']) }
    }
}

module.exports = { APIHandler, APIResponse }