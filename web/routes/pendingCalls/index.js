'use strict'
let getAPI = require('./Get');
let headerValidator = require('../../middleware/validator');

module.exports = [
    {
        method: 'Get',
        path: '/pendingCalls',
        handler: getAPI.APIHandler,
        config: {
            description: 'This API will be use for get pending calls',
            tags: ['api', 'Date'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];