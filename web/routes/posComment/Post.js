'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
var moment = require("moment");
const local = require('../../../locales');
const userPostCollectionES = require('../../../models/userPostES');
const postCommentCollection = require('../../../models/Postcomment');
const userPostCollection = require("../../../models/userPost");
const fcmPush = require("../../../library/fcm");




let validator = Joi.object({
    postId: Joi.string().required().description("object ID of post ").error(new Error('objectId  is missing')),
    comment: Joi.string().required().description("comment of post ").error(new Error('comment  is missing'))

}).required()

let handler = (req, res) => {
    let userId = req.auth.credentials._id;
    let postId = req.payload.postId;
    let _id = new ObjectID();
    let deviceType;
    let name =req.user.firstName;

    /**
   * @method POST post
   * @description This API is use to Commnet post we update data in two collection userPost and postComment.
   * @author jaydip Haraniya
   * @date 28-05-2019
  * @param {string} userId 
  * @param {string} Commnet  
  * @param {string} Timestamp
  * @param {string} CommnetCount
  * @property {string} language in header
  * @returns  500:internal server error 
  * @returns  200:Success
  * @returns  400:Bad request
  * @returns  429:You have exceeded the max attempt
  * @returns  404:Data Not Found
   */


    /**
     * this function is use to insert data in postComment collection
     */

    const UserPostComment = () => {

        return new Promise((resolve, reject) => {

            var data = {
                _id: _id,
                userId: ObjectID(userId),
                comment: req.payload.comment,
                postId: ObjectID(postId),
                commentedOn: moment().valueOf(),
                timestamp: new Timestamp(),
                date: new Date(),
            };

            postCommentCollection.Insert(data, (err, result) => {
                logger.silly(err)
                if (result) {
                    return resolve("----");
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                }
            });
        });
    }

    /**
     *  this function is use to Update data in userPost collection mongoDB & elastic searchDB
     */

    const UserPostElastic = () => {
        let dataToUpdate = {
            '$inc': { commentCount: +1 },

        }
        new Promise(function (resolve, reject) {

            userPostCollection.UpdateByData(postId, dataToUpdate, (errrrr, ressss) => {
                if (errrrr) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                userPostCollectionES.UpdateComment(postId, (e, r) => {
                    logger.silly(e)
                    if (r) {
                        sendPush()
                    }

                })
                return resolve(true);
            });
        });
    }

    /**
        * this function is use to  to send push to all matches when user update  preferences 
        */
       function sendPush() {


        var condition = { "_id": ObjectID(req.payload.postId) }
        userPostCollection.SelectOne(condition, (err, result) => {
            logger.error(err)
            //console.log("errrrrrrrrrrrrrrrrrrrrrrr", err)
            if (result && result.userId && result.userId != userId ) {
               // console.log("resulttttt", result)
                deviceType = result.deviceType || "1";
                let request = {
                    data: {
                        type: "32", deviceType: deviceType, title: process.env.APPNAME, message: name + " has commented on your post"
                    },
                    notification: { "title": process.env.APPNAME, "body": name + " has commented on your post" }
                };
                fcmPush.sendPushToTopic(`/topics/${result.userId}`, request, () => { })



            } else {
                logger.error("fcm error : - ", err)
            }
        });




    }

    UserPostComment()
        .then(UserPostElastic)
        .then(dt => {
            return res({ message: req.i18n.__('userPostComment')['200'] }).code(200);
        }).catch(e => {
            return res({ message: e.message }).code(204);
        });
}

let response = {
    status: {
        200: { message: local['userPostComment']['200'] },
        412: { message: local['userPostComment']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}
module.exports = { handler, validator, response }