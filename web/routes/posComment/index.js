'use strict'
let CommnetAPI = require('./Post');
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'POST',
        path: '/userPostComment',
        handler: CommnetAPI.handler,
        config: {
            description: 'This API will be used to  Comment on post',
            tags: ['api', 'postComment'],
            auth: 'userJWT',
            response: CommnetAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: CommnetAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
        }
    },

    {
        method: 'GET',
        path: '/userPostComment/{postId}',
        handler: GetAPI.handler,
        config: {
            description: 'This API will be used to get a comment by postId',
            tags: ['api', 'postComment'],
            auth: 'userJWT',
            response: GetAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }

    
];