'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const local = require('../../../locales');
const userPostCollection = require("../../../models/userPost");
const ObjectID = require('mongodb').ObjectID;

/**
 * @method GET  a likers details by postId
 * @description This API use to get a likers details  by postId.
 * @author jaydip Haraniya
 * @date 29-05-2019

 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} postId postId
 
 * @returns  200 : likers detail  get successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  422 : postId  doesnot exist in the database,try with the different ID.
 * @returns  500 : An unknown error has occurred.
 */
const validator = Joi.object({
    postId: Joi.string().required().min(24).max(24).description('postId'),
}).options({ allowUnknown: true });

let handler = (req, res) => {

    getLikersDetals()
        .then(function (value) {
            return res({ message: req.i18n.__('GetLikers')['200'], data: value }).code(200);
        }).catch(function (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });

    function getLikersDetals() {
        return new Promise(function (resolve, reject) {

            let condition = [
                { "$match": { "_id": ObjectID(req.params.postId) } },
                { "$unwind": "$Likers" },
                { "$lookup": { "from": "userList", "localField": "Likers", "foreignField": "_id", "as": "Data" } },
                { "$unwind": "$Data" },
                {
                    "$project": {
                        "likersName": "$Data.firstName",
                        "PhoneNo": "$Data.contactNumber",
                        "Email": "$Data.email",
                        "dob": "$Data.dob",
                        "profilePic": "$Data.profilePic",
                        "gender": "$Data.gender",

                    }
                }


            ]
            userPostCollection.Aggregate(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                return resolve(result);
            })
        });
    }
};

let response = {
    status: {
        200: { message: local['GetLikers']['200'], data: Joi.any() },
        412: { message: local['GetLikers']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

module.exports = { handler, response, validator }