'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
var moment = require("moment");
const local = require('../../../locales');
const userPostCollectionES = require('../../../models/userPostES');
const userlikeCollection = require('../../../models/postLikes');
const userPostCollection = require("../../../models/userPost");
const fcmPush = require("../../../library/fcm");




let validator = Joi.object({
    type: Joi.number().required().description("like:1 ,unLike:2").error(new Error('type  is missing')),
    postId: Joi.string().required().description("object ID of post ").error(new Error('objectId  is missing'))
}).required()

let handler = (req, res) => {
    let type = req.payload.type;
    let userId = req.auth.credentials._id;
    let postId = req.payload.postId;
    var inc = (type == 1) ? "+=1" : "-=1"
    let _id = new ObjectID();
    let name =req.user.firstName;
    let deviceType;
  //  console.log("==============>", req.user)
    /**
   * @method POST post
   * @description This API is use to like post we update data in two collection userPost and postlike.
   * @author jaydip Haraniya
   * @date 27-05-2019
  * @param {string} userId 
  * @param {string} Type  this is for the type of post  action 1 :like  2:unLike
  * @param {string} Timestamp
  * @param {string} likeCount
  * @param {string} Likers
  * @property {string} language in header
  * @returns  500:internal server error 
  * @returns  200:Success
  * @returns  400:Bad request
  * @returns  429:You have exceeded the max attempt
  * @returns  404:Data Not Found
   */


    /**
     *this function is use to insert data in postLike collection
     *  */
    const UserPostLike = () => {

        return new Promise((resolve, reject) => {

            var data = {
                _id: _id,
                userId: ObjectID(userId),
                action: (type == 1) ? "Like" : "Unlike",
                type: type,
                postId: ObjectID(postId),
                type: type,
                likedOn: moment().valueOf(),
                timestamp: new Timestamp(),
                date: new Date(),
            };

            userlikeCollection.Insert(data, (err, result) => {
                logger.silly(err)

                if (result) {
                    return resolve("----");
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                }
            });
        });
    }

    /**
     *  this function is use to Update data in userPost collection mongo & elastic 
     */

    const UserPostLikeElastic = () => {
        let dataToUpdate = {}
        if (type == 1) {
            dataToUpdate = {
                '$inc': { likeCount: +1 },
                '$addToSet': { Likers: (ObjectID(userId)) }
            }
        } else {
            dataToUpdate = {
                '$inc': { likeCount: -1 },
                '$pull': { Likers: (ObjectID(userId)) }
            }
        }

        new Promise(function (resolve, reject) {

            userPostCollection.UpdateByData(postId, dataToUpdate, (errrrr, ressss) => {
                if (errrrr) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 204 });
                userPostCollectionES.UpdateWithPushInc(postId, "Likers", userId, inc, (e, r) => {
                    logger.silly(e)
                    if (type === 1) {
                        sendPush()
                    }
                })
                return resolve(true);
            });
        });
    }



    /**
        * this function is use to  to send push to all matches when user update  preferences 
        */
    function sendPush() {


        var condition = { "_id": ObjectID(req.payload.postId) }
        userPostCollection.SelectOne(condition, (err, result) => {
            logger.error(err)
            //console.log("errrrrrrrrrrrrrrrrrrrrrrr", err)
            if (result && result.userId && result.userId != userId ) {
               // console.log("resulttttt", result)
                deviceType = result.deviceType || "1";
                let request = {
                    data: {
                        type: "32", deviceType: deviceType, title: process.env.APPNAME, message:"Your match " +  name + " liked your post"
                    },
                    notification: { "title": process.env.APPNAME, "body":"Your match " +  name + " liked your post" }
                };
                fcmPush.sendPushToTopic(`/topics/${result.userId}`, request, () => { })



            } else {
                logger.error("fcm error : - ", err)
            }
        });




    }


    UserPostLike()
        .then(UserPostLikeElastic)
        .then(dt => {
            return res({ message: req.i18n.__('userPostLike')['200'] }).code(200);
        }).catch(e => {
            return res({ message: e.message }).code(204);
        });
}

let response = {
    status: {
        200: { message: local['userPostLike']['200'], data: Joi.any() },
        412: { message: local['userPostLike']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}
module.exports = { handler, validator, response }