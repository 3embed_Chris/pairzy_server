'use strict'
let headerValidator = require('../../middleware/validator');
let postAPI = require('./Post');
let GetAPI = require('./Get');

module.exports = [
    
    {
        method: 'POST',
        path: '/userPostLikeUnlike',
        handler: postAPI.handler,
        config: {
            description: 'This API is  use to post User post Like And Unlike.',
            tags: ['api', 'userpostLikeUnlike'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    },
    {
        method: 'GET',
        path: '/userPostLike/{postId}',
        handler: GetAPI.handler,
        config: {
            description: 'This API will be used to get a likers detail by postId',
            tags: ['api', 'userpostLikeUnlike'],
            auth: 'userJWT',
            response: GetAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];