'use strict'
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const userListCollection = require('../../../models/userList');
const unverfiedUsersCollection = require('../../../models/unverfiedUsers');
const preferenceTableCollection = require('../../../models/preferenceTable');
const userPrefrances = require('../../../models/userPrefrances');
const local = require('../../../locales');

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let newUser = {}, myPrefrances = [], userData = {};

    async.series([
        function (callback) {
            userListCollection.Select({ _id: ObjectID(_id) }, (err, result) => {

                if (result[0]) {
                    userData["myPreferences"] = result[0]["myPreferences"];
                }
                callback(err, result);
            })
        },
        function (callback) {
            /*    userPrefrances.Select({}, (err, result) => {
                   if (err) return callback(err, result);
   
                   for (let index = 0; index < result.length; index++) {
                       for (let indexPref = 0; indexPref < userData["myPreferences"].length; indexPref++) {
                           if (result[index]._id.toString() == userData["myPreferences"][indexPref].pref_id.toString()) {
                               userData["myPreferences"][indexPref]["selectedValues"] = (userData["myPreferences"][indexPref]["selectedValues"]) ? userData["myPreferences"][indexPref]["selectedValues"] : [];
                               userData["myPreferences"][indexPref]["title"] = result[index]["title"];
                               userData["myPreferences"][indexPref]["label"] = result[index]["label"];
                               userData["myPreferences"][indexPref]["options"] = result[index]["options"];
                               userData["myPreferences"][indexPref]["type"] = result[index]["type"];
                               userData["myPreferences"][indexPref]["priority"] = result[index]["priority"];
                               userData["myPreferences"][indexPref]["iconNonSelected"] = result[index]["iconNonSelected"];
                               userData["myPreferences"][indexPref]["iconSelected"] = result[index]["iconSelected"];
                           }
                       }
                   }
                   let myPreferencesByGroupObj = {};
                   for (let index = 0; index < userData["myPreferences"].length; index++) {
                       if (myPreferencesByGroupObj[userData["myPreferences"][index]["title"]]) {
                           myPreferencesByGroupObj[userData["myPreferences"][index]["title"]].push(userData["myPreferences"][index]);
                       } else {
                           myPreferencesByGroupObj[userData["myPreferences"][index]["title"]] = [userData["myPreferences"][index]];
                       }
                   }
                   let myPreferencesByGroupArray = [];
                   for (let key in myPreferencesByGroupObj) {
                       myPreferencesByGroupArray.push({
                           "title": key,
                           "data": myPreferencesByGroupObj[key]
                       });
                   }
   
                   userData["myPreferences"] = myPreferencesByGroupArray;
   
                   return callback(err, result);
               }); */
            userPrefrances.Select({ mandatory: true }, (err, data) => {
                myPrefrances = data;
                let myPrefrancesDataToSend = []
                let MyVices = []
                let MyVitals = []
                let MyVirtues = []
                userData["myPreferences"].forEach(element => {
                    myPrefrances.find(e => {
                        if (e._id == element.pref_id.toString()) {
                            switch (e.title) {
                                case "My Vices":

                                    MyVices.push({
                                        "title": e.title,
                                        "label": e.label,
                                        "type": e.type,
                                        "priority": e.priority,
                                        "mandatory": e.mandatory,
                                        "ActiveStatus": e.ActiveStatus,
                                        "pref_id": element.pref_id,
                                        "isDone": element.isDone,
                                        "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                        "options": e.options,
                                        "priority": e.Priority
                                    })
                                    break;
                                case "My Vitals":
                                    MyVitals.push({
                                        "title": e.title,
                                        "label": e.label,
                                        "type": e.type,
                                        "priority": e.priority,
                                        "mandatory": e.mandatory,
                                        "ActiveStatus": e.ActiveStatus,
                                        "pref_id": element.pref_id,
                                        "isDone": element.isDone,
                                        "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                        "options": e.options,
                                        "priority": e.Priority
                                    })
                                    break;
                                case "My Virtues":
                                    MyVirtues.push({
                                        "title": e.title,
                                        "label": e.label,
                                        "type": e.type,
                                        "priority": e.priority,
                                        "mandatory": e.mandatory,
                                        "ActiveStatus": e.ActiveStatus,
                                        "pref_id": element.pref_id,
                                        "isDone": element.isDone,
                                        "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                        "options": e.options,
                                        "priority": e.Priority
                                    })
                                default:
                                    break;
                            }
                            return true
                        }
                    })
                });
                myPrefrancesDataToSend.push({
                    "title": "My Vices",
                    "data": MyVices
                })
                myPrefrancesDataToSend.push({
                    "title": "My Vitals",
                    "data": MyVitals
                })
                myPrefrancesDataToSend.push({
                    "title": "My Virtues",
                    "data": MyVirtues
                })
                userData["myPreferences"] = myPrefrancesDataToSend;

                //myPrefrances = myPrefrancesDataToSend
                //console.log("result[0].myPreferences", myPrefrancesDataToSend)
                callback(err, data);
            })
        }

    ], function (err, result) {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

        return res({
            message: req.i18n.__('GetPreferences')['200'],
            data: { myPreferences: userData["myPreferences"] }
        }).code(200);
    });
};

module.exports = { APIHandler }