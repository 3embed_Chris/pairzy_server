'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const userPostCollectionES = require('../../../models/userPostES');
const userPostCollection = require("../../../models/userPost");
const local = require('../../../locales');
const userListCollection = require('../../../models/userList');
var moment = require("moment");
const Timestamp = require('mongodb').Timestamp;
const userListType = require("../../../models/userListType");
const cronJObFile = require("../../../cronJobTask");
const fcmPush = require("../../../library/fcm");

let payloadValidator = Joi.object({
    pref_id: Joi.string().required().description("enter pref_id").example("5a2fd81901ba4b2ea9c6314c").error(new Error('pref_id is missing')),
    values: Joi.array().required().description("enter values").example(["value1", "value2"]).error(new Error('values is missing')),
}).required();


let APIHandler = (req, res) => {
    let _id = req.auth.credentials._id;
    let pref_id = req.payload.pref_id;
    let values = req.payload.values;
    let heightInCMObj = cronJObFile.heightInCMObj;
    let heightInFeetObj = cronJObFile.heightInFeetObj;
    let deviceType;

    let condition = { "_id": ObjectID(_id), "myPreferences": { "$elemMatch": { "pref_id": ObjectID(pref_id) } } };
    let dataToUpdate = { "myPreferences.$.selectedValues": values, "myPreferences.$.isDone": true };

    if (pref_id == "5a30fa6d27322defa4a14550") {

        dataToUpdate["height"] = heightInCMObj[values[0]] || 160;
        dataToUpdate["heightInFeet"] = heightInFeetObj[values[0]] || `5'3"`;

        let Data = { "height": dataToUpdate["height"], "heightInFeet": dataToUpdate["heightInFeet"] };
        userListType.Update(_id, Data, () => { });
    }
    /**
         * this function is use to create a post when user update preferences detail
         */
    function createPostForBioUpdate() {

        return new Promise(function (resolve, reject) {
            let typeFlag = 1;
            let Type = "bioUpdate";
            let userId = _id;
            let url = req.user.profilePic
            let _ID = new ObjectID();
            var data;

            data = {
                _id: _ID,
                userId: ObjectID(userId),
                targetId: ObjectID(userId),
                userName: req.user.firstName,
                profilePic: req.user.profilePic,
                type: Type,
                typeFlag: typeFlag,
                postedOn: moment().valueOf(),
                createdOn: new Timestamp(),
                date: new Date(),
                description: "bio Update",
                url: [url],
                likeCount: 0,
                commentCount: 0,
                Likers: [],
                commenters: [],
                longitude :req.user.location.longitude || 0,
                latitude : req.user.location.latitude || 0,
            }

            userPostCollection.Insert(data, (err, result) => {
                logger.silly(err)
                if (result) {
                    userPostCollectionES.Insert(data, (e, r) => {
                        logger.silly(e)
                    })
                    return resolve("--done--");
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                }
            });
        });

    }
    /**
         * this function is use to  update user preferences 
         */
    function updateMyPreferences() {
        return new Promise(function (resolve, reject) {
            userListCollection.Update(condition, dataToUpdate, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                } else {
                    userListCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                        if (result) {
                            userListType.Update(_id, { myPreferences: result.myPreferences }, () => {
                                //sendPush()
                            });
                        }
                        return resolve(true);
                    });
                }
            });
        });
    }
    /**
         * this function is use to  to send push to all matches when user update  preferences 
         */
    function sendPush() {


        var condition = { _id: { "$in": req.user.matchedWith } }
        userListCollection.Select(condition, (err, result) => {
            logger.error(err)
            if (result && result.length > 0) {
               // console.log("resulttttt", result)
                deviceType = result.deviceType || "1";
                result.forEach(element => {

                    let request = {
                        data: {
                            type: "30", deviceType: deviceType, title: process.env.APPNAME, message: "Your match " + req.user.firstName + " has an updated profile"
                        },
                        notification: { "title": process.env.APPNAME, "body": "Your match " + req.user.firstName + " has an updated profile" }
                    };
                    fcmPush.sendPushToTopic(`/topics/${element._id}`, request, () => { })

                })


            } else {
                logger.error("fcm error : - ", err)
            }
        });

       


    }

    updateMyPreferences()
        //.then(createPostForBioUpdate)
        .then(dt => {
            return res({ message: req.i18n.__('PatchPreference')['200'] }).code(200);
        }).catch(e => {
            return res({ message: e.message }).code(e.code);

        })

};

module.exports = { APIHandler, payloadValidator }