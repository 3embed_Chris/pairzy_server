'use strict'
let headerValidator = require("../../middleware/validator")
let PatchAPI = require('./Patch');
let GetAPI = require('./Get');


module.exports = [
    {
        method: 'PATCH',
        path: '/preferences',
        handler: PatchAPI.APIHandler,
        config: {
            description: `This API use for signup or login from facebook with facebook tokenZZZZZ`,
            tags: ['api', 'preferences'],
            auth: "userJWT",
            validate: {
                payload: PatchAPI.payloadValidator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/preferences',
        handler: GetAPI.APIHandler,
        config: {
            description: `This API use for signup or login from facebook with facebook tokenXXX`,
            tags: ['api', 'preferences'],
            auth: "userJWT",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];