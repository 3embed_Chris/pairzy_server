'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userListType = require('../../../models/userListType');
const userListCollection = require('../../../models/userList');
const chatListCollection = require('../../../models/chatList');

let APIHandler = (req, res) => {
    let _id = req.auth.credentials._id;

    updateProfileStatus()
        .then(value => {
            return deleteChatHistory();
        })
        .then(value => {
            return deletepairFromEs();
        })
        .then(value => {
            return res({ message: req.i18n.__('DeleteProfile')['200'] }).code(200);
        }).catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });

    function updateProfileStatus() {
        return new Promise(function (resolve, reject) {
            let dataToUpdate = {
                deleteStatus: 1,
                deleteTimeStamp: new Date().getTime()
            };
            /*  userListType.Update(_id, dataToUpdate, (err, result) => {
                 logger.error("userListType ", (err) ? err : "")
             }); */
            userListType.DeleteUser(_id, (err, result) => {
                logger.error("delete ", (err) ? err : "")
            });
            userListCollection.UpdateById(_id, dataToUpdate, (err, result) => {
                logger.error("userListCollection ", (err) ? err : "")
            });
            return resolve(true);
        });
    }
    function deleteChatHistory() {
        return new Promise(function (resolve, reject) {
            var userID = "members." + _id;
            let condition = {
                [userID]: { "$exists": true }
            }
            let dataToUpdate = {
                isDeleted: 1,
            };
            chatListCollection.Update(condition, dataToUpdate, (err, result) => {
                logger.error("userListType ", (err) ? err : "")
            });
            // userListCollection.UpdateById(_id, dataToUpdate, (err, result) => {
            //     logger.error("userListCollection ", (err) ? err : "")
            // });
            return resolve(true);
        });
    }
    function deletepairFromEs() {
        return new Promise(function (resolve, reject) {
            let Ids = [];
            Ids.push({
                "bool": {
                    "must": [
                        {
                            "match": {
                                "user1": _id
                            }
                        }
                    ]
                }
            },
                {
                    "bool": {
                        "must": [

                            {
                                "match": {
                                    "user2": _id
                                }
                            }
                        ]
                    }
                })
            userListType.DeletePairData(Ids, (err, result) => {

                if (err) {
                    logger.error("userListType ", (err) ? err : "")

                } else {
                    return resolve(true);

                }

            });
        });
    }
};

let response = {
    status: {
        200: { message: local['DeleteProfile']['200'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

module.exports = { APIHandler, response }