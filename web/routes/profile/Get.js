'use strict'
const logger = require('winston');
const Promise = require('promise');
const db = require('../../../models/userList');
const userPost = require('../../../models/userPost');
const userPrefrances = require("../../../models/userPrefrances");
const Joi = require("joi");
const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const GeoPoint = require('geopoint')

/**
 * @function GET profile
 * @description This API is used to get Profile.

 * @property {string} authorization - authorization
 * @property {string} lang - language
  
 * @returns  200 : User profile sent successfully.
 * @example {
  "message": "User profile sent successfully.",
  "data": {
    "firstName": "Rahul Sharma",
    "countryCode": "+91",
    "contactNumber": "+919182736451",
    "gender": "Male",
    "email": "example@domain.com",
    "dob": 1234567,
    "about": "You want app call to me",
    "height": 200
  }
}
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Entered User Id(token) doesnot exist,please check the entered token. 
 * @returns  500 : An unknown error has occurred.
 **/
let APIHandler = (req, res) => {
    let moments = []
    let _id = req.auth.credentials._id;
    let requiredField = {
        firstName: 1, countryCode: 1, contactNumber: 1, gender: 1,
        email: 1, dob: 1, about: 1, height: 1, profilePic: 1, profileVideo: 1, profileVideoThumbnail: 1,
        otherImages: 1, myPreferences: 1, instaGramProfileId: 1, instaGramToken: 1,
        dontShowMyAge: 1, dontShowMyDist: 1, boost: 1, count: 1
    };

    getUserData()
        .then(function (value) {
            return getPrefrances(value);
        })
        .then(function (value) {
            return getUserMoments(value);
        })
        .then(function (value) {
            return res({ message: req.i18n.__('GetProfile')['200'], data: value }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: req.i18n.__('GetProfile')['412'] }).code(412);
        });


    function getUserData() {
        return new Promise(function (resolve, reject) {
            db.SelectById({ _id: _id }, requiredField, (err, result) => {
                if (result) {

                    let cur = new Date();
                    let diff = cur - result.dob; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25

                    result["emailId"] = result.email;
                    result["mobileNumber"] = result.contactNumber;
                    result["dateOfBirth"] = result.dob;
                    result["profilePic"] = result.profilePic || "";
                    result["otherImages"] = result.otherImages || [];
                    result["profileVideo"] = result.profileVideo || "";
                    result["profileVideoThumbnail"] = result.profileVideoThumbnail || "";
                    result["instaGramProfileId"] = result.instaGramProfileId || "";
                    result["instaGramToken"] = result.instaGramToken || "";
                    result["myPreferences"] = result.myPreferences || [];
                    result["about"] = result.about || "";
                    result["age"] = { value: age, isHidden: result.dontShowMyAge || 0 };
                    result["distance"] = { value: 0, isHidden: result.dontShowMyDist || 0 };
                    result["boostExpiryTime"] = (result.boost && result.boost.expiryTime) ? result.boost.expiryTime : 0;

                    delete result.email;
                    delete result.contactNumber;
                    resolve(result);
                }
                reject("-")
            })
        });
    }

    function getPrefrances(userData) {
        return new Promise(function (resolve, reject) {
            userPrefrances.Select({ mandatory: true }, (err, result) => {
                if (err) reject("--");
                let myPrefrancesDataToSend = []
                let MyVices = []
                let MyVitals = []
                let MyVirtues = []
                let myPrefrances = []
                myPrefrances = result;

                /* for (let index = 0; index < result.length; index++) {
                    for (let indexPref = 0; indexPref < userData["myPreferences"].length; indexPref++) {
                        if (result[index]._id.toString() == userData["myPreferences"][indexPref].pref_id.toString()) {
                            userData["myPreferences"][indexPref]["selectedValues"] = (userData["myPreferences"][indexPref]["selectedValues"]) ? userData["myPreferences"][indexPref]["selectedValues"] : [];
                            userData["myPreferences"][indexPref]["title"] = result[index]["title"];
                            userData["myPreferences"][indexPref]["label"] = result[index]["label"];
                            userData["myPreferences"][indexPref]["options"] = result[index]["options"];
                            userData["myPreferences"][indexPref]["type"] = result[index]["type"];
                            userData["myPreferences"][indexPref]["priority"] = result[index]["priority"];
                            userData["myPreferences"][indexPref]["iconNonSelected"] = result[index]["iconNonSelected"];
                            userData["myPreferences"][indexPref]["iconSelected"] = result[index]["iconSelected"];
                        }
                    }
                }

                let myPreferencesByGroupObj = {};
                for (let index = 0; index < userData["myPreferences"].length; index++) {
                    if (myPreferencesByGroupObj[userData["myPreferences"][index]["title"]]) {
                        myPreferencesByGroupObj[userData["myPreferences"][index]["title"]].push(userData["myPreferences"][index]);
                    } else {
                        myPreferencesByGroupObj[userData["myPreferences"][index]["title"]] = [userData["myPreferences"][index]];
                    }
                }
                let myPreferencesByGroupArray = [];
                for (let key in myPreferencesByGroupObj) {
                    myPreferencesByGroupArray.push({
                        "title": key,
                        "data": myPreferencesByGroupObj[key]
                    });
                } */

                userData["myPreferences"].forEach(element => {
                    myPrefrances.find(e => {
                        if (e._id == element.pref_id.toString()) {
                            switch (e.title) {
                                case "My Vices":

                                    MyVices.push({
                                        "title": e.title,
                                        "label": e.label,
                                        "type": e.type,
                                        "priority": e.priority,
                                        "mandatory": e.mandatory,
                                        "ActiveStatus": e.ActiveStatus,
                                        "pref_id": element.pref_id,
                                        "isDone": element.isDone,
                                        "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                        "options": e.options
                                    })
                                    break;
                                case "My Vitals":
                                    MyVitals.push({
                                        "title": e.title,
                                        "label": e.label,
                                        "type": e.type,
                                        "priority": e.priority,
                                        "mandatory": e.mandatory,
                                        "ActiveStatus": e.ActiveStatus,
                                        "pref_id": element.pref_id,
                                        "isDone": element.isDone,
                                        "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                        "options": e.options
                                    })
                                    break;
                                case "My Virtues":
                                    MyVirtues.push({
                                        "title": e.title,
                                        "label": e.label,
                                        "type": e.type,
                                        "priority": e.priority,
                                        "mandatory": e.mandatory,
                                        "ActiveStatus": e.ActiveStatus,
                                        "pref_id": element.pref_id,
                                        "isDone": element.isDone,
                                        "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                        "options": e.options
                                    })
                                default:
                                    break;
                            }
                            return true
                        }
                    })
                });
                myPrefrancesDataToSend.push({
                    "title": "My Vices",
                    "data": MyVices
                })
                myPrefrancesDataToSend.push({
                    "title": "My Vitals",
                    "data": MyVitals
                })
                myPrefrancesDataToSend.push({
                    "title": "My Virtues",
                    "data": MyVirtues
                })
                myPrefrances = myPrefrancesDataToSend
                //console.log("result[0].myPreferences", myPrefrancesDataToSend)
                userData["myPreferences"] = myPrefrancesDataToSend;

                let values = {
                    "_id": userData["_id"],
                    "firstName": userData["firstName"],
                    "countryCode": userData["countryCode"],
                    "mobileNumber": userData["mobileNumber"],
                    "emailId": userData["emailId"],
                    "gender": userData["gender"],
                    "height": userData["height"],
                    "dateOfBirth": userData["dateOfBirth"],
                    "dob": userData["dob"],
                    "profilePic": userData["profilePic"],
                    "otherImages": userData["otherImages"],
                    "profileVideo": userData["profileVideo"],
                    "profileVideoThumbnail": userData["profileVideoThumbnail"],
                    "instaGramProfileId": userData["instaGramProfileId"],
                    "instaGramToken": userData["instaGramToken"],
                    "myPreferences": userData["myPreferences"],
                    "about": userData["about"],
                    "age": userData["age"],
                    "distance": userData["distance"],
                    "boostExpiryTime": userData["boostExpiryTime"],
                    "count": userData["count"],
                    //"moments": moments
                };

                resolve(values);

            });
        });
    }

    function getUserMoments(value) {
        let liked = false
        //console.log("=======>", value)
        return new Promise(function (resolve, reject) {
            // let cnd = {
            //     $and: [{ userId: ObjectID(_id) }, { typeFlag: { $ne: 5 } }],
            //     $and: [{ userId: { $in: req.user.matchedWith } }, { typeFlag: { $eq: 5 } }],
            //     status: { $ne: 1 }

            // }
            userPost.SelectByIdPost({ userId: ObjectID(_id), status: { $ne: 1 }, typeFlag: { $ne: 5 } }, {}, { 'postedOn': -1 }, (err, result) => {
                if (result) {
                    result.forEach(e => {
                        if (e.Likers && e.Likers.map(id => id.toString()).includes(_id)) {

                            liked = true
                            // console.log("its is alreay like")
                        } else {
                            liked = false
                            // console.log("u can like not like")

                        }

                        let point1 = new GeoPoint(req.user.location.latitude, req.user.location.longitude);
                        let point2 = new GeoPoint(e.latitude, e.longitude);
                        let distance = point1.distanceTo(point2, true).toFixed(2)//output in kilometers
                        moments.push({
                            "liked": liked,
                            "postId": e._id,
                            "userId": e.userId,
                            "userName": e.userName,
                            "type": e.type,
                            "typeFlag": e.typeFlag,
                            "postedOn": e.postedOn,
                            "profilePic": e.profilePic,
                            "date": e.date,
                            "description": e.description,
                            //"status": e.status,
                            "url": e.url,
                            "likeCount": e.likeCount,
                            "commentCount": e.commentCount,
                            "distance": (distance) ? distance : 0
                        })


                    });
                    moments.sort(function (a, b) {
                        //console.log("a",a.postedOn,"b",b.postedOn)
                        return Number(b.postedOn) - Number(a.postedOn);
                    });

                    value["moments"] = moments

                    resolve(value);

                }
                else {
                    moments = [];
                }

            })
        });
    }
};
let APIResponse = {
    status: {
        200: {
            message: local['GetProfile']['200'], data: Joi.any().example(
                {

                    "_id": "5ad099568cbafd68e03dad4f",
                    "firstName": "Rahul",
                    "countryCode": "+91",
                    "mobileNumber": "+919620826143",
                    "emailId": "rahul@mobifyi.com",
                    "gender": "Male",
                    "height": 165,
                    "dateOfBirth": 716730527604,
                    "dob": 716730527604,
                    "profilePic": "https://s3.amazonaws.com/datum-premium/datum-premium/profilePicture/IMAGE_20180413_171911.jpg",
                    "otherImages": [],
                    "profileVideo": "",
                    "profileVideoThumbnail": "",
                    "myPreferences": [
                        {
                            "pref_id": "5a30ff2627322defa4a146a8",
                            "isDone": true,
                            "selectedValues": [
                                "Hindu"
                            ],
                            "title": "My Virtues",
                            "label": "Religious beliefs",
                            "options": [
                                "Buddhist",
                                "Catholic",
                                "Christian",
                                "Hindu",
                                "Jewish",
                                "Muslim",
                                "Shinto",
                                "Agnostic",
                                "Athesist",
                                "Other"
                            ],
                            "type": 2,
                            "priority": 13,
                            "iconNonSelected": "http://107.170.105.134/datum_2.0-server/My_Virtues_Screen/religion_off@2x.png",
                            "iconSelected": "http://107.170.105.134/datum_2.0-server/My_Virtues_Screen/religion_on@2x.png"
                        },
                        {
                            "pref_id": "5a30fa6d27322defa4a14550",
                            "isDone": true,
                            "selectedValues": [
                                "5.5(165 cm)"
                            ],
                            "title": "My Vitals",
                            "label": "Height",
                            "options": [
                                "5.3(160 cm)",
                                "5.4(163 cm)",
                                "5.5(165 cm)",
                                "5.6(168 cm)",
                                "5.7(170 cm)"
                            ],
                            "type": 10,
                            "priority": 5,
                            "iconNonSelected": "http://107.170.105.134/datum_2.0-server/My_Vitals_Screen_Assets/height_off@2x.png",
                            "iconSelected": "http://107.170.105.134/datum_2.0-server/My_Vitals_Screen_Assets/height_on@2x.png"
                        }
                    ]
                }
            )
        },
        412: { message: local['GetProfile']['412'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] },
    }
}

module.exports = { APIHandler, APIResponse }