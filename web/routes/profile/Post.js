'use strict'
const Config = process.env;
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
var moment = require("moment");

const local = require('../../../locales');
const cronJobTask = require('../../../cronJobTask');
const otps_mongoDB = require('../../../models/otps');
const planCollection = require("../../../models/plans");
const coinConfig = require('../../../models/coinConfig');
const Auth = require("../../middleware/authentication.js");
const userList_ES = require("../../../models/userListType");
const userList_mongoDB = require('../../../models/userList');
const userPrefrances = require("../../../models/userPrefrances");
const coinWalletCollection = require("../../../models/coinWallet")
const userDevicesCollection = require("../../../models/userDevices")
const searchPreferencesCollection = require("../../../models/searchPreferences");



let validator = Joi.object({
    countryCode: Joi.string().required().description("Country Code only, Eg. +91").error(new Error('countryCode is missing')),
    contactNumber: Joi.string().required().description("Country Code and Phonenumber(without space), Eg. +919620826142").error(new Error('contactNumber is missing')),
    otp: Joi.number().required().description("Enter OTP(6-digit), Eg. 111111").error(new Error('otp is missing')),
    email: Joi.string().email().required().description("emailId, Eg. somone@example.com").error(new Error('email is missing or invalid')),
    firstName: Joi.string().required().description("firstName, Eg.Rahul").error(new Error('firstName is missing')),
    dob: Joi.number().required().description("dob Timestamp in milliseconds, Eg.694943017000").error(new Error('dob is missing or invalid')),
    longitude: Joi.number().description("longitude").error(new Error('longitude is missing or invalid')),
    latitude: Joi.number().description("latitude").error(new Error('latitude is missing or invalid')),
    gender: Joi.number().required().min(1).max(2).description("gender 1 : Male, 2 : Female, Eg.1").default(1).error(new Error('gender is missing or invalid')),
    profilePic: Joi.string()
        .description("photo Link, Eg. https://res.cloudinary.com/closebrace/image/upload/w_400/v1491315007/usericon_id76rb.png")
        .default("https://res.cloudinary.com/closebrace/image/upload/w_400/v1491315007/usericon_id76rb.png")
        .error(new Error('profilePic is missing')),
    profileVideo: Joi.string().allow("").description("profileVideo").error(new Error('profileVideo is missing')),
    profileVideoThumbnail: Joi.string().allow("").description("profileVideoThumbnail").error(new Error('profileVideoThumbnail is missing')),

    pushToken: Joi.string().allow("").description("Push Token, Eg. dY0xOny_uEA:APA91bE_HZQiTq").error(new Error('pushToken is missing')),
    deviceId: Joi.string().allow("").description("Device Id, Eg. 157875de315458000000000000000").error(new Error('deviceId is missing')),
    deviceMake: Joi.string().allow("").description("Device Make/Company, Eg. Samsung/Apple").error(new Error('deviceMake is missing')),
    deviceModel: Joi.string().allow("").description("Device Model Number, Eg. SM-N920T").error(new Error('deviceModel is missing')),
    deviceType: Joi.string().allow("").description("Device Type(1-IOS and 2-Android), Eg. 2").error(new Error('deviceType is missing')),
    deviceOs: Joi.string().allow("").description("deviceOs, Eg. Oreo").error(new Error('deviceOs is missing')),
    appVersion: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('appVersion is missing')),

    profileVideoWidth: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('profileVideoWidth is missing')),
    profileVideoHeight: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('profileVideoWidth is missing')),
    voipiospush: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('voipiospush is missing')),
    apnsPush: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('apnsPush is missing')),
    // isIosAppInDevelopment: Joi.boolean().default(false).description("isIosAppInDevelopment, Eg. 1.0.0").error(new Error('isIosAppInDevelopment is missing')),

}).unknown()

let handler = (req, res) => {

    let randomCode = req.payload.otp;
    let firstName = req.payload.firstName;
    let dob = req.payload.dob;
    let gender = (req.payload.gender == 1) ? "Male" : "Female";
    let email = req.payload.email.toLowerCase();
    let profilePic = req.payload.profilePic;
    let countryCode = req.payload.countryCode;
    let contactNumber = req.payload.contactNumber;

    let dataToSend = {};

    try {
        checkOTP()
            .then((value) => { return createUser(); })
            .then(() => { return getCoinConfig(); })
            .then((value) => { return res({ message: req.i18n.__('PostProfile')['200'], data: dataToSend, coinConfig: value.coinConfig }).code(200); })
            .catch((err) => {
                logger.info('Caught an error!', err);
                return res({ message: req.i18n.__('PostProfile')['412'] }).code(412);
            });
    } catch (error) {
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }
    function checkOTP() {
        return new Promise(function (resolve, reject) {
            let condition = [
                { "$match": { "phone": contactNumber, "type": 1, "otp": randomCode } },
                { "$sort": { time: -1 } }, { "$limit": 1 }
            ];
            otps_mongoDB.Aggregate(condition, (err, result) => {
                if (result[0]) {
                    return resolve("----");
                } else {
                    return reject(new Error('Ooops, something broke! at checkOTP'));
                }
            });
        });
    }
    function createUser() {
        return new Promise(function (resolve, reject) {

            let condition = { "contactNumber": contactNumber, deleteStatus: { "$ne": 1 } };

            userList_mongoDB.Select(condition, (err, result) => {
                if (err) return reject("---");

                if (result && result[0] && result[0].profileStatus) {
                    return reject({ message: req.i18n.__('genericErrMsg')['403'], code: 403 });
                } else if (result && result[0]) {

                    let cur = new Date();
                    let diff = cur - result[0].dob; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25

                    dataToSend["age"] = { value: age, isHidden: result[0].dontShowMyAge || 0 };
                    dataToSend["distance"] = { value: 0, isHidden: result[0].dontShowMyDist || 0 };
                    dataToSend["_id"] = result[0]._id;
                    dataToSend["firstName"] = result[0].firstName;
                    dataToSend["countryCode"] = result[0].countryCode;
                    dataToSend["contactNumber"] = result[0].contactNumber;
                    dataToSend["dob"] = result[0].dob;
                    dataToSend["profilePic"] = result[0].profilePic;
                    dataToSend["email"] = result[0].email;
                    dataToSend["gender"] = result[0].gender;
                    let token = Auth.SignJWT({ _id: result[0]._id, key: 'acc' }, 'user', "6000000");
                    dataToSend["token"] = token;
                    return resolve("---");
                } else {

                    let _id = new ObjectID();

                    dataToSend = {
                        _id: _id,
                        firstName: firstName,
                        countryCode: countryCode,
                        contactNumber: contactNumber,
                        dob: dob,
                        profilePic: profilePic,
                        email: email,
                        gender: gender,
                        registeredTimestamp: new Date().getTime(),
                        creationTs: new Timestamp(),
                        creationDate: new Date(),
                        searchPreferences: [],
                        "location": {
                            "longitude": req.payload.longitude || 0,
                            "latitude": req.payload.latitude || 0
                        },
                        currentLoggedInDevices: {
                            deviceId: req.payload.deviceId || "",
                            pushToken: req.payload.pushToken || "",
                            deviceMake: req.payload.deviceMake || "",
                            deviceModel: req.payload.deviceModel || "",
                            deviceType: req.payload.deviceType || "",
                            deviceOs: req.payload.deviceOs || "",
                            appVersion: req.payload.appVersion || "",
                            DevicetypeMsg: (req.payload.deviceType == "1") ? "iOS" : "Android",
                        },
                        profileVideo: req.payload.profileVideo || "",
                        profileVideoThumbnail: req.payload.profileVideoThumbnail || "",
                        myPreferences: [],
                        height: 160,
                        heightInFeet: `5'3"`,
                        onlineStatus: 1,// 0/1   1 :Online , 0: offline
                        likedBy: [],
                        myLikes: [],
                        myunlikes: [],
                        mySupperLike: [],
                        supperLikeBy: [],
                        disLikedUSers: [],
                        matchedWith: [],
                        lastUnlikedUser: [],
                        recentVisitors: [],
                        blockedBy: [],
                        myBlock: [],
                        supperLikeByHistory: [],
                        count: { rewind: 0, like: 0 },
                        lastTimestamp: { rewind: 0, like: 0 },
                        profileVideoWidth: req.payload.profileVideoWidth || "",
                        profileVideoHeight: req.payload.profileVideoHeight || "",
                        "userType": "Normal",
                        voipiospush: req.payload.voipiospush,
                        apnsPush: req.payload.apnsPush,
                        // isIosAppInDevelopment: req.payload.isIosAppInDevelopment
                    }

                    let myPrefrances = [];
                    async.series([
                        function (callback) {
                            userPrefrances.Select({ mandatory: true }, (err, result) => {
                                myPrefrances = result;
                                callback(err, result);
                            })
                        },
                        function (callback) {
                            for (let index = 0; index < myPrefrances.length; index++) {
                                myPrefrances[index]["isDone"] = false;
                                myPrefrances[index]["selectedValues"] = [];
                                dataToSend.myPreferences.push({ pref_id: myPrefrances[index]._id, isDone: false })
                            }
                            callback(null, 1);
                        },
                        function (callback) {
                            /* Get Preferences to save In userList collection */
                            searchPreferencesCollection.Select({ isSearchPreference: true }, (err, result) => {
                                if (err) callback(err, result);
                                // console.log("=========>",result)
                                dataToSend.searchPreferences = [];
                                for (let index = 0; index < result.length; index++) {

                                    /* add in my searchPreferences */
                                    let searchPreferencesData = {
                                        pref_id: result[index]._id,
                                        selectedValues: (result[index].type == 1 || result[index].type == 2) ? [result[index].options[0]] : result[index].options.map(Number)
                                    }
                                    if (result[index].type == 3 || result[index].type == 4) {
                                        searchPreferencesData["selectedUnit"] = result[index].optionsUnits[0];
                                    }
                                    if (result[index]._id.toString() == "5d6384b9b80db667866938f3") {
                                        if (dataToSend.gender == "Male") {
                                            searchPreferencesData["selectedValues"] = ["Female"]
                                        } else {
                                            searchPreferencesData["selectedValues"] = ["Male"]
                                        }
                                    }
                                    dataToSend.searchPreferences.push(searchPreferencesData);
                                }
                                callback(err, result);
                            })
                        },
                        function (callback) {
                            let deviceData = {
                                userId: _id,
                                creationDate: new Date(),
                                deviceId: req.payload.deviceId || "",
                                pushToken: req.payload.pushToken || "",
                                deviceMake: req.payload.deviceMake || "",
                                deviceModel: req.payload.deviceModel || "",
                                deviceType: req.payload.deviceType || "",
                                deviceOs: req.payload.deviceOs || "",
                                appVersion: req.payload.appVersion || "",
                                DevicetypeMsg: (req.payload.deviceType == "1") ? "iOS" : "Android",
                            };
                            /* insert into userDevices collection*/
                            userDevicesCollection.Insert(deviceData, (err, result) => {
                                callback(err, result);
                            });
                        },
                        function (callback) {
                            let dataToInsert = {
                                _id: _id,
                                "coins": {
                                    "Coin": 1000000
                                }
                            };
                            coinWalletCollection.Insert(dataToInsert, (err, result) => {
                                callback(err, result);
                            });
                        },
                        function (callback) {
                            planCollection.SelectOne({ _id: ObjectID("5b0d52c33e05b520f1605611") }, (err, planDetails) => {
                                if (planDetails) {
                                    let purchaseDate = moment().startOf('day').valueOf();
                                    let expiryTime = 0;
                                    if (planDetails.durationInMonths) {
                                        expiryTime = moment().add(planDetails.durationInMonths, "months").startOf('day').valueOf();
                                    } else {
                                        expiryTime = moment().add(planDetails.durationInDays, "days").startOf('day').valueOf();
                                    }
                                    let purchaseTime = new Date().getTime();

                                    let dataToPush = {
                                        "planId": ObjectID(planDetails._id),
                                        "subscriptionId": "Free Plan",
                                        "purchaseDate": purchaseDate,
                                        "purchaseTime": purchaseTime,
                                        "userPurchaseTime": purchaseTime,
                                        "durationInMonths": planDetails.durationInMonths,
                                        "actualId": planDetails.actualId,
                                        "actualIdForiOS": planDetails.actualIdForiOS,
                                        "actualIdForAndroid": planDetails.actualIdForAndroid,
                                        "expiryTime": expiryTime,
                                        "likeCount": planDetails.likeCount,
                                        "rewindCount": planDetails.rewindCount,
                                        "superLikeCount": planDetails.superLikeCount,
                                        "whoLikeMe": planDetails.whoLikeMe,
                                        "whoSuperLikeMe": planDetails.whoSuperLikeMe,
                                        "recentVisitors": planDetails.recentVisitors,
                                        "readreceipt": planDetails.readreceipt,
                                        "passport": planDetails.passport,
                                        "noAdds": planDetails.noAdds,
                                        "hideDistance": planDetails.hideDistance,
                                        "hideAge": planDetails.hideAge,
                                    };
                                    dataToSend["subscription"] = [dataToPush];
                                    callback(err, result);
                                } else {
                                    callback(err, result);
                                }

                            })
                        }//assign free plan from DB
                    ], function (err, result) {
                        userList_mongoDB.Insert(dataToSend, (err, result) => {
                            if (err) { return reject("---"); }

                            // delete dataToSend.myPreferences;
                            let lat = dataToSend.location.latitude;
                            let lon = dataToSend.location.longitude;
                            // delete dataToSend.location;
                            delete dataToSend.subscription;

                            dataToSend.location = { "lat": lat, "lon": lon };

                            userList_ES.Insert(dataToSend, (err, result) => { });
                            let token = Auth.SignJWT({ _id: _id, key: 'acc' }, 'user', 6000000);
                            dataToSend["token"] = token;
                            dataToSend["_id"] = _id;

                            let cur = new Date();
                            let diff = cur - dataToSend.dob; // This is the difference in milliseconds
                            let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                            dataToSend["age"] = { value: age, isHidden: 0 };
                            dataToSend["distance"] = { value: 0, isHidden: 0 };
                            cronJobTask.sendWellComeMailOnSignUp({ email: dataToSend["email"], firstName: dataToSend["firstName"] })
                            return resolve("--");
                        });
                    });
                }
            });
        });
    }
    function getCoinConfig() {
        return new Promise(function (resolve, reject) {
            coinConfig.Select({}, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke! at getCoinConfig'));

                return resolve({ coinConfig: result[0] });
            });
        });
    }
};

module.exports = { handler, validator }