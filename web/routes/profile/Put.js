'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const fcmPush = require("../../../library/fcm");
var moment = require("moment");
const Timestamp = require('mongodb').Timestamp;
const local = require('../../../locales');
const cronJObFile = require("../../../cronJobTask");
const userListType = require('../../../models/userListType');
const userListCollection = require('../../../models/userList');
const userPostCollectionES = require('../../../models/userPostES');
const userPostCollection = require("../../../models/userPost");
let payloadValidator = Joi.object({
    firstName: Joi.string().description("firstName").error(new Error('firstName is missing')),
    gender: Joi.string().description("gender").error(new Error('gender is missing')),
    height: Joi.number().description("height").error(new Error('height is missing')),
    email: Joi.string().email().description("email").error(new Error('email is missing')),
    dob: Joi.number().description("dob").error(new Error('dob is missing')),
    instaGramProfileId: Joi.string().description("instaGramProfileId").error(new Error('instaGramProfileId is missing')),
    instaGramToken: Joi.string().description("instaGramToken").error(new Error('instaGramToken is missing')),
    dontShowMyAge: Joi.number().min(0).max(1).allow(["", null]).default(null).description("dontShowMyAge").error(new Error('dontShowMyAge is missing')),
    dontShowMyDist: Joi.number().min(0).max(1).allow(["", null]).default(null).description("dontShowMyDist").error(new Error('dontShowMyDist is missing')),
    about: Joi.string().allow("").description("about").error(new Error('about is missing')),
    eduation: Joi.string().description("eduation").error(new Error('eduation is missing')),
    work: Joi.string().description("work").error(new Error('work is missing')),
    bioData: Joi.string().description("bioData").error(new Error('bioData is missing')),
    profilePic: Joi.string()
        .description("photo Link, Eg. https://res.cloudinary.com/closebrace/image/upload/w_400/v1491315007/usericon_id76rb.png")
        .error(new Error('profilePic is missing')),
    profileVideo: Joi.string().allow("").description("profileVideo").error(new Error('profileVideo is missing')),
    profileVideoThumbnail: Joi.string().allow("").description("profileVideoThumbnail").error(new Error('profileVideoThumbnail is missing')),
    otherImages: Joi.array().description("otherImages").error(new Error('otherImages is missing')),
    myPreferences: Joi.array().description(`myPreferences you need to enter like this ,e.g.[{"pref_id":"5a30ff2627322defa4a146a8","selectedValues":["Hindu1"]}]`).error(new Error('myPreferences is missing')),
    profileVideoWidth: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('profileVideoWidth is missing')),
    profileVideoHeight: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('profileVideoWidth is missing')),

}).unknown(true);

/**
 * @function PATCH profile
 * @description This API is used to PATCH Profile.

 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} firstName - firstName
 * @property {number} dob - Date of Birth(miliseconds),Eg. 1504867670000
 * @property {string} imageLink - imageLink
 * @property {string} videoLink - videoLink
  
 * @returns  200 : Profile updated successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Entered User Id(token) doesnot exist,please check the entered token. 
 * @returns  500 : An unknown error has occurred.
    send type if profilePic update type:1
    profileVideo Update type :2
    other photo update:3
    else on other bio updates send type :4
 **/
let APIHandler = (req, res) => {
    console.log("payload---------------------", JSON.stringify(req.payload),typeof(req.payload.type));
    // return res({ message: req.i18n.__('PutProfile')['200'] }).code(200);
    let _id = req.auth.credentials._id;
    let heightInCMObj = cronJObFile.heightInCMObj;
    let heightInFeetObj = cronJObFile.heightInFeetObj;
    let deviceType;
    let dataToUpdate = {
        // "location": {
        //     "longitude": req.payload.longitude || 0,
        //     "latitude": req.payload.latitude || 0
        // }
    };

    if (req.payload.firstName) dataToUpdate["firstName"] = req.payload.firstName;
    if (req.payload.gender) dataToUpdate["gender"] = req.payload.gender;
    // if (req.payload.height) dataToUpdate["height"] = req.payload.height;
    if (req.payload.email) dataToUpdate["email"] = req.payload.email;
    if (req.payload.dob) dataToUpdate["dob"] = req.payload.dob;
    if (req.payload.instaGramProfileId) dataToUpdate["instaGramProfileId"] = req.payload.instaGramProfileId;
    if (req.payload.instaGramToken) dataToUpdate["instaGramToken"] = req.payload.instaGramToken;
    if (req.payload.eduation) dataToUpdate["eduation"] = req.payload.eduation;
    if (req.payload.work) dataToUpdate["work"] = req.payload.work;
    if (req.payload.bioData) dataToUpdate["bioData"] = req.payload.bioData;
    if (req.payload.profilePic) dataToUpdate["profilePic"] = req.payload.profilePic;
    if (req.payload.profileVideo || req.payload.profileVideo == "") dataToUpdate["profileVideo"] = req.payload.profileVideo;
    if (req.payload.profileVideoThumbnail || req.payload.profileVideoThumbnail == "") dataToUpdate["profileVideoThumbnail"] = req.payload.profileVideoThumbnail;
    if (req.payload.otherImages) dataToUpdate["otherImages"] = req.payload.otherImages;
    if (req.payload.dontShowMyAge != null) dataToUpdate["dontShowMyAge"] = req.payload.dontShowMyAge;
    if (req.payload.dontShowMyDist != null) dataToUpdate["dontShowMyDist"] = req.payload.dontShowMyDist;
    if (req.payload.profileVideoWidth != null) dataToUpdate["profileVideoWidth"] = req.payload.profileVideoWidth;
    if (req.payload.profileVideoHeight != null) dataToUpdate["profileVideoHeight"] = req.payload.profileVideoHeight;
     dataToUpdate["about"] = req.payload.about;
    //for update myPreferences we use updateMyPreferences() function and set values in datatoupdate[] array.
    let myPrefrancesArray = [];

    updateMyPreferences()
        .then(value => {
            return updateInMongo();
        }).then(value => {
            return updateInElasticSearch();
            // }).then(value => {
            //     return createPostForBioUpdate()
        }).then(value => {
            return res({ message: req.i18n.__('PutProfile')['200'] }).code(200);
        }).catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('PutProfile')['412'] }).code(412);
        });

    function updateMyPreferences() {
        return new Promise(function (resolve, reject) {
            if (!req.payload.myPreferences) return resolve(true);

            let myPreferencesObj = {};
            userListCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (err) reject(err);

                let pref_id = "";
                for (let index = 0; index < result.myPreferences.length; index++) {
                    pref_id = result.myPreferences[index]["pref_id"];
                    myPreferencesObj[pref_id] = result.myPreferences[index];
                }
                for (let index = 0; index < req.payload.myPreferences.length; index++) {
                    pref_id = req.payload.myPreferences[index]["pref_id"];
                    if (myPreferencesObj[pref_id]) {
                        myPreferencesObj[pref_id] = {
                            "pref_id": ObjectID(req.payload.myPreferences[index]["pref_id"]),
                            "selectedValues": req.payload.myPreferences[index]["selectedValues"] || [],
                            "isDone": true
                        }
                        if (pref_id == "5a30fa6d27322defa4a14550") {
                            let values = req.payload.myPreferences[index]["selectedValues"][0];
                            dataToUpdate["height"] = heightInCMObj[values] || 160;
                            dataToUpdate["heightInFeet"] = heightInFeetObj[values] || `5'3"`;
                        }
                    }
                }
                for (let key in myPreferencesObj) {
                    myPrefrancesArray.push(myPreferencesObj[key]);
                }

                dataToUpdate["myPreferences"] = myPrefrancesArray;
                return resolve(true);

            });

        });
    }
    function updateInMongo() {
        return new Promise(function (resolve, reject) {

            userListCollection.UpdateById(_id, dataToUpdate, (err, result) => {
                if (parseInt(req.payload.type) == 1 || parseInt(req.payload.type) == 2 || parseInt(req.payload.type) == 3 || parseInt(req.payload.type) == 4) {
                    createPostForBioUpdate()
                }
                return (err) ? reject(new Error('Ooops, something broke! 101')) : resolve("--");
                
            });
        });
    }
    function updateInElasticSearch() {
        return new Promise(function (resolve, reject) {

            // let lat = dataToUpdate.location.latitude;
            // let lon = dataToUpdate.location.longitude;
            // delete dataToUpdate.location;
            
            // dataToUpdate.location = { "lat": lat, "lon": lon };
            userListType.Update(_id, dataToUpdate, (err, result) => {
                return (err) ? reject(new Error('Ooops, something broke! 102')) : resolve("--");

            });
        });
    }

    /**
     * this function is use to create a post when user update profile detail
     */
    function createPostForBioUpdate() {

        // console.log("======================>",dataToUpdate)
        return new Promise(function (resolve, reject) {
            //let typeFlag = (req.payload.profileVideo) ? 6 : (req.payload.profilePic) ? 2 : 1;
            let typeFlag = parseInt(req.payload.type)
            var otherImages = req.payload.otherImages || [];
            otherImages.push(req.user.profilePic);
            //let Type = (dataToUpdate.profileVideo) ? "profileVideoUpdate" : (dataToUpdate.profilePic ) ? "profilePhotoUpdate" :(dataToUpdate.otherImages ) ? "profilePhotoUpdate": "bioUpdate";
            let About= (parseInt(req.payload.type) == 4)?req.payload.about: "bio Update"
            let Type = (parseInt(req.payload.type) == 2) ? "profile Video Update" : (parseInt(req.payload.type) == 1) ? "Profile Photo Update" : (parseInt(req.payload.type) == 3) ? "Profile Update" :About;

            let userId = req.auth.credentials._id;
            let pic = (parseInt(req.payload.type) == 1)?[req.payload.profilePic]:[req.user.profilePic]
            let url = (parseInt(req.payload.type) == 2) ? [req.payload.profileVideo] : (parseInt(req.payload.type) == 3) ? req.payload.otherImages :pic;
            let _ID = new ObjectID();
            var data;


            data = {
                _id: _ID,
                userId: ObjectID(userId),
                targetId: ObjectID(userId),
                userName: (req.payload.firstName) ? req.payload.firstName : req.user.firstName,
                type: Type,
                profilePic: (req.payload.profilePic) ? req.payload.profilePic : req.user.profilePic,
                typeFlag: typeFlag,
                postedOn: moment().valueOf(),
                createdOn: new Timestamp(),
                date: new Date(),
                description: Type,
                url: url,
                likeCount: 0,
                commentCount: 0,
                Likers: [],
                commenters: [],
                longitude: req.user.location.longitude || 0,
                latitude: req.user.location.latitude || 0,
            }
            if (req.payload.type == 1) {
                updateInUserPost()
            }
            userPostCollection.Insert(data, (err, result) => {
                logger.silly(err)
                if (result) {
                    userPostCollectionES.Insert(data, (e, r) => {
                        logger.error(e);
                        sendPush()
                    })
                    return resolve("--done--");
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                }
            });

        });

    }

    /**
            * this function is use to  to send push to all matches when user update  profile 
            */
    function sendPush() {


        var condition = { _id: { "$in": req.user.matchedWith } }
        userListCollection.Select(condition, (err, result) => {
            logger.error(err)
            if (result && result.length > 0) {

                // console.log("resulttttt", result)
                deviceType = result.deviceType || "1";
                result.forEach(element => {
                    if (element._id != req.user._id) {
                        let request = {
                            data: {
                                type: "32", deviceType: deviceType, title: process.env.APPNAME, message: "Your match " + req.user.firstName + " has an updated profile"
                            },
                            notification: { "title": process.env.APPNAME, "body": "Your match " + req.user.firstName + " has an updated profile" }
                        };
                        fcmPush.sendPushToTopic(`/topics/${element._id}`, request, () => { })
                    }


                })


            } else {
                logger.error("fcm error : - ", err)
            }
        });




    }

    function updateInUserPost() {

        var condition = { "userId": ObjectID(_id) }
        var data = { profilePic: req.payload.profilePic }
        userPostCollectionES.UpdateProfilePic(_id, data, (e, r) => {
            logger.silly(r);
            logger.error(e);
        })
        userPostCollection.Update(condition, data, (err, result) => {
            logger.error(err);
            if (result) {
                return true

            } else {
                logger.error("insert error ", err)
            }
        });




    }
};

let response = {
    status: {
        200: { message: local['PutProfile']['200'] },
        412: { message: local['PutProfile']['412'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] },
    }
}

module.exports = { APIHandler, payloadValidator, response }