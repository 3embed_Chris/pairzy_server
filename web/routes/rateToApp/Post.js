const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const rateToApp = require('../../../models/rateToApp');

let validator = Joi.object({
    rate: Joi.number().required().description("rate : 1 Bad || 2 Good || 3 Great ").error(new Error('rate is missing')),
    message: Joi.string().allow("").description("message").error(new Error('message is missing'))
});

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let rate = req.payload.rate;
    let message = req.payload.message || "";

    let dataToInsert = {
        userId: ObjectID(_id),
        rate: rate,
        message: message,
        creation: new Date().getTime()
    }

    rateToApp.Insert(dataToInsert, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

        return res({ message: req.i18n.__('PostRateToApp')['200'] }).code(200);
    });
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PostRateToApp']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { APIHandler, validator,response }