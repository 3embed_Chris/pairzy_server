
let headerValidator = require('../../middleware/validator');
let postLikeAPI = require('./Post');

module.exports = [

    {
        method: 'POST',
        path: '/rateToApp',
        handler: postLikeAPI.APIHandler,
        config: {
            description: 'This API is used to like user',
            tags: ['api', 'app'],
            auth: 'userJWT',
            response: postLikeAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postLikeAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];