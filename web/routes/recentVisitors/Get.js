'use strict'
const logger = require('winston');
const userListCollection = require('../../../models/userList');
const ObjectID = require('mongodb').ObjectID;
const Joi = require("joi");
const local = require('../../../locales');
/**
 * @method GET recentVisitiors
 * @description This API use to get recentVisitiors list.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 
 * @returns  200 : list sent successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : list is empty. 
 * @returns  500 : An unknown error has occurred.
 * 
 * @example {
  "message": "list sent successfully.",
  "data": [
    {
      "opponentId": "5a1552f37cbf61169c7a34ac",
      "firstName": "Dipen",
      "countryCode": "+91",
      "mobileNumber": "+9191019181902",
      "gender": "Male",
      "profilePic": ".............",
      "otherImages": [
        "........................",
        "................"
      ],
      "emailId": "example@domain.com",
      "profileVideo": ".................",
      "otherVideos": [
        "........................",
        "................"
      ],
      "dateOfBirth": 1504867670000,
      "about": "Hey I am using sync 1 to 1.",
      "height": 190,
      "heightInFeet": "5'6\""
    }
  ]
}
 */
let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let limit = parseInt(req.query.limit) || 20;
    let offset = parseInt(req.query.offset) || 0;


    let condition = [
        {
            "$geoNear": {
                "near": {
                    "longitude": req.user.location.longitude,
                    "latitude": req.user.location.latitude
                },
                "distanceField": "dist",
                "num": 10000000,
                "spherical": true
            }
        },
        { "$match": { "_id": ObjectID(_id) } },
        { "$unwind": "$recentVisitors" },
        { "$lookup": { "from": "userList", "localField": "recentVisitors", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        { "$match": { "Data.isDeactive": { "$ne": true }, "Data.deleteStatus": { "$ne": 1 } } },
        { "$project": { "Data": 1, "_id": 0, "dist": 1 } },
        {
            "$project": {
                "opponentId": "$Data._id",
                "firstName": "$Data.firstName",
                "countryCode": "$Data.countryCode",
                "mobileNumber": "$Data.contactNumber",
                "gender": "$Data.gender",
                "profilePic": "$Data.profilePic",
                "otherImages": "$Data.otherImages",
                "emailId": "$Data.email",
                "profileVideo": "$Data.profileVideo",
                "otherVideos": "$Data.otherVideos",
                "dateOfBirth": "$Data.dob",
                "about": "$Data.about",
                "height": "$Data.height",
                "heightInFeet": "$Data.heightInFeet",
                "matchedWith": "$Data.matchedWith",
                "onlineStatus": "$Data.onlineStatus",
                "myPreferences": "$Data.myPreferences",
                "dontShowMyAge": "$Data.dontShowMyAge",
                "dontShowMyDist": "$Data.dontShowMyDist",
                "location": "$Data.location",
                "dist": 1

            }
        },
        {
            "$group": {
                "_id": "$opponentId",
                "matchedWith": { "$first": "$matchedWith" },
                "opponentId": { "$first": "$opponentId" },
                "firstName": { "$first": "$firstName" },
                "countryCode": { "$first": "$countryCode" },
                "mobileNumber": { "$first": "$mobileNumber" },
                "gender": { "$first": "$gender" },
                "profilePic": { "$first": "$profilePic" },
                "otherImages": { "$first": "$otherImages" },
                "emailId": { "$first": "$emailId" },
                "profileVideo": { "$first": "$profileVideo" },
                "otherVideos": { "$first": "$otherVideos" },
                "dateOfBirth": { "$first": "$dateOfBirth" },
                "about": { "$first": "$about" },
                "height": { "$first": "$height" },
                "heightInFeet": { "$first": "$heightInFeet" },
                "location": { "$first": "$location" },
                "onlineStatus": { "$first": "$onlineStatus" },
                "myPreferences": { "$first": "$myPreferences" },
                "dist": { "$first": "$dist" },
                "dontShowMyAge": { "$first": "$dontShowMyAge" },
                "dontShowMyDist": { "$first": "$dontShowMyDist" },
            }
        },
        { "$skip": offset },
        { "$limit": limit }
    ]
    userListCollection.matchList(condition, (err, result) => {
        if (result && result.length) {

            for (let index = 0; index < result.length; index++) {

                result[index]["isMatched"] = result[index]["matchedWith"].includes(_id)
                result[index]["otherImages"] = (result[index]["otherImages"]) ? result[index]["otherImages"] : [];
                result[index]["profileVideo"] = (result[index]["profileVideo"]) ? result[index]["about"] : "";
                result[index]["otherVideos"] = (result[index]["otherVideos"]) ? result[index]["about"] : "";
                result[index]["about"] = (result[index]["about"]) ? result[index]["about"] : "";
                result[index]["work"] = "";
                result[index]["job"] = "";
                result[index]["education"] = "";

                if (result[index]["myPreferences"]) {
                    for (let pref_index = 0; pref_index < result[index]["myPreferences"].length; pref_index++) {
                        if (result[index]["myPreferences"][pref_index]["pref_id"] == "5a30fda027322defa4a14638"
                            && result[index]["myPreferences"][pref_index]["isDone"]) {
                            result[index]["work"] = result[index]["myPreferences"][pref_index]["selectedValues"][0];
                        }
                        if (result[index]["myPreferences"][pref_index]["pref_id"] == "5a30fdd127322defa4a14649"
                            && result[index]["myPreferences"][pref_index]["isDone"]) {
                            result[index]["job"] = result[index]["myPreferences"][pref_index]["selectedValues"][0];
                        }
                        if (result[index]["myPreferences"][pref_index]["pref_id"] == "5a30fdfa27322defa4a14653"
                            && result[index]["myPreferences"][pref_index]["isDone"]) {
                            result[index]["education"] = result[index]["myPreferences"][pref_index]["selectedValues"][0];
                        }
                    }
                }


                let cur = new Date();
                let diff = cur - result[index].dateOfBirth; // This is the difference in milliseconds
                let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25

                result[index]["age"] = { value: age, isHidden: result[index].dontShowMyAge || 0 };
                result[index]["distance"] = { value: result[index].dist, isHidden: result[index].dontShowMyDist || 0 };

                delete result[index]["myPreferences"];
                delete result[index]["matchedWith"];
                delete result[index]["dist"];
                delete result[index]["dontShowMyAge"];
                delete result[index]["dontShowMyDist"];
                delete result[index]["location"];

            }
            return res({ message: req.i18n.__('GetRecentVisitors')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetRecentVisitors')['412'] }).code(412);
        }

    })
};

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

let response = {
    status: {
        200: {
            message: Joi.any().default(local['GetRecentVisitors']['200']), data: Joi.any().example(
                [
                    {
                        "opponentId": "5a1552f37cbf61169c7a34ac",
                        "firstName": "Dipen",
                        "countryCode": "+91",
                        "mobileNumber": "+9191019181902",
                        "gender": "Male",
                        "profilePic": ".............",
                        "otherImages": ["........................", "................"],
                        "emailId": "example@domain.com",
                        "profileVideo": ".................",
                        "otherVideos": ["........................", "................"],
                        "dateOfBirth": 1504867670000,
                        "about": "Hey I am using sync 1 to 1.",
                        "height": 190,
                        "heightInFeet": "5'6\""
                    }
                ]
            )
        },
        412: { message: Joi.any().default(local['GetRecentVisitors']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}

module.exports = { handler, response, queryValidator }