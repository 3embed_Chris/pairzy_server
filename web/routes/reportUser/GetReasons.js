'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const local = require('../../../locales');
const reportReasons = require("../../../models/reportReasons");

/**
 * @method GET reportUserReasons
 * @description This API use to get reportUserReasons.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 
 * @returns  200 : Reasons to report user sent successfully.
 * @example {
  "message": "Reasons to report user sent successfully.",
  "data": {
    "data": [
      "Duplicate user"
    ]
  }
}
 * @returns  412 : Reasons to report user doesnot exist.. 
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    let dataTosend = [];

    reportReasons.Select({}, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        if (!result.length) return res({ message: req.i18n.__('GetReportUserReasons')['412'] }).code(412);

        for (let index = 0; index < result.length; index++) {
            dataTosend.push(result[index]["reportReason"]);
        }
        return res({ message: req.i18n.__('GetReportUserReasons')['200'], data: dataTosend }).code(200);
    });
};

let response = {
    status: {
        200: {
            message: Joi.any().default(local['GetReportUserReasons']['200']), data: Joi.any().example({
                "data": [
                    "Duplicate user"
                ]
            })
        },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        412: { message: Joi.any().default(local['genericErrMsg']['412']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { handler, response }