'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const userReportsCollection = require("../../../models/userReports");
const userListCollection = require("../../../models/userList");

/**
 * @method POST reportUser
 * @description This API use to POST reportUser.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 * @property {*} reason reason
 * @property {*} message message
 
 * @returns  200 : User reported successfully..
 * @returns 400 : targetUser doesnot exist, try with a different targetUserId.
 * @returns  412 : Reasons to report user doesnot exist.
 * @returns 422 : User cannot report his own targetUserId,please use a different targetUserId.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let targetUserId = req.payload.targetUserId;
    let reason = req.payload.reason;
    let message = req.payload.messsage;

    if (_id === targetUserId) {
        return res({ message: req.i18n.__('PostReportUser')['422'] }).code(422);
    }


    userListCollection.SelectById({ _id: targetUserId }, { _id: 1 }, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);


        if (!result || !result["_id"] || result["_id"] == null) { return res({ message: req.i18n.__('PostReportUser')['412'] }).code(412); }

        let dataToInsert = {
            "userId": ObjectID(_id),
            "targetUserId": ObjectID(req.payload.targetUserId),
            "reason": reason,
            "messsage": message,
            "creation": new Date().getTime()
        };

        userReportsCollection.Insert(dataToInsert, (err, result) => {
            if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

            return res({ message: req.i18n.__('PostReportUser')['200'] }).code(200);
        });
    });
};
let response = {
    status: {
        200: { message: Joi.any().default(local['PostReportUser']['200']) },
        412: { message: Joi.any().default(local['PostReportUser']['412']) },
        422: { message: Joi.any().default(local['PostReportUser']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
let validator = Joi.object({
    targetUserId: Joi.string().required().max(24).min(24).description("targetUserId").example("5a096e680941c626e794bc5c").error(new Error('targetUserId is missing || incorrect targetUserId length must be 24 Character')),
    reason: Joi.string().required().description("reason").example("Duplicate User").error(new Error('reason is missing ')),
    message: Joi.string().required().description("message").example("message").error(new Error('message is missing'))
}).required();

module.exports = { handler, response, validator }