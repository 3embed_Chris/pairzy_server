'use strict'
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const local = require('../../../locales');
const userListCollection = require('../../../models/userList');

let payloadValidator = Joi.object({
    pref_id: Joi.string().required().error(new Error('pref_id is missing')),
    selectedValue: Joi.array().required().error(new Error('selectedValue is missing')),
    type: Joi.string().required().description("1:favoritePreferences,2:myPreferences").example("1").error(new Error('type is missing')),
    
}).required();

/**
 * @method PATCH  preference
 * @description This API used to update particuler one preferences value.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} pref_id pref_id
 * @property {*} selectedValue selectedValue
 * @property {*} type type : 1:favoritePreferences,2:myPreferences
 
 * @returns  200 : User preferences updated successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */
let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let pref_id = ObjectID(req.payload.pref_id);
    let selectedValue = req.payload.selectedValue;

    updatePreferences(_id)
        .then(function (value) {
            return res({ message: req.i18n.__('PatchSearchPrefrance')['200'] }).code(200);
        })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('PatchSearchPrefrance')['412'] }).code(412);
        });


    function updatePreferences(_id) {
        return new Promise(function (resolve, reject) {

            let preferencesType = "searchPreferences";
            let updateData = [preferencesType] + ".$.selectedValue";

            userListCollection.Update(
                { _id: ObjectID(_id), [preferencesType]: { "$elemMatch": { "pref_id": pref_id } } },
                { [updateData]: selectedValue }, (err, result) => {
                    if (err) {
                        reject(new Error('Ooops, something broke! at updateVideoLink_MONGO'));
                    } else {
                        resolve("update successfully. at updateVideoLink_MONGO");
                    }
                });

        });
    }

};

let APIResponse = {
    status: {
        200: { message: Joi.any().default(local['PatchSearchPrefrance']['200']) },
        412: { message: Joi.any().default(local['PatchSearchPrefrance']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}
module.exports = { payloadValidator, APIHandler, APIResponse }