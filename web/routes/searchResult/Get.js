'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const moment = require("moment");
const cronJobTask = require('../../../cronJobTask');
const userListType = require('../../../models/userListType');
const userList = require('../../../models/userList');
const redisClient = require('../../../models/redisDB').client;
const searchPreferenceCollection = require('../../../models/searchPreferences');
const myLibrary = require("../../../library/myLibrary");
const ObjectID = require("mongodb").ObjectID

/**
 * @method GET searchResult
 * @description This API use to get searchResult.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header

 */
let APIHandler = (req, res) => {

    var _id = req.auth.credentials._id;
    const likeRefreshInMillisecond = cronJobTask.likeRefreshInMillisecond;
    let limit = parseInt(req.query.limit) || 20;
    let skip = parseInt(req.query.offset) || 0;
    let remainsLikesInString = "0", remainsRewindsInString = "0";

    let favoritePreferences = [], dataToSend = [], matcheResult = [];
    let gender = "Male,Female", heightMin = 100, heightMax = 300, dobMin = 1504867670000, dobMax = 1804867670000, distanceMin = 1,
        distanceMax = 500, latitude = 77.589864, longitude = 13.028712, contactNumber = "123", fbId = "123", must_not = [];
    let myLikes = [], matches = [], myDisLikes = [], myBlock = [], mySupperLike = []; let DATA = []
    let resultOfPref
    let values = {}
    var UserData = {}, objResult = {};
    let Ids = []
    let total;

    try {

        async.series([
            function (callback) {

                userList.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                    (err) ? logger.error(err) :
                        console.log("result000000000000000000000", JSON.stringify(result.searchPreferences))

                    UserData = result
                    if (req.user) {
                        favoritePreferences = UserData.searchPreferences;
                        //mysearchPreference = UserData.searchPreferences
                        latitude = req.user.location.latitude || 77;
                        longitude = req.user.location.longitude || 13;
                        contactNumber = req.user.contactNumber || req.user.fbId;
                        fbId = req.user.fbId;
                        myLikes = req.user.myLikes || [];
                        mySupperLike = req.user.mySupperLike || [];
                        matches = req.user.matchedWith || [];
                        myDisLikes = req.user.myunlikes || [];
                        // myBlock = req.user.myBlock || [];
                    } else {
                        return res({ message: req.i18n.__('genericErrMsg')['401'] }).code(401);
                    }
                    return callback(null, true);
                })
            },
            function (callback) {
                searchPreferenceCollection.Select({}, (err, result) => {
                    (err) ? logger.error(err) :
                        //console.log("result", JSON.stringify(result))
                        resultOfPref = result
                    callback(null, true)
                })
            },
            function (callback) {
                if (favoritePreferences) {

                    for (let index = 0; index < favoritePreferences.length; index++) {
                        //console.log("=================33333333333333333333============",favoritePreferences[index],index)

                        if (favoritePreferences[index].isDone && favoritePreferences[index].type != 4 &&
                            favoritePreferences[index].type != 3 && favoritePreferences[index].type != 5) {
                            console.log("=================33333333333333333333============", favoritePreferences[index], index)

                            DATA.push(
                                {
                                    "nested": {
                                        "path": "string_facets",
                                        "query": {
                                            "bool": {
                                                "must": [
                                                    {
                                                        "term": {
                                                            "string_facets.facet-id": "5d63857eb80db667866938f5"
                                                        }
                                                    },
                                                    {
                                                        "terms": {
                                                            "string_facets.facet-value": [
                                                                "heyasdsadsa", "abcd"
                                                            ]
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                }
                            )
                        }

                        resultOfPref.find(e => {
                            if (favoritePreferences[index].isDone && e._id.toString() == favoritePreferences[index].pref_id.toString()) {
                                values = favoritePreferences[index].selectedValues[0]
                                return true
                            }
                        })
                        if (favoritePreferences[index].pref_id == "5d6384b9b80db667866938f3") {
                            gender = favoritePreferences[index].selectedValues[0].toString();

                        } else if (favoritePreferences[index].pref_id == "5d638500b80db667866938f4") {
                            heightMin = (favoritePreferences[index].selectedValues[0] < 100) ? 100 : favoritePreferences[index].selectedValues[0];
                            heightMax = (favoritePreferences[index].selectedValues[1] < 100) ? 300 : favoritePreferences[index].selectedValues[1];
                        } else if (favoritePreferences[index].pref_id == "5d638428b80db667866938f1") {
                            dobMin = favoritePreferences[index].selectedValues[0];
                            dobMax = favoritePreferences[index].selectedValues[1];

                            let MaxTimeStamp = moment().add(-dobMin, "y").valueOf();
                            let MinTimeStamp = moment().add(-(dobMax + 1), "y").valueOf() - 1;

                            dobMin = (favoritePreferences[index].selectedValues[1] == 55) ? -2179594491000 : MinTimeStamp;
                            dobMax = MaxTimeStamp
                        } else if (favoritePreferences[index].pref_id == "5d63845ab80db667866938f2") {
                            distanceMin = (favoritePreferences[index].selectedValues[0] || 0) + "km";
                            distanceMax = (favoritePreferences[index].selectedValues[1] || 100) + "km";
                        }
                    }
                }
                return callback(null, 1);
            },
            function (callback) {
                redisClient.hgetall(_id, (err, data) => {
                    logger.error("errerrerrerrerrerrerrerr", err)
                    for (let _id in data) {
                        must_not.push({ "match": { "_id": _id } });
                    }
                    logger.silly("must_not redis ", must_not.length)

                    callback(null, true)
                });
                must_not.push({
                    "match": {
                        "profileStatus": 1
                    }
                },
                    {
                        "match": {
                            "_id": _id
                        }
                    },
                    {
                        "match": {
                            "deleteStatus": 1
                        }
                    },
                    {
                        "terms": {
                            "_id": {
                                "index": process.env.ELASTICK_INDEX,
                                "type": "userList",
                                "id": _id,
                                "path": "myLikes"
                            }
                        }
                    },
                    {
                        "terms": {
                            "_id": {
                                "index": process.env.ELASTICK_INDEX,
                                "type": "userList",
                                "id": _id,
                                "path": "matchedWith"
                            }
                        }
                    },
                    {
                        "terms": {
                            "_id": {
                                "index": process.env.ELASTICK_INDEX,
                                "type": "userList",
                                "id": _id,
                                "path": "myunlikes"
                            }
                        }
                    },
                    {
                        "terms": {
                            "_id": {
                                "index": process.env.ELASTICK_INDEX,
                                "type": "userList",
                                "id": _id,
                                "path": "myBlock"
                            }
                        }
                    },
                    {
                        "terms": {
                            "_id": {
                                "index": process.env.ELASTICK_INDEX,
                                "type": "userList",
                                "id": _id,
                                "path": "mySupperLike"
                            }
                        }
                    });
                if (req.user.myLikes && req.user.myLikes.length) {
                    req.user.myLikes.slice(Math.max(req.user.myLikes.length - 20, 0)).map(e => {
                        must_not.push({ "match": { "_id": e } });
                    })
                }
                if (req.user.myunlikes && req.user.myunlikes.length) {
                    req.user.myunlikes.slice(Math.max(req.user.myunlikes.length - 20, 0)).map(e => {
                        must_not.push({ "match": { "_id": e } });
                    })
                }
                if (req.user.mySupperLike && req.user.mySupperLike.length) {
                    req.user.mySupperLike.slice(Math.max(req.user.mySupperLike.length - 20, 0)).map(e => {
                        must_not.push({ "match": { "_id": e } });
                    })
                }
                if (req.user.matchedWith && req.user.matchedWith.length) {
                    req.user.matchedWith.slice(Math.max(req.user.matchedWith.length - 20, 0)).map(e => {
                        must_not.push({ "match": { "_id": e } });
                    })
                }
            },
            function (callback) {
                // must_not.push({ "match": { "contactNumber": contactNumber || "12345" } });
                // must_not.push({ "match": { "fbId": fbId || "-11111" } });
                // must_not.push({ "match": { "profileStatus": 1 } });
                // must_not.push({ "match": { "deleteStatus": 1 } });
                // if (myLikes) myLikes.map(id => { must_not.push({ "match": { "_id": id } }) })
                //if (matches) matches.map(id => { must_not.push({ "match": { "_id": id } }) })
                // if (myDisLikes) myDisLikes.map(id => { must_not.push({ "match": { "_id": id } }) })
                // if (myBlock) myBlock.map(id => { must_not.push({ "match": { "_id": id } }) })
                // if (mySupperLike) mySupperLike.map(id => { must_not.push({ "match": { "_id": id } }) })
                return callback(null, true);
            },
            function (callback) {
                userListType.findMatch({
                    gender: gender, heightMin: heightMin, heightMax: heightMax, dobMin: dobMin,
                    dobMax: dobMax, longitude: longitude, latitude: latitude, distanceMin: distanceMin,
                    distanceMax: distanceMax, contactNumber: contactNumber, must_not: must_not,
                    skip: skip, limit: limit, _id: _id, prefData: DATA
                }, (err, result) => {
                    //logger.error("matcheResult ",JSON.stringify(err) );
                    // console.log("errrrrrrrrrrrrrrrrrrrrrrrr", JSON.stringify(err))
                    matcheResult = (result.hits) ? result.hits.hits : [];
                    // logger.silly("matcheResult total ", JSON.stringify(matcheResult))
                    logger.silly("matcheResult total ", matcheResult.length)
                    callback(err, result);
                });
            },
            function (callback) {

                for (let index = 0; index < matcheResult.length; index++) {
                    let birthdate = new Date(matcheResult[index]["_source"]["dob"]);
                    let cur = new Date();
                    let diff = cur - birthdate; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                    let work = "", job = "", education = "";
                    Ids.push({
                        "bool": {
                            "must": [
                                {
                                    "match": {
                                        "user1": _id
                                    }
                                },
                                {
                                    "match": {
                                        "user2": matcheResult[index]["_id"]
                                    }
                                }
                            ]
                        }
                    },
                        {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "user1": matcheResult[index]["_id"]
                                        }
                                    },
                                    {
                                        "match": {
                                            "user2": _id
                                        }
                                    }
                                ]
                            }
                        })

                    // if (matcheResult[index]["_source"]["myPreferences"]) {
                    //     for (let pref_index = 0; pref_index < matcheResult[index]["_source"]["myPreferences"].length; pref_index++) {
                    //         if (matcheResult[index]["_source"]["myPreferences"][pref_index]["pref_id"] == "5a30fda027322defa4a14638"
                    //             && matcheResult[index]["_source"]["myPreferences"][pref_index]["isDone"]) {
                    //             work = matcheResult[index]["_source"]["myPreferences"][pref_index]["selectedValues"][0];
                    //         }
                    //         if (matcheResult[index]["_source"]["myPreferences"][pref_index]["pref_id"] == "5a30fdd127322defa4a14649"
                    //             && matcheResult[index]["_source"]["myPreferences"][pref_index]["isDone"]) {
                    //             job = matcheResult[index]["_source"]["myPreferences"][pref_index]["selectedValues"][0];
                    //         }
                    //         if (matcheResult[index]["_source"]["myPreferences"][pref_index]["pref_id"] == "5a30fdfa27322defa4a14653"
                    //             && matcheResult[index]["_source"]["myPreferences"][pref_index]["isDone"]) {
                    //             education = matcheResult[index]["_source"]["myPreferences"][pref_index]["selectedValues"][0];
                    //         }
                    //     }
                    // }
                    /*
                    dataToSend.push({
                        "opponentId": matcheResult[index]["_id"],
                        "firstName": matcheResult[index]["_source"]["firstName"],
                        "mobileNumber": matcheResult[index]["_source"]["contactNumber"],
                        "emailId": matcheResult[index]["_source"]["email"],
                        "height": matcheResult[index]["_source"]["height"],
                        "gender": matcheResult[index]["_source"]["gender"],
                        "dateOfBirth": matcheResult[index]["_source"]["dob"],
                        "onlineStatus": matcheResult[index]["_source"]["onlineStatus"],
                        "about": matcheResult[index]["_source"]["about"],
                        "profilePic": matcheResult[index]["_source"]["profilePic"] || "https://res.cloudinary.com/demo/image/upload/ar_4:3,c_fill/c_scale,w_auto,dpr_auto/sample.jpg",
                        "profileVideo": matcheResult[index]["_source"]["profileVideo"],
                        "otherImages": (Array.isArray(matcheResult[index]["_source"]["otherImages"])) ? matcheResult[index]["_source"]["otherImages"] : [],
                        "firebaseTopic": matcheResult[index]["_source"]["firebaseTopic"],
                        "instaGramProfileId": matcheResult[index]["_source"]["instaGramProfileId"] || "",
                        "instaGramToken": matcheResult[index]["_source"]["instaGramToken"] || "",
                        "deepLink": matcheResult[index]["_source"]["deepLink"],
                        "profileVideoThumbnail": (matcheResult[index]["_source"]["profileVideoThumbnail"] && !Array.isArray(matcheResult[index]["_source"]["profileVideoThumbnail"])) ? matcheResult[index]["_source"]["profileVideoThumbnail"] : "",
                        "distance": { value: parseFloat(matcheResult[index]["sort"][1].toFixed(2)), isHidden: matcheResult[index].dontShowMyDist || 0 },
                        "age": { value: age, isHidden: matcheResult[index].dontShowMyAge || 0 },
                        "myPreferences": matcheResult[index]["_source"]["myPreferences"],
                        "superliked": (req.user && req.user.supperLikeByHistory &&
                            req.user.supperLikeByHistory.map(id => id.toString()).includes(matcheResult[index]["_id"])) ? 1 : 0,
                        "profileVideoWidth": matcheResult[index]["_source"]["profileVideoWidth"],
                        "profileVideoHeight": matcheResult[index]["_source"]["profileVideoHeight"],
                        "isBoostProfile": (matcheResult[index]["_source"]["boostTimeStamp"] && matcheResult[index]["_source"]["boostTimeStamp"] > 0),
                        // "yesVote": numbers.map(function (num) {
                        //     return num * 2;
                        // })
                    }) */

                    objResult[matcheResult[index]["_id"]] = {
                        "opponentId": matcheResult[index]["_id"],
                        "firstName": matcheResult[index]["_source"]["firstName"],
                        "mobileNumber": matcheResult[index]["_source"]["contactNumber"],
                        "emailId": matcheResult[index]["_source"]["email"],
                        "height": matcheResult[index]["_source"]["height"],
                        "gender": matcheResult[index]["_source"]["gender"],
                        "dateOfBirth": matcheResult[index]["_source"]["dob"],
                        "onlineStatus": matcheResult[index]["_source"]["onlineStatus"],
                        "about": matcheResult[index]["_source"]["about"],
                        "profilePic": matcheResult[index]["_source"]["profilePic"] || "https://res.cloudinary.com/demo/image/upload/ar_4:3,c_fill/c_scale,w_auto,dpr_auto/sample.jpg",
                        "profileVideo": matcheResult[index]["_source"]["profileVideo"],
                        "otherImages": (Array.isArray(matcheResult[index]["_source"]["otherImages"])) ? matcheResult[index]["_source"]["otherImages"] : [],
                        "firebaseTopic": matcheResult[index]["_source"]["firebaseTopic"],
                        "instaGramProfileId": matcheResult[index]["_source"]["instaGramProfileId"] || "",
                        "instaGramToken": matcheResult[index]["_source"]["instaGramToken"] || "",
                        "deepLink": matcheResult[index]["_source"]["deepLink"],
                        "profileVideoThumbnail": (matcheResult[index]["_source"]["profileVideoThumbnail"] && !Array.isArray(matcheResult[index]["_source"]["profileVideoThumbnail"])) ? matcheResult[index]["_source"]["profileVideoThumbnail"] : "",
                        "distance": { value: parseFloat(matcheResult[index]["sort"][1].toFixed(2)), isHidden: matcheResult[index].dontShowMyDist || 0 },
                        "age": { value: age, isHidden: matcheResult[index].dontShowMyAge || 0 },
                        "myPreferences": matcheResult[index]["_source"]["myPreferences"],
                        "superliked": (req.user && req.user.supperLikeByHistory &&
                            req.user.supperLikeByHistory.map(id => id.toString()).includes(matcheResult[index]["_id"])) ? 1 : 0,
                        "profileVideoWidth": matcheResult[index]["_source"]["profileVideoWidth"],
                        "profileVideoHeight": matcheResult[index]["_source"]["profileVideoHeight"],
                        "isBoostProfile": (matcheResult[index]["_source"]["boostTimeStamp"] && matcheResult[index]["_source"]["boostTimeStamp"] > 0),
                        "yesVotes": 0,
                        "noVotes": 0
                    }
                }

                callback(null, true);
            },
            function (callback) {
                // console.log("==============>", JSON.stringify(Ids))

                userListType.SelectVoteFromuserpairdetails(Ids, (err, result) => {
                    console.log("res", result, "err", err)
                    result = (result.hits) ? result.hits.hits : [];

                    // "opponentId": matcheResult[index]["_id"],
                    // "firstName": matcheResult[index]["_source"]["firstName"],
                    result.forEach(element => {
                        if (element["_source"].user1 == _id) {
                            total = element["_source"].likes.length + element["_source"].dislikes.length
                            console.log("if", total, element["_source"].likes.length,((element["_source"].likes.length / total) * 100).toFixed(0))
                            objResult[element["_source"].user2]["yesVotes"] = (element["_source"].likes.length == 0) ? 0 : parseInt(((element["_source"].likes.length / total) * 100).toFixed(0))
                            objResult[element["_source"].user2]["noVotes"] = (element["_source"].dislikes.length == 0) ? 0 : parseInt(((element["_source"].dislikes.length / total) * 100).toFixed(0))
                        } else if (element["_source"].user2 == _id) {
                            total = element["_source"].likes.length + element["_source"].dislikes.length

                            console.log("else if", total, element["_source"].likes.length, element["_source"].dislikes.length)

                            objResult[element["_source"].user1]["yesVotes"] = (element["_source"].likes.length == 0) ? 0 : parseInt(((element["_source"].likes.length / total) * 100).toFixed(0))
                            objResult[element["_source"].user1]["noVotes"] = (element["_source"].dislikes.length == 0) ? 0 : parseInt(((element["_source"].dislikes.length / total) * 100).toFixed(0))
                        }
                    });
                    dataToSend = [];
                    for (let id in objResult) {
                        dataToSend.push(objResult[id])
                    }
                    callback(null, true);

                });
            },
        ], (err, result) => {

            if (req.user.lastTimestamp) {
                req.user.lastTimestamp.like = (req.user.lastTimestamp && req.user.lastTimestamp.like) ? req.user.lastTimestamp.like : new Date().getTime();
            } else {
                req.user["lastTimestamp"] = { "like": 0 }
                // req.user["lastTimestamp"]["like"] = 0;
            }

            var a = moment();
            var b = moment(req.user.lastTimestamp.like);
            if (req.user.subscription[0]["likeCount"] != "unlimited" &&
                req.user.subscription &&
                req.user.count.like >= req.user.subscription[0]["likeCount"] &&
                a.diff(b, 'ms') >= likeRefreshInMillisecond) {
                remainsLikesInString = parseInt(req.user.subscription[0]["likeCount"]);
            } else {
                remainsLikesInString = (req.user.subscription[0]["likeCount"] == "unlimited") ? "unlimited" : parseInt(req.user.subscription[0]["likeCount"]) - req.user.count.like;
            }

            b = moment(req.user.lastTimestamp.rewind);
            if (req.user.subscription[0]["rewindCount"] != "unlimited" &&
                req.user.subscription &&
                req.user.count.like >= req.user.subscription[0]["rewindCount"] &&
                a.diff(b, 'ms') >= likeRefreshInMillisecond) {
                remainsRewindsInString = parseInt(req.user.subscription[0]["rewindCount"]);
            } else {
                remainsRewindsInString = (req.user.subscription[0]["rewindCount"] == "unlimited") ? "unlimited" : parseInt(req.user.subscription[0]["rewindCount"]) - req.user.count.rewind;
            }
            b = moment(req.user.lastTimestamp.like);
            let nextLikeTime = (remainsLikesInString == "0" || parseInt(remainsLikesInString) < 0)
                ? (b.add(likeRefreshInMillisecond, 'ms').valueOf())
                : 0;
            let nextRewindTime = (remainsRewindsInString == "0" || parseInt(remainsRewindsInString) < 0)
                ? (b.add(likeRefreshInMillisecond, 'ms').valueOf())
                : 0;

            // console.log("=====>>>>", JSON.stringify(dataToSend))

            return res({
                message: "success",
                data: dataToSend,
                remainsLikesInString: remainsLikesInString,
                remainsRewindsInString: remainsRewindsInString,
                nextLikeTime: nextLikeTime,
                nextRewindTime: nextRewindTime,
                boost: (req.user && req.user.boost) ? req.user.boost : {}
            }).code(200);
        })
    } catch (error) {
        logger.error("error : ", error);
        return res({ message: "success", data: [] }).code(200);
    }
};

let validator = Joi.object({
    offset: Joi.number().default(0).description("0").error(new Error('offset is missing')),
    limit: Joi.number().default(20).description("0").error(new Error('limit is missing'))
}).required({ Unknown: true });

module.exports = { APIHandler, validator }