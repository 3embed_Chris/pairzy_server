'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
const local = require('../../../locales');
const moment = require("moment");
const userPostCollectionES = require('../../../models/userPostES');
const rabbitMq = require('../../../library/rabbitMq');
const fcmPush = require("../../../library/fcm");
const mqttClient = require("../../../library/mqtt");
const userListType = require('../../../models/userListType');
const messagesCollection = require('../../../models/messages');
const userListCollection = require('../../../models/userList');
const chatLikesCollection = require('../../../models/chatList');
const userLikesCollection = require('../../../models/userLikes');
const userMatchCollection = require('../../../models/userMatch');
const userSupperLikeCollection = require("../../../models/userSupperLike");
const userUnlikesCollection = require("../../../models/userUnlikes");
const coinConfigCollection = require('../../../models/coinConfig');
const userBoostCollection = require('../../../models/userBoost');
const coinWalletCollection = require('../../../models/coinWallet');
const walletCustomerCollection = require('../../../models/walletCustomer');
const userPostCollection = require("../../../models/userPost");


let validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId").error(new Error('targetUserId is missing'))
}).required();

/**
 * @method POST like
 * @description This API use to like a user.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId,Eg. 5a14013b7b334c19349aec7b
 */
let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let targetUserId = req.payload.targetUserId;
    let isTargetLikedMe = false;
    let isTargetBoostProfile = false;
    let targetBoostId = "";
    let targetBoostDetails = "";
    let targetPushToken = "";
    let firstLikedByName = "";
    let firstLikedByPhoto = "";
    let myName = req.user.firstName;
    var timestamp = new Date().getTime();
    var trigger = "superLike";
    var data = {};


    if (req.user.matchedWith && req.user.matchedWith.map(id => id.toString()).includes(targetUserId)) {
        return res({ message: req.i18n.__('genericErrMsg')['405'] }).code(405);
    } else if (req.user.myLikes && req.user.myLikes.map(id => id.toString()).includes(targetUserId)) {
        return res({ message: req.i18n.__('genericErrMsg')['405'] }).code(405);
    } else if (req.user.mySupperLike && req.user.mySupperLike.map(id => id.toString()).includes(targetUserId)) {
        return res({ message: req.i18n.__('genericErrMsg')['405'] }).code(405);
    }

    userListType.UpdateWithPull(_id, "myunlikes", targetUserId, (err, result) => { });
    userListCollection.UpdateByIdWithPull({ _id: _id }, { "myunlikes": ObjectID(targetUserId) }, (e, r) => { })



    try {
        let condition = { _id: ObjectID(_id), "$or": [{ "mySupperLike": ObjectID(targetUserId) }, { "matchedWith": ObjectID(targetUserId) }] };
        userListCollection.Select(condition, (err, result) => {
            if (err) {
                logger.error("Error : ", err);
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            } else if (result && result[0]) {
                logger.silly("Error : already liked", JSON.stringify(condition));
                /**Update In elastic Search, in supperLikeByHistory will not remove userid we use this to show star in user's profile */
                userListType.UpdateWithPush(targetUserId, "supperLikeByHistory", _id, (err, result) => {
                    if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                });
                /**Update in MongoDB, in supperLikeByHistory will not remove userid we use this to show star in user's profile */
                userListCollection.UpdateByIdWithAddToSet({ _id: targetUserId }, { "supperLikeByHistory": ObjectID(_id) }, (err, result) => {
                    if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                });
                /**Update in MongoDB */
                userListCollection.UpdateByIdWithAddToSet({ _id: targetUserId }, { "supperLikeBy": ObjectID(_id) }, (err, result) => {
                    if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                });
                userListCollection.UpdateByIdWithAddToSet({ _id: _id }, { "mySupperLike": ObjectID(targetUserId) }, (err, result) => {
                    if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                });
                return res({ message: req.i18n.__('PostSupperLike')['200'] }).code(200);
            } else {
                logger.silly("Error : already not liked");
                condition = {
                    "$or": [
                        { "userId": ObjectID(targetUserId), "targetUserId": ObjectID(_id) },
                        { "userId": ObjectID(_id), "targetUserId": ObjectID(targetUserId) }]
                };
                userUnlikesCollection.Delete(condition, (err, result) => {
                    if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                });
                userListType.UpdateWithPull(_id, "disLikedUSers", targetUserId, (err, result) => {
                    // if (err) {logger.error("disLikedUSers : ",err)}
                });

                getRequiredCoinForTrigger()
                    .then((value) => { return getUsersCoinFromCoinWalletAndValidate(value); })
                    .then(() => { return targetUserExists(); })
                    .then(() => { return isTargetLikedMeFun(); })
                    .then(() => { return processLike(); })
                    .then((value) => { return insertInWalletCusetomerAndUpdateInCoinWallet(value); })
                    .then((value) => {
                        if (isTargetLikedMe) {
                            return res({ message: req.i18n.__('PostSupperLike')['201'], data: value, coinWallet: data.coinWallet }).code(201);
                        } else {
                            return res({ message: req.i18n.__('PostSupperLike')['200'], coinWallet: data.coinWallet }).code(200);
                        }
                    }).catch((err) => {
                        //logger.silly('Caught an error!', err);
                        logger.error(err)
                        return res({ message: err.message }).code(err.code);
                    });
            }
        })

        function getRequiredCoinForTrigger() {
            return new Promise((resolve, reject) => {
                coinConfigCollection.SelectOne({}, (err, result) => {
                    logger.error(err)

                    if (err) return reject(new Error('Ooops, no have data in coinConfig!'));

                    return resolve(result[trigger] || {});
                });
            });
        }
        function getUsersCoinFromCoinWalletAndValidate(requiredCoin) {
            return new Promise((resolve, reject) => {
                coinWalletCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                    logger.error(err)

                    if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });

                    logger.silly("requiredCoin ", requiredCoin)
                    for (let key in requiredCoin) {
                        if (requiredCoin[key] == 0 || (result && result["coins"][key] && requiredCoin[key] <= result["coins"][key])) {
                        } else {
                            return reject({ code: 402, message: req.i18n.__('genericErrMsg')['402'] });
                        }
                    }
                    data["coinWallet"] = result["coins"] || {};
                    data["requiredCoin"] = requiredCoin;
                    return resolve(true);
                });
            });
        }
        function targetUserExists() {
            return new Promise(function (resolve, reject) {
                userListCollection.SelectById({ _id: targetUserId }, { _id: 1, firstName: 1, profilePic: 1, currentLoggedInDevices: 1 }, (err, result) => {
                    if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    logger.error(err)

                    if (result) {
                        firstLikedByName = result.firstName || "";
                        firstLikedByPhoto = result.profilePic || "";
                        return resolve(result);
                    } else {
                        userListType.Delete("userList", { _id: targetUserId }, () => { });
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    }
                });
            });
        }
        function isTargetLikedMeFun() {
            return new Promise(function (resolve, reject) {

                isTargetLikedMe = (req.user.likedBy.map(id => id.toString()).includes(targetUserId))
                isTargetLikedMe = (isTargetLikedMe) ? isTargetLikedMe : req.user.supperLikeBy.map(id => id.toString()).includes(targetUserId);

                userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
                    if (result && result.boost && result.boost.expire >= new Date().getTime()) {
                        isTargetBoostProfile = true;
                        targetBoostId = result.boost._id;
                        targetBoostDetails = result.boost;
                    }
                    return resolve(true);
                });
            });
        }
        function processLike() {
            return new Promise(function (resolve, reject) {
                logger.silly("in process")
                /**Update In elastic Search, in supperLikeByHistory will not remove userid we use this to show star in user's profile */
                userListType.UpdateWithPush(targetUserId, "supperLikeByHistory", _id, (err, result) => {
                    logger.error(err)

                    if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                });
                /**Update in MongoDB, in supperLikeByHistory will not remove userid we use this to show star in user's profile */
                userListCollection.UpdateByIdWithAddToSet({ _id: targetUserId }, { "supperLikeByHistory": ObjectID(_id) }, (err, result) => {
                    logger.error(err)

                    if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                });

                if (isTargetLikedMe) {
                    logger.silly("in process if")
                    let chatId = new ObjectID();
                    let msg_id = new ObjectID();

                    /**Update In Elastic Search */

                    userListType.UpdateWithPull(_id, "likedBy", targetUserId, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });
                    userListType.UpdateWithPull(targetUserId, "myLikes", _id, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });

                    userListType.UpdateWithPull(_id, "supperLikeBy", targetUserId, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });
                    userListType.UpdateWithPull(targetUserId, "mySupperLike", _id, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });

                    userListType.UpdateWithPush(_id, "matchedWith", targetUserId, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });
                    userListType.UpdateWithPush(targetUserId, "matchedWith", _id, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });

                    /**Update In MongoDB */

                    userListCollection.UpdateByIdWithPull({ _id: _id }, { "likedBy": ObjectID(targetUserId) }, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });
                    userListCollection.UpdateByIdWithPull({ _id: targetUserId }, { "myLikes": ObjectID(_id) }, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });

                    userListCollection.UpdateByIdWithPull({ _id: _id }, { "supperLikeBy": ObjectID(targetUserId) }, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });
                    userListCollection.UpdateByIdWithPull({ _id: targetUserId }, { "mySupperLike": ObjectID(_id) }, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });

                    let condition = { "userId": ObjectID(targetUserId), "targetUserId": ObjectID(_id) };
                    userLikesCollection.Delete(condition, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });

                    userSupperLikeCollection.Delete(condition, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });

                    let dataToInsert = {
                        "initiatedBy": ObjectID(targetUserId),
                        "opponentId": ObjectID(_id),
                        "createdTimestamp": new Date().getTime(),
                        "chatId": chatId,
                        "creationTs": new Timestamp(),
                        "creationDate": new Date()
                    }

                    if (targetBoostId != "") dataToInsert["boostId"] = targetBoostId;
                    if (targetBoostId != "") dataToInsert["boostByUser"] = targetUserId;
                    userMatchCollection.Insert(dataToInsert, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });
                    userListCollection.UpdateByIdWithAddToSet({ _id: _id }, { "matchedWith": ObjectID(targetUserId) }, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });
                    userListCollection.UpdateByIdWithAddToSet({ _id: targetUserId }, { "matchedWith": ObjectID(_id) }, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });

                    });

                    userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
                        let deviceType = "1";
                        if (result && result._id) {
                            /**
                         * this both function is create post on match 
                         */

                            deviceType = result.deviceType || "1";
                        }
                        createPostForMatchOwn();
                        createPostForMatchOppositeUser();
                        let payloadUser1 = {
                            notification: {
                                body: myName + " matched with you! Please introduce yourself."
                                , title: "Match"
                            },
                            data: { type: "1", target_id: _id, deviceType: deviceType }
                        }
                        fcmPush.sendPushToTopic("/topics/" + targetUserId, payloadUser1, (e, r) => { });
                    });
                    let payloadUser2 = {
                        notification: {
                            body: "You got matched with " + firstLikedByName + ".Please introduce yourself."
                            , title: "Match"
                        },
                        data: { type: "1", target_id: targetUserId, deviceType: req.user.deviceType } // values must be string
                    }
                    fcmPush.sendPushToTopic("/topics/" + _id, payloadUser2, (e, r) => { });
                    let cond1 = [
                        {
                            "match": {
                                "user1": targetUserId.toString()
                            }
                        },
                        {
                            "match": {
                                "user2": _id.toString()
                            }
                        }

                    ]

                    userListType.SelectCoinrewardUser(cond1, (err1, result1) => {
                        logger.error(err1)

                        if (result1 && result1.hits.hits.length != 0) {
                            userListType.UpdateInUserPairDetail(result1.hits.hits[0]._id, { "isMatch": true, "matchedStateCode": 1, "matchedStateText": "ACCEPTED", "matchTimestamp": moment().valueOf() }, (ees, reses) => { })

                            result1.hits.hits[0]._source.likes.forEach(element => {
                                element["firstLikedByPhoto"] = firstLikedByPhoto;
                                element["userPhoto"] = req.user.profilePic;
                                rabbitMq.sendToQueue(rabbitMq.Coin_wallet, { isVerified: true, data: element }, (e, r) => {
                                    if (e) {
                                        logger.error("worker error : ", e);
                                    } else {
                                        console.log("DONER")

                                    }
                                });
                            });



                        } else {

                            let cond2 = [
                                {
                                    "match": {
                                        "user2": targetUserId.toString()
                                    }
                                },
                                {
                                    "match": {
                                        "user1": _id.toString()
                                    }
                                }

                            ]
                            userListType.SelectCoinrewardUser(cond2, (err2, result2) => {
                                logger.error(err2)

                                if (result2 && result2.hits.hits.length != 0) {

                                    userListType.UpdateInUserPairDetail(result2.hits.hits[0]._id, { "isMatch": true, "matchedStateCode": 1, "matchedStateText": "ACCEPTED", "matchTimestamp": moment().valueOf() }, (ees, reses) => { })

                                    result2.hits.hits[0]._source.likes.forEach(element => {
                                        element["firstLikedByPhoto"]=firstLikedByPhoto;
                                        element["userPhoto"]=req.user.profilePic;
                                        rabbitMq.sendToQueue(rabbitMq.Coin_wallet, { isVerified: true, data: element }, (e, r) => {
                                            if (e) {
                                                logger.error("worker error : ", e);
                                            } else {

                                                console.log("DONER")
                                            }
                                        });
                                    });

                                }

                            })


                        }




                    })


                    if (isTargetBoostProfile) {
                        userBoostCollection.UpdateByIdWithPush({ _id: targetBoostId },
                            { "match": { userId: ObjectID(_id), timestamp: new Date().getTime() } },
                            (e, r) => { if (e) logger.error(" userBoostCollection.UpdateByIdWithPush : ", e); });

                        userBoostCollection.UpdateWithIncrease({ _id: targetBoostId }, { "count.match": 1 },
                            (e, r) => { if (e) logger.error(" userBoostCollection.UpdateWithIncrease : ", e); });

                        userListCollection.UpdateWithIncrease({ _id: ObjectID(targetUserId) }, { "boost.match": 1 },
                            (e, r) => { if (e) logger.error(" userListCollection.UpdateWithIncrease : ", e); });

                        targetBoostDetails["match"] = targetBoostDetails["match"] + 1
                        mqttClient.publish(targetUserId, JSON.stringify({ "messageType": "boost", boost: targetBoostDetails }), { qos: 0 }, (e, r) => { if (e) logger.error("/boost/match", e) })

                    }
                    req.user.supperLikeByHistory.map(id => id.toString());

                    userListCollection.SelectById({ _id: _id }, { _id: 1, firstName: 1, profilePic: 1 }, (err, result) => {

                        if (result._id) {
                            let dataToSend = {
                                "matchDate": new Date().getTime(),
                                "firstLikedBy": targetUserId,
                                "firstLikedByName": firstLikedByName,
                                "firstLikedByPhoto": firstLikedByPhoto,
                                "isFirstSupperLiked": (req.user && req.user.supperLikeByHistory && req.user.supperLikeByHistory.includes(targetUserId)) ? 1 : 0,
                                "secondLikedBy": _id,
                                "SecondLikedByName": result.firstName,
                                "secondLikedByPhoto": result.profilePic,
                                "isSecondSupperLiked": 1,
                            };


                            let condition = {
                                ["members." + _id]: { "$exists": true },
                                ["members." + targetUserId]: { "$exists": true },
                                "chatType": "NormalChat",
                                "isUnMatched": { "$exists": false }
                            };
                            chatLikesCollection.SelectOne(condition, (err, result) => {
                                if (err) return reject(new Error('Ooops, something broke!'));

                                if (result) {
                                    chatLikesCollection.UpdateById(result._id.toString(), { "isMatchedUser": 1 }, (err, result) => {
                                        if (err) return reject(new Error('Ooops, something broke!'));
                                    })
                                    dataToSend["chatId"] = result._id
                                    dataToSend["messageType"] = "match/user";
                                    mqttClient.publish(_id, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { });
                                    mqttClient.publish(targetUserId, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { });
                                } else {
                                    dataToSend["chatId"] = chatId;
                                    dataToSend["messageType"] = "match/user";
                                    mqttClient.publish(_id, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { });
                                    mqttClient.publish(targetUserId, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { });
                                    let dataToInsert = {
                                        "_id": chatId,
                                        "message": [msg_id],
                                        "members": {
                                            [_id]: {
                                                "status": "NormalMember"
                                            },
                                            [targetUserId]: {
                                                "status": "NormalMember"
                                            }
                                        },
                                        "initiatedBy": ObjectID(_id),
                                        "createdAt": new Date().getTime(),
                                        "chatType": "NormalChat",
                                        "isMatchedUser": 1,
                                        "secretId": "",
                                        creationTs: new Timestamp(),
                                        creationDate: new Date()
                                    };
                                    if (targetBoostId != "") dataToInsert["boostId"] = targetBoostId;
                                    if (targetBoostId != "") dataToInsert["boostByUser"] = targetUserId;
                                    chatLikesCollection.Insert(dataToInsert, (e, r) => { });
                                    /** create dummy message */
                                    let newMessage = {
                                        "_id": msg_id,
                                        "messageId": "" + new Date().getTime(),
                                        "secretId": "",
                                        "dTime": -1,
                                        "senderId": ObjectID(_id),
                                        "receiverId": ObjectID(targetUserId),
                                        "payload": "3embed test",
                                        "messageType": "0",
                                        "timestamp": new Date().getTime(),
                                        "expDtime": 0,
                                        "chatId": chatId,
                                        "userImage": firstLikedByPhoto,
                                        "toDocId": "test message",
                                        "name": firstLikedByName,
                                        "dataSize": null,
                                        "thumbnail": null,
                                        "mimeType": null,
                                        "extension": null,
                                        "fileName": null,
                                        "members": {
                                            [_id]: {

                                            },
                                            [targetUserId]: {
                                                "status": 0
                                            }
                                        },
                                        creationTs: new Timestamp(),
                                        creationDate: new Date(),
                                    }
                                    messagesCollection.Insert(newMessage, (e, r) => { })
                                }
                            });

                            return resolve(dataToSend);
                        } else {
                            return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                        }
                    });
                } else {
                    /**Update In elastic Search */
                    userListType.UpdateWithPush(targetUserId, "supperLikeBy", _id, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke 308!'));
                    });
                    userListType.UpdateWithPush(_id, "mySupperLike", targetUserId, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke 311!'));
                    });
                    /**Update in MongoDB */
                    userListCollection.UpdateByIdWithAddToSet({ _id: targetUserId }, { "supperLikeBy": ObjectID(_id) }, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });
                    userListCollection.UpdateByIdWithAddToSet({ _id: _id }, { "mySupperLike": ObjectID(targetUserId) }, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    });
                    console.log("isTargetBoostProfile ", isTargetBoostProfile)
                    if (isTargetBoostProfile) {
                        userBoostCollection.UpdateByIdWithPush({ _id: targetBoostId },
                            { likes: { userId: ObjectID(_id), timestamp: new Date().getTime() } },
                            (e, r) => { if (e) logger.error(" userBoostCollection.UpdateByIdWithPush : ", e); });

                        userBoostCollection.UpdateWithIncrease({ _id: targetBoostId }, { "count.supperLikes": 1 },
                            (e, r) => { if (e) logger.error(" userBoostCollection.UpdateWithIncrease : ", e); });

                        userListCollection.UpdateWithIncrease({ _id: ObjectID(targetUserId) }, { "boost.supperLikes": 1 },
                            (e, r) => { if (e) logger.error(" userListCollection.UpdateWithIncrease : ", e); });

                        targetBoostDetails["supperLikes"] = targetBoostDetails["supperLikes"] + 1
                        mqttClient.publish(targetUserId, JSON.stringify({ "messageType": "boost", boost: targetBoostDetails }), { qos: 0 }, (e, r) => { if (e) logger.error("/boost/like", e) })
                    }
                    userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
                        let deviceType = "1";
                        if (result && result._id) {
                            deviceType = result.deviceType || "1";
                        }
                        let payload = {
                            notification: { body: myName + " has super liked you. " },
                            data: { type: '2', target_id: _id, deviceType: deviceType } // values must be string  data: { type: "1", target_id: _id }
                        }
                        fcmPush.sendPushToTopic("/topics/" + targetUserId, payload, (e, r) => { });
                    });
                    let dataToInsert = {
                        userId: ObjectID(_id),
                        targetUserId: ObjectID(targetUserId),
                        timestamp: new Date().getTime(),
                        creationTs: new Timestamp(),
                        creationDate: new Date(),
                    }
                    userSupperLikeCollection.Insert(dataToInsert, (err, result) => {
                        if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });

                        return resolve("---");
                    });
                }
            });
        }
        function insertInWalletCusetomerAndUpdateInCoinWallet(value) {
            return new Promise((resolve, reject) => {
                let coinWallet = data.coinWallet;
                let requiredCoin = data.requiredCoin;

                let insertData = [];
                let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
                for (let key in requiredCoin) {

                    if (coinWallet[key]) {
                        insertData.push({
                            "txnId": txnId,
                            "userId": ObjectID(_id),
                            "docType": "on superlike",
                            "docTypeCode": 4,
                            "txnType": "DEBIT",
                            "txnTypeCode": 2,
                            "trigger": `Spent ${requiredCoin[key]} coins on a Super Like`,
                            "currency": "N/A",
                            "currencySymbol": "N/A",
                            "coinType": key,
                            "coinOpeingBalance": coinWallet[key],
                            "cost": 0,
                            "coinAmount": requiredCoin[key],
                            "coinClosingBalance": (coinWallet[key] - requiredCoin[key]),
                            "paymentType": "N/A",
                            "timestamp": timestamp,
                            "transctionTime": timestamp,
                            "transctionDate": timestamp,
                            "paymentTxnId": "N/A",
                            "initatedBy": "customer"
                        });
                        coinWallet[key] = coinWallet[key] - requiredCoin[key];
                    }
                }
                walletCustomerCollection.InsertMany(insertData, () => { });
                coinWalletCollection.Update({ _id: ObjectID(_id) }, { "coins": coinWallet }, () => { });
                coinWallet = data;
                return resolve(value);
            });
        }

    }
    catch (err) {
        console.log("===========================>", err)
    }
    /**
*
* @description This both function is use to create user new post for matches user only .
* @author jaydip Haraniya
* @date 24-05-2019
* @param {string} Type  5  only 
* @param {string} typeFlag  match
* @param {string} Timestamp
* @param {string} description  match post
* @param {string} url
* @param {string} likeCount
* @param {string} commentCount
* @param {string} Likers
* @param {string} commenters
* @property {string} language in header
* @returns  500:internal server error 
* @returns  200:Success
* @returns  400:Bad request
* @returns  429:You have exceeded the max attempt
* @returns  404:Data Not Found
*/


    function createPostForMatchOwn() {




        let typeFlag = 5;
        let Type = "match";
        let userId = new ObjectID(targetUserId);
        let _ID = new ObjectID();
        var data;

        data = {
            _id: _ID,
            userId: ObjectID(userId),
            targetId: ObjectID(userId),
            type: Type,
            typeFlag: typeFlag,
            userName: firstLikedByName,
            profilePic: firstLikedByPhoto,
            postedOn: moment().valueOf(),
            createdOn: new Timestamp(),
            date: new Date(),
            description: "IT'S A MATCH !",
            url: [firstLikedByPhoto],
            likeCount: 0,
            commentCount: 0,
            Likers: [],
            commenters: [],
            longitude: req.user.location.longitude || 0,
            latitude: req.user.location.latitude || 0,
        }
        userPostCollection.Insert(data, (err, result) => {
            logger.error(err)
            console.log(err)

            if (result) {

                userPostCollectionES.Insert(data, (e, r) => {
                    logger.error(e)

                })
                //return resolve("----");
            } else {
                logger.error(e)

                // return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 204 });
            }
        });

    }

    function createPostForMatchOppositeUser() {



        let typeFlag = 5;
        let Type = "match";
        let userId = new ObjectID(_id);
        let _ID = new ObjectID();
        var data;

        data = {
            _id: _ID,
            userId: ObjectID(userId),
            targetId: ObjectID(userId),
            type: Type,
            userName: req.user.firstName,
            profilePic: req.user.profilePic,
            typeFlag: typeFlag,
            postedOn: moment().valueOf(),
            createdOn: new Timestamp(),
            date: new Date(),
            description: "IT'S A MATCH !",
            url: [req.user.profilePic],
            likeCount: 0,
            commentCount: 0,
            Likers: [],
            commenters: [],
            longitude: req.user.location.longitude || 0,
            latitude: req.user.location.latitude || 0,
        }
        userPostCollection.Insert(data, (err, result) => {
            console.log(err)

            if (result) {

                userPostCollectionES.Insert(data, (e, r) => {
                    logger.error(e)

                })

                // return resolve("----");
            } else {
                logger.error(e)
            }
        });

    }
};

let response = {
    status: {
        201: { message: Joi.any().default(local['PostSupperLike']['201']), data: Joi.any(), coinWallet: Joi.any() },
        200: { message: Joi.any().default(local['PostSupperLike']['200']), coinWallet: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        402: { message: Joi.any().default(local['genericErrMsg']['402']) },
        405: { message: Joi.any().default(local['genericErrMsg']['405']) },
        409: { message: Joi.any().default(local['genericErrMsg']['409']) },
        412: { message: Joi.any().default(local['PostSupperLike']['412']) },
    }
}

module.exports = { APIHandler, validator, response }