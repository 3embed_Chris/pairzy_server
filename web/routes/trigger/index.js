'use strict'
let PostAPI = require('./Post');
let headerValidator = require('../../middleware/validator');

module.exports = [   
    {
        method: 'POST',
        path: '/trigger',
        handler: PostAPI.handler,
        config: {
            description: 'This API will be used to get a plans',
            tags: ['api', 'plans'],
            auth: 'userJWT',
            response: PostAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];