'use strict'
let PostAPI = require('./Post');
let headerValidator = require("../../middleware/validator")


module.exports = [
    {
        method: 'POST',
        path: '/unCompleteUser',
        handler: PostAPI.handler,
        config: {
            description: `This API use for signup or login from facebook with facebook token`,
            tags: ['api', 'Login'],
            auth: false,
            validate: {
                payload: PostAPI.validator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];