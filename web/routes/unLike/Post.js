'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;

const local = require('../../../locales');
const userListCollection = require('../../../models/userList');
const userListType = require('../../../models/userListType');
const userUnlikesCollection = require('../../../models/userUnlikes');
const redisClient = require('../../../models/redisDB').client;
const rabbitMq = require('../../../library/rabbitMq');


let validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId").error(new Error('targetUserId is missing'))
}).required();


/**
 * @method POST unLike
 * @description This API use to post  unlike.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} lang targetUserId
 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let targetUserId = req.payload.targetUserId;
    logger.silly("targetUserId ", targetUserId)
    let isAdded = false;
    let bulkArray = [];
    let condition = { _id: ObjectID(_id), myunlikes: ObjectID(targetUserId) };

    userListCollection.Select(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('PostUnLike')['412'] }).code(412);
        } else if (result[0]) {
            logger.silly("already disliked ")
            bulkArray.push({ "update": { "_id": `${targetUserId}`, "_type": "userList", "_index": process.env.ELASTICK_INDEX, "retry_on_conflict": 3 } },
                { "script": { "source": "ctx._source.disLikedUSers.add('" + _id + "')", "lang": "painless" } },
                { "update": { "_id": `${_id}`, "_type": "userList", "_index": process.env.ELASTICK_INDEX, "retry_on_conflict": 3 } },
                { "script": { "source": "ctx._source.myunlikes.add('" + targetUserId + "')", "lang": "painless" } }
            );
        
            rabbitMq.sendToQueue(rabbitMq.update_bulk,bulkArray , (e, r) => {
                if (e) {
                    logger.error("OSKDCO : ", e);
                }
            }); 
            return res({ message: req.i18n.__('PostUnLike')['200'] }).code(200);
        } else {
            targetUserExists()
                .then(function (value) {
                    return processUnLike();
                }).then(function (value) {
                
                    rabbitMq.sendToQueue(rabbitMq.update_bulk,bulkArray , (e, r) => {
                        if (e) {
                            logger.error("OSKDCO : ", e);
                        }
                    });
                    return res({ message: req.i18n.__('PostUnLike')['200'] }).code(200);
                }).catch(function (err) {
                    logger.silly('Caught an error!', err);
                    userListType.Delete("userList", { _id: targetUserId }, () => { });
                    return res({ message: req.i18n.__('PostUnLike')['412'] }).code(412);
                });
        }
    });

    function targetUserExists() {
        return new Promise(function (resolve, reject) {
            userListCollection.SelectById({ _id: targetUserId }, { firstName: 1, profilePic: 1, currentLoggedInDevices: 1 }, (err, result) => {
                if (result && result._id) {
                    resolve(result);
                } else {
                    userListType.Delete("userList", { _id: targetUserId }, () => { });
                    reject(new Error('Ooops, something broke!'));
                }
            });
        });
    }

    function processUnLike() {
        return new Promise(function (resolve, reject) {

            redisClient.hmset(_id, { [targetUserId]: targetUserId }, (e, r) => {
                logger.error("insert in redis ", ((e) ? e : ""));
            });
            redisClient.expire(_id, 20, () => { })
            console.log("_id  ", _id);
            /**Update In elastic Search */

            bulkArray.push({ "update": { "_id": `${targetUserId}`, "_type": "userList", "_index": process.env.ELASTICK_INDEX, "retry_on_conflict": 3 } },
                { "script": { "source": "ctx._source.disLikedUSers.add('" + _id + "')", "lang": "painless" } },
                { "update": { "_id": `${_id}`, "_type": "userList", "_index": process.env.ELASTICK_INDEX, "retry_on_conflict": 3 } },
                { "script": { "source": "ctx._source.myunlikes.add('" + targetUserId + "')", "lang": "painless" } }
            );

            /**Update in MongoDB */
            userListCollection.UpdateByIdWithAddToSet({ _id: targetUserId }, { "disLikedUSers": ObjectID(_id) }, (err, result) => {
                if (err) {
                    logger.error(err);
                    return reject(new Error('Ooops, something broke!'));
                }
            });
            userListCollection.UpdateByIdWithAddToSet({ _id: _id }, { "myunlikes": ObjectID(targetUserId) }, (err, result) => {
                if (err) {
                    logger.error(err);
                    return reject(new Error('Ooops, something broke!'));
                }
            });

            userListCollection.Update({ _id: ObjectID(_id) }, { "lastUnliked": { "userId": ObjectID(targetUserId), timestamp: new Date().getTime() } }, (err, result) => {
                if (err) {
                    logger.error(err);
                    return reject(new Error('Ooops, something broke!'));
                }
            });

            let dataToInsert = {
                userId: ObjectID(_id),
                targetUserId: ObjectID(targetUserId),
                timestamp: new Date().getTime(),
                "creationTs": new Timestamp(),
                "creationDate": new Date()
            }
            userUnlikesCollection.Insert(dataToInsert, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
                return resolve("---");
            });
        });
    }
};

let response = {
    status: {
        200: { message: local['PostUnLike']['200'] },
        412: { message: local['PostUnLike']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

module.exports = { APIHandler, validator, response }