'use strict'
let headerValidator = require('../../middleware/validator');
let postUnLikeAPI = require('./Post');
let getAPI = require('./Get');
let GetByAPI = require('./GetBy');

module.exports = [
    {
        method: 'POST',
        path: '/unLike',
        handler: postUnLikeAPI.APIHandler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'unLike'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postUnLikeAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/myUnLikes',
        handler: getAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'unLike'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: getAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getAPI.response
        }
    }, {
        method: 'GET',
        path: '/unLikesBy',
        handler: GetByAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'unLike'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetByAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByAPI.response
        }
    }
];