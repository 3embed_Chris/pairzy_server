'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
var moment = require("moment");
const local = require('../../../locales');
const userPostCollectionES = require('../../../models/userPostES');
const userPostCollection = require("../../../models/userPost");


let validator = Joi.object({
    postId: Joi.string().required().description("object ID of post ").error(new Error('objectId  is missing'))
}).unknown()

let APIHandler = (req, res) => {

    /**
   * @method PUT update delete status
   * @description This API is use to update post status.
   * @author jaydip Haraniya
   * @date 29-05-2019
  * @param {string} postId  this is for the post id which user want to delete 
  * @param {string} status this is the flag value for deletestatus 1:delete 0:active
  * @param {string} deleteTimeStamp
  * @property {string} language in header
  * @returns  500:internal server error 
  * @returns  200:Success
  * @returns  400:Bad request
  * @returns  429:You have exceeded the max attempt
  * @returns  404:Data Not Found
   */
    let postId = req.payload.postId.toString();
    var dataToUpdate = {
        status: 1,
        deleteTimeStamp: moment().valueOf()
    };

    updatePostStatus()
        .then(value => {
            return updateIntoElasticSearch();
        })
        .then(value => {
            return res({ message: req.i18n.__('deleteUserPost')['200'] }).code(200);
        }).catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });

    /**
     *  using this function we update delete status in mongoDB userPost collection
     */
    function updatePostStatus() {
        return new Promise(function (resolve, reject) {

            userPostCollection.Update({ "_id": ObjectID(postId) }, dataToUpdate, (err, result) => {
                logger.error("userPostCollection ", (err) ? err : "")
            });
            return resolve(true);
        });
    }
    /**
*  using this function we update delete status in elastic search userPost collection
*/
    function updateIntoElasticSearch() {
        return new Promise(function (resolve, reject) {

            userPostCollectionES.Update(postId, dataToUpdate, (err, result) => {
                logger.error("userPostCollection ", (err) ? err : "")
            });
            return resolve(true);
        });
    }
};

let response = {
    status: {
        200: { message: local['deleteUserPost']['200'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

module.exports = { APIHandler, response, validator }