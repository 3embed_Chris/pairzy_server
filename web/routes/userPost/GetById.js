'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const userListCollection = require('../../../models/userList');
const userPostCollectionES = require('../../../models/userPostES');
const local = require('../../../locales');
const GeoPoint = require('geopoint')

/**
 * @function GET byUserMatchId
 * @description This API is used to get all match user post.
 * @property {string} authorization - authorization
 * @property {string} lang - language
  * @author jaydip Haraniya
 * @date 24-05-2019
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : post doesnot exist in the database. 
 * @returns  500 : An unknown error has occurred.
 **/


let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let liked = false
    let limit = parseInt(req.query.limit) || 20;
    let skip = parseInt(req.query.offset) || 0;
    /**
     * in this function we getting match user list from mongodb userlList collection
     */
    const getmatchUser = () => {
        return new Promise((resolve, reject) => {

            userListCollection.SelectById({ _id }, { matchedWith: 1 }, (err, result) => {
                if (result) {
                    return resolve(result.matchedWith);
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 204 });
                }
            });
        });
    };

    /**
        * in this function we search  all post of match user from elastic search
        */
    const matchUserPost = (data) => {
        return new Promise(function (resolve, reject) {

            //console.log("================>",data)
            data.push(_id)
            userPostCollectionES.SelectMatch(data, limit, skip, _id, (err, result) => {
                if (result && result.hits.hits.length != 0) {
                    let rsss = [];

                    result.hits.hits.forEach(e => {
                        if (e._source.Likers && e._source.Likers.map(id => id.toString()).includes(_id)) {

                            liked = true
                            // console.log("its is alreay like")
                        } else {
                            liked = false
                            // console.log("u can like not like")

                        }

                        let point1 = new GeoPoint(req.user.location.latitude, req.user.location.longitude);
                        let point2 = new GeoPoint(e._source.latitude, e._source.longitude);
                        let distance = point1.distanceTo(point2, true).toFixed(2)//output in kilometers

                        rsss.push({
                            "liked": liked,
                            "postId": e._id,
                            "userId": e._source.userId,
                            "userName": e._source.userName,
                            "type": e._source.type,
                            "typeFlag": e._source.typeFlag,
                            "isPairSuccess": (e._source && e._source.isPairSuccess) ? e._source.isPairSuccess : false,
                            "postedOn": e._source.postedOn,
                            "profilePic": e._source.profilePic,
                            "date": e._source.date,
                            "description": e._source.description,
                            "status": e._source.status,
                            "url": e._source.url,
                            "likeCount": e._source.likeCount,
                            "commentCount": e._source.commentCount,
                            "distance": (distance) ? distance : 0


                        })

                        // rsss.push(e._source.postId = e._id)
                        // rsss.push(e._source);
                    });
                    return resolve(rsss);
                } else {
                    return reject({ message: req.i18n.__('GetPostById')['412'], code: 412 })
                }
            });
        })
    };

    getmatchUser()
        .then(matchUserPost)
        .then(dt => {
            return res({ message: req.i18n.__('GetPostById')['200'], data: dt }).code(200);
        }).catch(e => {
            return res({ message: e.message }).code(e.code);

        })

}
let validator = Joi.object({
    offset: Joi.number().default(0).description("0").error(new Error('offset is missing')),
    limit: Joi.number().default(20).description("0").error(new Error('limit is missing'))
}).unknown();

let response = {
    status: {
        200: { message: local['GetPostById']['200'], data: Joi.any() },
        412: { message: local['GetPostById']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

module.exports = { handler, response, validator }