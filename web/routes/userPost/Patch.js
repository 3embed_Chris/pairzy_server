'use strict'
const Joi = require("joi");
const logger = require('winston');
const local = require('../../../locales');
const userPostCollectionES = require('../../../models/userPostES');
const userPostCollection = require("../../../models/userPost");

let validator = Joi.object({
    typeFlag: Joi.number().description("bio update :1, Profile Photo Update :2 , photo post:3 , video post:4 ,match :5 ").error(new Error('type  is missing')),
    description: Joi.string().description("Description about post").error(new Error('Description is missing')),
    url: Joi.string()
        .description("photo Link, Eg. https://res.cloudinary.com/closebrace/image/upload/w_400/v1491315007/usericon_id76rb.png")
        .default("https://res.cloudinary.com/closebrace/image/upload/w_400/v1491315007/usericon_id76rb.png")
        .error(new Error('url is missing')),
    postId: Joi.string().required().description("object ID of post ").error(new Error('objectId  is missing'))
}).unknown()

let handler = (req, res) => {


    let postId = req.payload.postId
    var data;

    /**
   * @method PUT post
   * @description This API is use to update user  post.
   * @author jaydip Haraniya
   * @date 29-05-2019
  * @param {string} Type  this is for the type of post 
  * @param {string} typeFlag this is the flag value for the type of post
  * @param {string} description
  * @param {string} url
  * @property {string} language in header
  * @returns  500:internal server error 
  * @returns  200:Success
  * @returns  400:Bad request
  * @returns  429:You have exceeded the max attempt
  * @returns  404:Data Not Found
   */

    const updatePost = () => {
        return new Promise((resolve, reject) => {

            if (req.payload.typeFlag) {
                let typeFlag = req.payload.typeFlag;
                let type = (typeFlag == 1) ? "bioUpdate" : (typeFlag == 2) ? "profilePhotoUpdate" : (typeFlag == 3) ? "photoPost" : (typeFlag == 4) ? "videoPost" : (typeFlag == 5) ? "match" : (typeFlag == 6) ? "profileVideoUpdate" : "N/A";
                data.type = type;
                data.typeFlag = typeFlag;
            }
            if (req.payload.description) data.description = description;
            if (req.payload.url) data.url = url;

            userPostCollection.Update(postId, data, (err, result) => {
                logger.error(err)
                if (result) {
                    return resolve("----");
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                }
            });
        });
    }
    updatePost()
        .then(updateIntoElastic)
        .then(dt => {
            return res({ message: req.i18n.__('putUserPost')['200'] }).code(200);
        }).catch(e => {
            return res({ message: e.message }).code(e.code);

        })

    const updateIntoElastic = () => {
        return new Promise(function (resolve, reject) {
            userPostCollectionES.Update(postId, data, (err, result) => {
                if (err) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });

                return resolve({ data: result[0] });
            });
        });
    }
};

let response = {
    status: {
        200: { message: local['putUserPost']['200'] },
        412: { message: local['putUserPost']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

module.exports = { handler, validator, response }