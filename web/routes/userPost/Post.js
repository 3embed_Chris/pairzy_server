'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
var moment = require("moment");
const fcmPush = require("../../../library/fcm");
const local = require('../../../locales');
const userListCollection = require('../../../models/userList');
const userPostCollectionES = require('../../../models/userPostES');
const userPostCollection = require("../../../models/userPost");

let validator = Joi.object({
    typeFlag: Joi.number().required().description("photo post:3 , video post:4").error(new Error('type  is missing')),
    description: Joi.string().required().description("Description about post").error(new Error('Description is missing')),
    url: Joi.string()
        .description("photo Link, Eg. https://res.cloudinary.com/closebrace/image/upload/w_400/v1491315007/usericon_id76rb.png")
        .default("https://res.cloudinary.com/closebrace/image/upload/w_400/v1491315007/usericon_id76rb.png")
        .error(new Error('url is missing')),
}).required();

let handler = (req, res) => {

    let typeFlag = req.payload.typeFlag;
    let ty = (parseInt(req.payload.typeFlag) == 3) ? 1 : 2
    let Type = (typeFlag == 3) ? "photo Post" : (typeFlag == 4) ? "video Post" : "bio Update"
    let userId = req.auth.credentials._id;
    let _id = new ObjectID();
    let deviceType;
    var data;
    /**
   * @method POST post
   * @description This API is used to create new user post.
   * @author jaydip Haraniya
   * @date 24-05-2019
  * @param {string} userId 
  * @param {string} Type  this is for the type of post 
  * @param {string} typeFlag this is the flag value for the type of post
  * @param {string} Timestamp
  * @param {string} description this is the post description
  * @param {string} url  this is the post url
  * @param {string} likeCount
  * @param {string} commentCount
  * @param {string} Likers
  * @param {string} commenters
  * @param {number} status  0:activePost 1:deletedPost 
  * @property {string} language in header
  * @returns  500:internal server error 
  * @returns  200:Success
  * @returns  400:Bad request
  * @returns  429:You have exceeded the max attempt
  * @returns  404:Data Not Found
   */




    /*  create and insert post into mongoDB 
     */
    const createPost = () => {
        return new Promise((resolve, reject) => {
            data = {
                _id: _id,
                userId: ObjectID(userId),
                targetId: "",
                userName: req.user.firstName,
                profilePic: req.user.profilePic,
                type: Type,
                typeFlag: ty,
                postedOn: moment().valueOf(),
                createdOn: new Timestamp(),
                date: new Date(),
                description: req.payload.description,
                status: 0,
                url: [req.payload.url],
                likeCount: 0,
                commentCount: 0,
                Likers: [],
                longitude: req.user.location.longitude || 0,
                latitude: req.user.location.latitude || 0,

            }
            userPostCollection.Insert(data, (err, result) => {
                logger.error(err)
                if (result) {
                    return resolve(true);
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                }
            });
        });
    }
    /*  create and insert post into Elastic search 
        */

    const inserIntoElastic = () => {
        return new Promise(function (resolve, reject) {
            userPostCollectionES.Insert(data, (err, result) => {
                if (err) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                if (result) {
                    sendPush()
                }
                return resolve(true);
            });
        });
    }

    /*  send  fcm push to all matches user 
    */
    function sendPush() {

        var condition = { _id: { "$in": req.user.matchedWith } }
        userListCollection.Select(condition, (err, result) => {
            logger.error(err)
            if (result && result.length > 0) {



                //  console.log("resulttttt", result)
                deviceType = result.deviceType || "1";
                result.forEach(element => {
                    console.log("element._id", element._id, userId)

                    if (element._id != userId) {

                        let request = {
                            data: {
                                type: "32", deviceType: deviceType, title: process.env.APPNAME, message: "Your match " + req.user.firstName + " created an activity post."
                            },
                            notification: { "title": process.env.APPNAME, "body": "Your match " + req.user.firstName + " created an activity post." }
                        };
                        fcmPush.sendPushToTopic(`/topics/${element._id}`, request, () => { })
                    }
                })


            } else {
                logger.error("fcm error : - ", err)
            }
        });





    }
    createPost()
        .then(inserIntoElastic)
        .then(dt => {
            return res({ message: req.i18n.__('userPost')['200'] }).code(200);
        }).catch(e => {
            return res({ message: e.message }).code(e.code);

        })
};

let response = {
    status: {
        200: { message: local['userPost']['200'] },
        412: { message: local['userPost']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

module.exports = { handler, validator, response }