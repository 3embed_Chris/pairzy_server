'use strict'
let patchAPI = require('./Patch');
let postAPI = require('./Post');
let GetProfileByIdAPI = require('./GetById');
let DeleteAPI = require('./Delete');
let headerValidator = require("../../middleware/validator");


module.exports = [
    {
        method: 'PUT',
        path: '/userPost',
        handler: DeleteAPI.APIHandler,
        config: {
            description: 'This API used to delete User post ',
            tags: ['api', 'userPost'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'GET',
        path: '/userPost',
        handler: GetProfileByIdAPI.handler,
        config: {
            description: 'This API used to get User post by Id',
            tags: ['api', 'userPost'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetProfileByIdAPI.validator,

                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetProfileByIdAPI.response
        }
    },
  
    {
        method: 'POST',
        path: '/userPost',
        handler: postAPI.handler ,
        config: {
            description: 'This API is  use to post User Post.',
            tags: ['api', 'userPost'],
            auth: "userJWT",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    },
    {
        method: 'PATCH',
        path: '/userPost',
        handler: patchAPI.handler,
        config: {
            description: 'This API use to update userPost.',
            tags: ['api', 'userPost'],
            auth: "userJWT",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: patchAPI.response
        }
    }
];