'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');

const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const otps_mongoDB = require('../../../models/otps');
const coinConfig = require('../../../models/coinConfig');
const coinWallet = require('../../../models/coinWallet');
const Auth = require("../../middleware/authentication.js");
const userList_mongoDB = require('../../../models/userList');
const userListType_ES = require('../../../models/userListType');
const userPrefrances = require('../../../models/userPrefrances');
const userDevicesCollection = require('../../../models/userDevices');
const searchPreferencesCollection = require('../../../models/searchPreferences');

const Config = process.env;
const Timestamp = require('mongodb').Timestamp;

let validator = Joi.object({
    phoneNumber: Joi.string().required().description("Country Code and Phonenumber(without space), Eg. +919620826142").error(new Error('phoneNumber is missing')),
    type: Joi.number().required().min(1).max(2).description("Type-1(New registration || login), Type-2(Recovery password), Eg. 1/2").error(new Error('type is missing')),
    otp: Joi.number().required().description("Enter OTP(4-digit), Eg. 1111").error(new Error('otp is missing')),
    pushToken: Joi.string().allow("").description("Push Token, Eg. dY0xOny_uEA:APA91bE_HZQiTq").error(new Error('pushToken is missing')),
    deviceId: Joi.string().allow("").description("Device Id, Eg. 157875de315458000000000000000").error(new Error('deviceId is missing')),
    deviceMake: Joi.string().allow("").description("Device Make/Company, Eg. Samsung/Apple").error(new Error('deviceMake is missing')),
    deviceModel: Joi.string().allow("").description("Device Model Number, Eg. SM-N920T").error(new Error('deviceModel is missing')),
    deviceType: Joi.string().allow("").description("Device Type(1-IOS and 2-Android), Eg. 2").error(new Error('deviceType is missing')),
    deviceOs: Joi.string().allow("").description("deviceOs, Eg. Oreo").error(new Error('deviceOs is missing')),
    appVersion: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('appVersion is missing')),
    voipiospush: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('voipiospush is missing')),
    apnsPush: Joi.string().allow("").description("apnsPush, Eg. 1.0.0").error(new Error('apnsPush is missing')),
    // isIosAppInDevelopment: Joi.boolean().allow("").default(false).description("isIosAppInDevelopment, Eg. 1.0.0")
}).unknown();

let handler = (req, res) => {

    let phoneNumber = req.payload.phoneNumber;
    let randomCode = req.payload.otp;
    let dataToSend = { isNewUser: true };
    let coinWalletObj = {};

    try {
        if (req.payload.type == 1) {
            checkOTP()
                .then((value) => { return checkUser(); })
                .then((value) => { return getCoinBalance(); })
                .then((value) => { return getCoinConfig(); })
                .then((value) => {
                    return res({
                        message: req.i18n.__('PostVerificationCode')['200'],
                        data: dataToSend,
                        coinConfig: value.coinConfig || {},
                        coinWallet: coinWalletObj[0].coins
                    }).code(200);
                })
                .catch((err) => {
                    logger.error('Caught an error!', err);
                    return res({ message: err.message });
                });
        } else if (req.payload.type == 2) {
            return res({ message: req.i18n.__('PostVerificationCode')['412'] }).code(412);
        }

    } catch (error) {
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }
    function checkOTP() {
        return new Promise(function (resolve, reject) {
            let condition = [
                { "$match": { "phone": phoneNumber, "type": req.payload.type, "otp": randomCode } },
                { "$sort": { time: -1 } }, { "$limit": 1 }
            ];

            otps_mongoDB.Aggregate(condition, (err, result) => {
                if (err) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });

                if (result.length) {
                    return resolve(result[0]);
                } else {
                    return reject({ message: req.i18n.__('PostVerificationCode')['412'], code: 412 });
                }
            });
        });
    }
    function checkUser() {
        return new Promise(function (resolve, reject) {

            let condition = { "contactNumber": phoneNumber, deleteStatus: { "$ne": 1 } };

            userList_mongoDB.Select(condition, (err, result) => {
                if (err) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });

                if (result && result[0] && result[0].profileStatus) {
                    return reject({ message: req.i18n.__('genericErrMsg')['403'], code: 403 });
                } else if (result && result[0]) {

                    userList_mongoDB.Update({ _id: ObjectID(result[0]._id.toString()) }, {
                        voipiospush: req.payload.voipiospush, apnsPush: req.payload.apnsPush
                        // isIosAppInDevelopment: req.payload.isIosAppInDevelopment
                    }, () => { })

                    let cur = new Date();
                    let diff = cur - result[0].dob; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25

                    dataToSend["_id"] = result[0]._id;
                    dataToSend["countryCode"] = result[0].countryCode;
                    dataToSend["contactNumber"] = result[0].contactNumber;
                    dataToSend["firstName"] = result[0].firstName;
                    dataToSend["dob"] = result[0].dob;
                    dataToSend["profilePic"] = result[0].profilePic;
                    dataToSend["gender"] = result[0].gender;
                    let token = Auth.SignJWT({ _id: result[0]._id, key: 'acc' }, 'user', 60000000);
                    dataToSend["token"] = token;
                    dataToSend["isNewUser"] = false;
                    dataToSend["height"] = result[0].height;
                    dataToSend["email"] = result[0].email;
                    dataToSend["profileVideo"] = result[0].profileVideo;
                    dataToSend["profileVideoThumbnail"] = result[0].profileVideoThumbnail;
                    dataToSend["otherImages"] = result[0].otherImages;
                    dataToSend["about"] = result[0].about;
                    dataToSend["instaGramProfileId"] = result[0].instaGramProfileId || "";
                    dataToSend["instaGramToken"] = result[0].instaGramToken || "";
                    dataToSend["eduation"] = "";
                    dataToSend["work"] = "";
                    dataToSend["job"] = "";
                    dataToSend["age"] = { value: age, isHidden: result[0].dontShowMyAge || false };
                    dataToSend["distance"] = { value: 0, isHidden: result[0].dontShowMyDist || false };
                    dataToSend["subscription"] = result[0]["subscription"] || [];
                    dataToSend["count"] = result[0]["count"] || {};
                    dataToSend["isPassportLocation"] = result[0]["isPassportLocation"] || false;
                    dataToSend["location"] = result[0]["location"]

                    let myPrefrances = [];
                    let prefrances = {}
                    let _id = result[0]._id;
                    let searchPreferencesObj = {};
                    let searchPreferences = [];
                    let myPrefrancesDataToSend = []
                    let MyVices = []
                    let MyVitals = []
                    let MyVirtues = []

                    async.series([
                        function (callback) {
                            userPrefrances.Select({ mandatory: true }, (err, data) => {
                                myPrefrances = data;
                                result[0].myPreferences.forEach(element => {
                                    myPrefrances.find(e => {
                                        if (e._id == element.pref_id.toString()) {
                                            switch (e.title) {
                                                case "My Vices":

                                                    MyVices.push({
                                                        "title": e.title,
                                                        "label": e.label,
                                                        "type": e.type,
                                                        "priority": e.priority,
                                                        "mandatory": e.mandatory,
                                                        "ActiveStatus": e.ActiveStatus,
                                                        "pref_id": element.pref_id,
                                                        "isDone": element.isDone,
                                                        "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                                        "options": e.options
                                                    })
                                                    break;
                                                case "My Vitals":
                                                    MyVitals.push({
                                                        "title": e.title,
                                                        "label": e.label,
                                                        "type": e.type,
                                                        "priority": e.priority,
                                                        "mandatory": e.mandatory,
                                                        "ActiveStatus": e.ActiveStatus,
                                                        "pref_id": element.pref_id,
                                                        "isDone": element.isDone,
                                                        "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                                        "options": e.options
                                                    })
                                                    break
                                                case "My Virtues":
                                                    MyVirtues.push({
                                                        "title": e.title,
                                                        "label": e.label,
                                                        "type": e.type,
                                                        "priority": e.priority,
                                                        "mandatory": e.mandatory,
                                                        "ActiveStatus": e.ActiveStatus,
                                                        "pref_id": element.pref_id,
                                                        "isDone": element.isDone,
                                                        "selectedValues": (element.isDone == true) ? element.selectedValues : [],
                                                        "options": e.options
                                                    })
                                                default:
                                                    break;
                                            }
                                            return true
                                        }
                                    })
                                });
                                myPrefrancesDataToSend.push({
                                    "title": "My Vices",
                                    "data": MyVices
                                })
                                myPrefrancesDataToSend.push({
                                    "title": "My Vitals",
                                    "data": MyVitals
                                })
                                myPrefrancesDataToSend.push({
                                    "title": "My Virtues",
                                    "data": MyVirtues
                                })
                                myPrefrances = myPrefrancesDataToSend
                                console.log("result[0].myPreferences", myPrefrancesDataToSend)
                                callback(err, data);
                            })
                        },
                        function (callback) {
                            searchPreferencesCollection.Select({ isSearchPreference: true }, (err, result) => {
                                if (result[0]) {
                                    let pref_id = "";
                                    for (let index = 0; index < result.length; index++) {
                                        pref_id = result[index]._id;
                                        searchPreferencesObj[pref_id] = result[index];
                                        searchPreferencesObj[pref_id]["selectedValue"] = searchPreferencesObj[pref_id]["options"];
                                        delete searchPreferencesObj[pref_id]["ActiveStatus"];
                                        if (result[index]["TypeOfPreference_User"]) {
                                            delete searchPreferencesObj[pref_id]["TypeOfPreference_User"];
                                        }
                                    }
                                }
                                callback(null, 1);
                            })
                        },
                        function (callback) {
                            for (let index = 0; index < result[0].searchPreferences.length; index++) {
                                let pref_id = result[0].searchPreferences[index].pref_id;
                                if (searchPreferencesObj[pref_id]) {
                                    searchPreferencesObj[pref_id]["selectedValue"] = result[0].searchPreferences[index]["selectedValue"];
                                    if (result[0].searchPreferences[index]["selectedUnit"]) {
                                        searchPreferencesObj[pref_id]["selectedUnit"] = result[0].searchPreferences[index]["selectedUnit"];
                                    }
                                }
                            }
                            for (let pref_id in searchPreferencesObj) {
                                searchPreferences.push(searchPreferencesObj[pref_id])
                            }
                            callback(null, 1);
                        },
                        function (callback) {
                            let deviceData = {
                                userId: _id,
                                creationDate: new Date(),
                                deviceId: req.payload.deviceId || "",
                                pushToken: req.payload.pushToken || "",
                                deviceMake: req.payload.deviceMake || "",
                                deviceModel: req.payload.deviceModel || "",
                                deviceType: req.payload.deviceType || "",
                                deviceOs: req.payload.deviceOs || "",
                                appVersion: req.payload.appVersion || "",
                                DevicetypeMsg: (req.payload.deviceType == "1") ? "iOS" : "Android",

                            };
                            /* insert into userDevices collection*/

                            userDevicesCollection.Insert(deviceData, (err, result) => {
                                callback(err, result);
                            })

                        },
                        function (callback) {
                            let deviceData = {
                                currentLoggedInDevices: {
                                    deviceId: req.payload.deviceId || "",
                                    pushToken: req.payload.pushToken || "",
                                    creationDate: new Date(),
                                    deviceMake: req.payload.deviceMake || "",
                                    deviceModel: req.payload.deviceModel || "",
                                    deviceType: req.payload.deviceType || "",
                                    deviceOs: req.payload.deviceOs || "",
                                    appVersion: req.payload.appVersion || "",
                                    DevicetypeMsg: (req.payload.deviceType == "1") ? "iOS" : "Android",
                                }
                            };
                            /* update  into userList type in elastic search*/
                            userListType_ES.Update(_id, deviceData, () => { });
                            /* update  into userList  collection*/

                            userList_mongoDB.UpdateById(_id, deviceData, (err, result) => {
                                callback(err, result);
                            });
                        }
                    ], () => {
                        dataToSend["myPreferences"] = myPrefrances;
                        dataToSend["searchPreferences"] = searchPreferences;
                        return resolve("--");
                    })

                } else {
                    return resolve("--");
                }
            });
        });
    }
    function getCoinConfig() {
        return new Promise(function (resolve, reject) {
            coinConfig.Select({}, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke! at getCoinConfig'));

                return resolve({ coinConfig: result[0] || { coinConfig: {} } });
            });
        });
    }
    function getCoinBalance() {
        return new Promise(function (resolve, reject) {
            coinWallet.Select({ _id: ObjectID(dataToSend["_id"]) }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke! at getCoinConfig'));

                coinWalletObj = (dataToSend.isNewUser) ? [{ coins: { "Coin": 0 } }] : result;
                return resolve(true);
            });
        });
    }
};

module.exports = { handler, validator }