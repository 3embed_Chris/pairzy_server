'use strict'
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const userListCollection = require('../../../models/userList');
const local = require('../../../locales');


const Config = process.env;
const cryptr = new Cryptr(Config.SECRET_KEY);

let payloadValidator = Joi.object({
    email: Joi.string().required().description("enter emailID").example("example@app.com").error(new Error('email is missing')),
}).required();

/**
 * @method PATCH verificationEmailidExists
 * @description This API use to PATCH verificationEmailidExists.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} email email
 
 * @returns  200 : Email-id exist in the database.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Email-id doesnot exist in the database.
 * @returns  500 : An unknown error has occurred.
 */
let APIHandler = (req, res) => {


    let email = req.payload.email;
    let condition = { email: email.toLowerCase(),"deleteStatus": { '$exists': false, '$ne': 1 } };
    userListCollection.Select(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result[0]) {
            return res({ message: req.i18n.__('PatchEmailIdExistsVerificaiton')['200'] }).code(200);
        } else {
            return res({ message: req.i18n.__('PatchEmailIdExistsVerificaiton')['412'] }).code(412);
        }
    });

};

let APIResponse = {
    status: {
        200: { message: local['PatchEmailIdExistsVerificaiton']['200'] },
        412: { message: local['PatchEmailIdExistsVerificaiton']['412'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] }
    }
}

module.exports = { APIHandler, payloadValidator, APIResponse }