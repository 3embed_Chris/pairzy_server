'use strict'
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const local = require('../../../locales');
const userListCollection = require('../../../models/userList');

let payloadValidator = Joi.object({
    phoneNumber: Joi.string().required().description("enter phoneNumber with country code").example("+919620826142").error(new Error('phoneNumber is missing')),
}).required();

/**
 * @method PATCH verificationPhoneNumberExists
 * @description This API use to PATCH verificationPhoneNumberExists.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} phoneNumber enter phoneNumber with country code
 
 * @returns  200 : Mobile number already exist in the database..
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Mobile number doesnot exist in the database.
 * @returns  500 : An unknown error has occurred.
 */
let APIHandler = (req, res) => {

    
    let phoneNumber = req.payload.phoneNumber;
    let condition = { contactNumber: phoneNumber,"deleteStatus": { '$exists': false, '$ne': 1 } };

    userListCollection.Select(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['200'] }).code(500);
        } else if (result[0]) {
            return res({ message: req.i18n.__('PatchPhoneNumberExistsVerificaton')['200'] }).code(200);
        } else {
            return res({ message: req.i18n.__('PatchPhoneNumberExistsVerificaton')['412'] }).code(412);
        }
    });
};

let APIResponse = {
    status: {
        200: { message: local['PatchPhoneNumberExistsVerificaton']['200'] },
        412: { message: local['PatchPhoneNumberExistsVerificaton']['412'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] }
    }
}

module.exports = { APIHandler, payloadValidator, APIResponse }