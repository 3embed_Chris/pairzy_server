'use strict'
var patchEmailIdExistsVerificaitonAPI = require('./PatchEmailIdExists');
let patchPhoneNumberExistsVerificatonAPI = require("./PatchPhoneNumberExists");
var headerValidator = require('../../middleware/validator');

module.exports = [
    {
        method: 'PATCH',
        path: '/emailIdExistsVerificaiton',
        handler: patchEmailIdExistsVerificaitonAPI.APIHandler,
        config: {
            description: 'This API confirms if the email id entered is already registered or not ',
            tags: ['api', 'verification'],
            auth: false,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchEmailIdExistsVerificaitonAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: patchEmailIdExistsVerificaitonAPI.APIResponse
        }
    }, {
        method: 'PATCH',
        path: '/phoneNumberExistsVerificaton',
        handler: patchPhoneNumberExistsVerificatonAPI.APIHandler,
        config: {
            description: 'This API confirms if the phone number entered is already registered or not.',
            tags: ['api', 'verification'],
            auth: false,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchPhoneNumberExistsVerificatonAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: patchPhoneNumberExistsVerificatonAPI.APIResponse
        }
    }
];