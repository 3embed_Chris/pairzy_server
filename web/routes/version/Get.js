'use strict'
const Joi = require("joi");
const local = require('../../../locales');
const config = require("../../../config")
const appVersionCollection = require('../../../models/appVersion');
/**
 * @method GET version
 * @description This API use to GET version.
 
 * @param {*} req 
 * @param {*} res 
 
 * @returns  200 : retun latest version.
 * @returns  500 : An unknown error has occurred.
 */

let validator = Joi.object({
    type: Joi.number().required().description("for iOS :2 , android :1").error(new Error('type is missing'))
}).required();
let APIHandler = (req, res) => {
    //return res({ message: req.i18n.__('GETLatestVersion')['200'], data: { version: config.server.LATEST_VERSION } }).code(200);
let type =  { "type" : req.params.type }
let field = {
    _id:0,
    timeStamp:0,
    type:0
}
    appVersionCollection.SelectOneVersion( type, (err, result) => {
            if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);


            return res({ message: req.i18n.__('GETLatestVersion')['200'] ,data:  result  }).code(200);
        });
};

let APIResponse = {
    status: {
        200: { message: local['GETLatestVersion']['200'], data: Joi.any()},
        500: { message: local['genericErrMsg']['500'] }
    }
}

module.exports = { APIHandler, APIResponse,validator }