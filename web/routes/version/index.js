'use strict'
var GetAPI = require('./Get');
let headerValidator = require('../../middleware/validator');

module.exports = [
    // {
    //     method: 'GET',
    //     path: '/version',
    //     handler: GetAPI.APIHandler,
    //     config: {
    //         description: 'This API will return latest version of the APP',
    //         tags: ['api', 'version'],
    //         auth: false,
    //         response: GetAPI.APIResponse
    //     }
    // },
    {
        method: 'GET',
        path: '/version/{type}',
        handler: GetAPI.APIHandler,
        config: {
            description: 'This API will return latest version of the APP',
            tags: ['api', 'version'],
            auth: false,
            validate: {
                params: GetAPI.validator,
               // headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: getAPI.response
        }
    },
];