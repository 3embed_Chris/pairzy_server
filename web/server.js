var Hapi = require('hapi');
var Server = new Hapi.Server();
var cron = require('node-cron');
var logger = require('winston');
var cluster = require('cluster');
 const trace = require("../node-trace");
 const trace_endpoint = require("../node-trace").Endpoint;
const rabbitMq = require('../library/rabbitMq');
const rabbitMqWorker = require('../library/rabbitMq/worker');
var config = require('../config');
var db = require('../models/mongodb');
var middleware = require('./middleware');
var userListType = require('../models/userListType');
var numCPUs = require('os').cpus().length;
 var express = require("express");

var redis = require("redis")

var subscriber = redis.createClient(6379, config.redis.REDIS_IP);
subscriber.auth("3embed");
var cronJobTask = require('../cronJobTask');
var elasticSearchDB = require('../models/elasticSearch');
db.connect(() => { });//create a connection to mongodb
var initialize = () => {
}

if (cluster.isMaster) {
    db.connect(() => { });//create a connection to mongodb
    elasticSearchDB.connect(() => { }); // create a connection to elasticSearchDB 

    cron.schedule('* * * * *', function () {
        cronJobTask.removeBoostIfRequired();
        console.log("cron Job Task Heppning * * * * * ")
    });
    cron.schedule('* * * * *', function () {
        cronJobTask.updateOnlineUsers();
        cronJobTask.findAndRemoveExpiredInAppPurchasePlans();
        // cronJobTask.sendWellComeMailOnSignUp(11);
        console.log("cron Job Task Heppning * * * * *")
    });
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    //cluster.fork("../worker/bulk-CoinInsert/")
    var metricsServer = express();
    metricsServer.listen(3002, () => { });
    metricsServer.use('', trace.MetricsCluster(metricsServer, express));
    metricsServer.use('', trace.SnapshotExpress(metricsServer, express));

    subscriber.on("message", function (channel, key) {
        
        if (key.match(cronJobTask.likeNotificationAt)) {

            cronJobTask.sendLikeNotification(key.replace(cronJobTask.likeNotificationAt, ""));
        } else if (key.match(cronJobTask.redisKeyForBoost)) {
            cronJobTask.onBoostExpire(key.replace(cronJobTask.redisKeyForBoost, ""));
        }

        console.log("\n\n\n\nMessage '" + key + "' on channel '" + channel + "' arrived!")
    });
    subscriber.subscribe("__keyevent@0__:expired");


    cluster.on('exit', function (worker) {
        logger.error(`worker ${worker.process.pid} died`);
        cluster.fork();
    });

} else {
    Server.connection({
        port: config.server.port,
        routes: {
            cors: true
        }
    });
    Server.register(
        [
            middleware.good,
            middleware.swagger.inert,
            middleware.swagger.vision,
            middleware.swagger.swagger,
            middleware.auth.HAPI_AUTH_JWT,
            middleware.localization.i18n
        ], function (err) {
            if (err) Server.log(['error'], 'hapi-swagger load error: ' + err)
        });

    Server.auth.strategy('refJwt', 'jwt', middleware.auth.refJWTConfig);
    Server.auth.strategy('userJWT', 'jwt', middleware.auth.userJWTConfig);
    Server.route(require('./routes/index'));
  
    Server.on('response', (req) => {
        logger.info("req.payload : ", JSON.stringify(req.payload));
         trace_endpoint.onComplete(req.info.received, req.route.method.toUpperCase(), req.route.path, req.response.statusCode)
    });


    initialize = () => {
        Server.start(() => {
            logger.info(`Server is listening on port `, config.server.port)
            db.connect(() => { });//create a connection to mongodb
            elasticSearchDB.connect(() => { }); // create a connection to elasticSearchDB 

            rabbitMq.connect(() => {
                rabbitMqWorker.startWorker()
            });
            // redisDB.connect(()=>{});
        });// Add the route
    }
}

//do something when app is closing
process.on('close', function (code, signal) {
    console.log(`close main process exited with code $ and signal ${signal}`);
    rabbitMqWorker.exitHandler()
});

//do something when app is closing
process.on('exit', function (code, signal) {
    console.log(`exit main process exited with code $ and signal ${signal}`);
    rabbitMqWorker.exitHandler()
});

//catches ctrl+c event
process.on('SIGINT', function (code, signal) {
    console.log(`SIGINT main process exited with code $ and signal ${signal}`);
    rabbitMqWorker.exitHandler();
});

//catches ctrl+c event
process.on('SIGTERM', function (code, signal) {
    console.log(`SIGTERM main process exited with code $ and signal ${signal}`);
    rabbitMqWorker.exitHandler();
});

//catches uncaught exceptions
process.on('uncaughtException', function (err, code, signal) {
    console.log(`uncaughtException ${err} main process exited with code ${code} and signal ${signal}`);
    rabbitMqWorker.exitHandler();
});

module.exports = { initialize };