'use strict'
const logger = require('winston');
const rabbitMq = require('../../library/rabbitMq');
const coinWalletCollection = require("../../models/coinWallet")
const walletCustomerCollection = require('../../models/walletCustomer');
const ObjectID = require('mongodb').ObjectID;
const mongodb = require('../../models/mongodb');
mongodb.connect(() => { });//create a connection to mongodb
const moment = require("moment");
const Timestamp = require('mongodb').Timestamp;

const elasticSearchDB = require('../../models/elasticSearch');
elasticSearchDB.connect(() => { });
const userPostCollectionES = require('../../models/userPostES');
const userPostCollection = require("../../models/userPost");
const userListType = require('../../models/userListType');
const userList = require('../../models/userList');
const fcmPush = require("../../library/fcm");
rabbitMq.connect(() => {

    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.Coin_wallet, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {

    channel.assertQueue(queue.name, queue.options, function (err, amqpQueue) {
        if (err) {
            process.exit();
        } else {
            channel.consume(queue.name, function (msg) {
                try {
                    var dataInArray = JSON.parse(msg.content.toString());
                    insertInWalletCusetomerAndUpdateInCoinWallet(dataInArray.data.userId)

                } catch (error) {
                    logger.error("error : ", error);
                }
                /*insert WalletCusetomer And Update In Coin Wallet Collections
                 */
                function insertInWalletCusetomerAndUpdateInCoinWallet(id) {
                    return new Promise((resolve, reject) => {
                        let uid = id.toString()
                        coinWalletCollection.SelectOne({ _id: ObjectID(uid) }, (err, balance) => {
                            if (err) { logger.error(err) }
                            let balanceCoin = (balance.coins) ? balance.coins.Coin : 0;
                            let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
                            let insertData = [];

                            insertData.push({
                                "txnId": txnId,
                                "userId": ObjectID(uid),
                                "txnType": "CREDIT",
                                "txnTypeCode": 1,
                                "trigger": "Coins credit for playing match maker",
                                "docType": "on Earn coins By playing match maker",
                                "docTypeCode": 20,
                                "currency": "N/A",
                                "currencySymbol": "N/A",
                                "coinType": "Coin",
                                "coinOpeingBalance": balanceCoin,
                                "cost": 0,
                                "coinAmount": 100,
                                "coinClosingBalance": (balanceCoin + 100),
                                "paymentType": "N/A",
                                "timestamp": new Date().getTime(),
                                "transctionTime": new Date().getTime(),
                                "transctionDate": new Date().getTime(),
                                "paymentTxnId": "N/A",
                                "initatedBy": "customer",
                                "note": "Match Maker Game"
                            });
                            walletCustomerCollection.InsertMany(insertData, (walerr, reswal) => {
                                if (walerr) { logger.error(walerr) }
                                if (reswal) {
                                    sendPush(uid)
                                    coinWalletCollection.UpdateWitInc({ _id: ObjectID(uid) }, { "coins.Coin": 100 }, (errwaa, rewaa) => {
                                        if (errwaa) { logger.error(errwaa); }
                                        if (rewaa) {
                                            userListType.UpdateWithPushIncCoinCount(uid.toString(), (errUL, resUL) => {
                                                if (resUL) {
                                                    logger.silly("done")
                                                } else {
                                                    logger.silly(errUL)
                                                }
                                            })
                                            logger.silly("rewaa done")
                                        }

                                    })
                                }


                            });
                        })
                    });
                }

                /*  send  fcm push to all reward user  */
                function sendPush(Id) {

                    var condition = { _id: ObjectID(Id) }
                    userList.SelectOne(condition, (err, result) => {
                        logger.silly("firstName", result.firstName)

                        logger.error(err)
                        
                        if (result && result.firstName) {
                            let deviceType = result.currentLoggedInDevices.deviceType || "1";
                            let postData = {
                                "longitude":result.location.longitude,
                                "latitude": result.location.latitude,

                            }
                            createPost(postData)
                            let request = {
                                data: {
                                    type: "32", deviceType: deviceType, title: process.env.APPNAME, message: "Pair Success!"
                                },
                                notification: { "title": process.env.APPNAME, "body": "Pair Success!" }
                            };

                            fcmPush.sendPushToTopic(`/topics/${Id}`, request, () => { })
                            
                        } else {
                            logger.error("fcm error in worker  : - ", err)
                        }
                    });
                }


                function createPost(data) {

                        var dataToInsert = {
                            _id: new ObjectID(),
                            userId: ObjectID(dataInArray.data.userId),
                            targetId: "",
                            userName: "Pairzy",
                            profilePic: "https://res.cloudinary.com/pipelineai/image/upload/v1564647385/admin/vygsi2mjkouwk2ndktgp.png",
                            type: "photo Post",
                            typeFlag: 1,
                            isPairSuccess:true,
                            postedOn: moment().valueOf(),
                            createdOn: new Timestamp(),
                            date: new Date(),
                            description: "Pair Successful!",
                            status: 0,
                            url: [dataInArray.data.firstLikedByPhoto,dataInArray.data.userPhoto],
                            likeCount: 0,
                            commentCount: 0,
                            Likers: [],
                            longitude: data.longitude || 0,
                            latitude: data.latitude || 0,

                        }
                        userPostCollection.Insert(dataToInsert, (err, result) => {
                            logger.error(err)
                            if (result) {
                                inserIntoElastic(dataToInsert)
                            } else {
                                logger.error("createPost error in worker  : - ", err)
                            }
                        });
                    
                }
                /*  create and insert post into Elastic search 
                    */

                function inserIntoElastic(dataToInsert) {
                        userPostCollectionES.Insert(dataToInsert, (err, result) => {
                            logger.error("createPost error in worker  : - ", err)
                           
                            
                        });
                   
                }

            }, { noAck: true }, function (err, ok) {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}