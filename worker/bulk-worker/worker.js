'use strict'
const logger = require('winston');
const rabbitMq = require('../../library/rabbitMq');
const userListType = require('../../models/userListType');
const elasticSearchDB = require('../../models/elasticSearch');
var bulkMsg = [];
var timeOut;
rabbitMq.connect(() => {
    elasticSearchDB.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.update_bulk, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {

    channel.assertQueue(queue.name, queue.options, function (err, amqpQueue) {
        if (err) {
            process.exit();
        } else {
            channel.consume(queue.name, function (msg) {
                try {
                    var dataInArray = JSON.parse(msg.content.toString());
                    dataInArray.forEach(element => {
                        bulkMsg.push(element);
                    });
                    if (bulkMsg.length > 100) {
                        updateInES();
                    } else {
                        timeOut = setTimeout(updateInES, 1000 * 10);
                    }
                } catch (error) {
                    logger.error("error : ", error);
                }

                function updateInES() {
                    if (bulkMsg.length) {
                        userListType.Bulk(bulkMsg, (e, r) => { if (e) logger.error(e) });
                    }
                    bulkMsg = [];
                    clearTimeout(timeOut);
                }

            }, { noAck: true }, function (err, ok) {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}