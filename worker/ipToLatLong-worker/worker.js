'use strict'
const logger = require('winston');
const rabbitMq = require('../../library/rabbitMq');
const ipc = require('node-ipc');
const userList = require('../../models/userList');
const elasticSearchDB = require('../../models/elasticSearch');
const mongodb = require('../../models/mongodb');
elasticSearchDB.connect(() => { });
mongodb.connect(() => { });//create a connection to mongodb
const userListType = require('../../models/userListType');
var geoip = require('geoip-lite');
const userListCollection = require("../../models/userList");
var NodeGeocoder = require('node-geocoder');
const iplocation = require("iplocation").default;
var geo
rabbitMq.connect(() => {
    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.update_location_queue, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {
    channel.assertQueue(queue.name, queue.options, function (err, amqpQueue) {
        if (err) {
            process.exit();
        } else {
            channel.consume(queue.name, function (msg) {

                let data = JSON.parse(msg.content.toString());

                let _id = data.data._id;
                let ip = data.data.ip;
                // let geo = geoip.lookup(ip);

                iplocation(ip, [], (error, res) => {

                    /**
                     * {
                         as: 'AS11286 KeyBank National Association',
                         city: 'Cleveland',
                         country: 'United States',
                         countryCode: 'US',
                         isp: 'KeyBank National Association',
                         lat: 41.4875,
                         lon: -81.6724,
                         org: 'KeyBank National Association',
                         query: '156.77.54.32',
                         region: 'OH',
                         regionName: 'Ohio',
                         status: 'success',
                         timezone: 'America/New_York',
                         zip: '44115'
                     }
                     */

                    geo = res;


                });
                try {
                    if (geo) {
                        geo = { longitude: geo.longitude, latitude: geo.latitude };
                        userList.UpdateById(_id, { location: geo }, (emongo, rmongo) => {
                            if (emongo) { logger.error("emongo Erro", emongo) }
                            if (rmongo) { logger.info("rmongo resES") }

                        });

                        let data_ES = {
                            location: {
                                "lat": geo.latitude,
                                "lon": geo.longitude
                            }
                        };
                        userListType.Update(_id, data_ES, (eres, reses) => {
                            if (eres) { logger.error("emongo Erro", eres) }
                            if (reses) { logger.info("rmongo resES") }

                         });



                        try {
                            var options = {
                                provider: 'google',
                                httpAdapter: 'https', // Default
                                apiKey: process.env.GOOGLE_API_KEY, // for Mapquest, OpenCage, Google Premier
                                formatter: null         // 'gpx', 'string', ...


                            };
                            var geocoder = NodeGeocoder(options);
                            geocoder.reverse({ lat: geo.latitude, lon: geo.longitude }, function (err, res) {
                                if (err) { logger.error("error in geocoder reverse", err) }
                                if (res) { logger.info("geocoder reverse success") }
                                if (res && res[0]) {
                                    var add = {
                                    }
                                    add["formattedAddress"] = res[0].formattedAddress;
                                    add["latitude"] = res[0].latitude;
                                    add["longitude"] = res[0].longitude;
                                    add["googlePlaceId"] = res[0].extra.googlePlaceId;
                                    add["neighborhood"] = res[0].extra.neighborhood;
                                    add["level2long"] = res[0].administrativeLevels.level2long;
                                    add["level2short"] = res[0].administrativeLevels.level2short;
                                    add["level1long"] = res[0].administrativeLevels.level1long;
                                    add["level1short"] = res[0].administrativeLevels.level1short;
                                    add["streetName"] = res[0].streetName;
                                    add["city"] = res[0].city;
                                    add["country"] = res[0].country;
                                    add["countryCode"] = res[0].countryCode;
                                    add["zipcode"] = res[0].zipcode;
                                    add["provider"] = res[0].provider;

                                    userListCollection.UpdateById(_id, {
                                        "address": add
                                    }, (errMDB, resMDB) => {
                                        if (errMDB) { logger.error("error in mongo", err) }
                                        if (resMDB) {
                                            logger.info("mongo Done")
                                            var ESUpdate = { "address": add }
                                            userListType.Update(_id, ESUpdate, (erES, resES) => {
                                                if (err) { logger.error("Es Erro", erES) }
                                                if (resES) { logger.info("Es resES") }

                                            });

                                        }

                                    })
                                }
                            });
                        } catch (error) {
                            logger.error("IP_______OVER_QUERY_LIMIT. You have exceeded your daily request quota for this API. If you did not set a custom daily request quota, verify your project has an active billing account: http://g.co/dev/maps-no-account")

                        }

                    } else {

                        console.log("error accure during updating lat longgggggg")
                      
                    }

                } catch (error) {
                    logger.error("error")
                }

            }, { noAck: true }, function (err, ok) {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}