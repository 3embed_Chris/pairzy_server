'use strict'
const logger = require('winston');
const mongodb = require('../../models/mongodb');
const rabbitMq = require('../../library/rabbitMq');
const ObjectID = require("mongodb").ObjectID
const elasticSearchDB = require('../../models/elasticSearch');
elasticSearchDB.connect(() => { });
mongodb.connect(() => { });//create a connection to mongodb
const userListType = require('../../models/userListType');
const userListCollection = require("../../models/userList");
var NodeGeocoder = require('node-geocoder');

rabbitMq.connect(() => {
    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.update_latLong_to_city, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {

    channel.assertQueue(queue.name, queue.options, function (err, amqpQueue) {
        if (err) {
            process.exit();
        } else {
            channel.consume(queue.name, function (msg) {
                let data = JSON.parse(msg.content.toString());
                try {
                    var options = {
                        provider: 'google',
                        httpAdapter: 'https', // Default
                        apiKey: process.env.GOOGLE_API_KEY, // for Mapquest, OpenCage, Google Premier
                        formatter: null         // 'gpx', 'string', ...
                    };
                    var geocoder = NodeGeocoder(options);
                    geocoder.reverse({ lat: data.lat, lon: data.lon }, function (err, res) {
                        if (res && res[0]) {
                            logger.info("update city successfully in lat long worker ")
                            let address = {};
                            if (res[0]["formattedAddress"]) address["formattedAddress"] = res[0]["formattedAddress"]
                            if (res[0]["latitude"]) address["latitude"] = res[0]["latitude"]
                            if (res[0]["longitude"]) address["longitude"] = res[0]["longitude"]
                            if (res[0]["streetNumber"]) address["streetNumber"] = res[0]["streetNumber"]
                            if (res[0]["streetName"]) address["streetName"] = res[0]["streetName"]
                            if (res[0]["country"]) address["country"] = res[0]["country"]
                            if (res[0]["countryCode"]) address["countryCode"] = res[0]["countryCode"]
                            if (res[0]["city"]) address["city"] = res[0]["city"]


                            userListCollection.UpdateById(data._id, {
                                "address": address
                            }, (erMDB, resMDB) => {
                                if (erMDB) { logger.error("erMDB Erro", erMDB) }
                                if (resMDB) {
                                    logger.info("Es resES")
                                    var ESUpdate = { "address": address }
                                    userListType.Update(data._id, ESUpdate, (erES, resES) => {
                                        if (err) { logger.error("Es Erro", erES) }
                                        if (resES) { logger.info("Es resES") }

                                    });

                                }
                            })

                        } else {
                            logger.error("LAT__LONG___OVER_QUERY_LIMIT. You have exceeded your daily request quota for this API. If you did not set a custom daily request quota, verify your project has an active billing account: http://g.co/dev/maps-no-account")
                        }
                    });
                } catch (error) {
                    logger.error("LAT__LONG___OVER_QUERY_LIMIT. You have exceeded your daily request quota for this API. If you did not set a custom daily request quota, verify your project has an active billing account: http://g.co/dev/maps-no-account", error)
                }


            }, { noAck: true }, function (err, ok) {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}