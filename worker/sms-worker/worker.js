'use strict'
const logger = require('winston');
const sendMail = require('../../library/mailgun');
const mongodb = require('../../models/mongodb');
const rabbitMq = require('../../library/rabbitMq');

rabbitMq.connect(() => {
    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.reg_email_queue, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {

    channel.assertQueue(queue.name, queue.options, function (err, amqpQueue) {
        if (err) {
            process.exit();
        } else {
            channel.consume(queue.name, function (msg) {

                var data = JSON.parse(msg.content.toString());
                let param = {
                    from: data.from,
                    to: data.to,
                    subject: data.subject,
                    html: data.html,
                    text: data.html
                }
                sendMail.sendMail(param, (err, res) => {
                    if (res) {
                        logger.silly("maila sent to ", data.to);
                    }
                });

            }, { noAck: true }, function (err, ok) {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}